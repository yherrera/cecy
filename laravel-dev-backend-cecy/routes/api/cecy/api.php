<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Reports
use App\Http\Controllers\Cecy\Report\CourseController;
use App\Http\Controllers\Cecy\Report\PlanificationController;
use App\Http\Controllers\Cecy\Report\DetailPlanificationController;
use App\Http\Controllers\Cecy\AuthoritiesController;
use App\Http\Controllers\Cecy\InstitutionsController;
use App\Http\Controllers\Cecy\CatalogueController;
use App\Http\Controllers\Cecy\ScheduleContoller;
use App\Http\Controllers\Cecy\SchoolPeriodController;
use App\Http\Controllers\Cecy\ParticipantController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Begin router of master table router
Route::apiResource('authorities', AuthoritiesController::class);
Route::apiResource('institutions', InstitutionsController::class);
Route::apiResource('participant', ParticipantController::class);
Route::apiResource('schedule', ScheduleContoller::class);
Route::apiResource('schoolPeriod', SchoolPeriodController::class);
Route::apiResource('catalogue',CatalogueController::class);
Route::get('AuthoritiesUser', [AuthoritiesController::class,'getUser']);

//End router of master table router

//Begin router of reports  
Route::group(['prefix' => 'courses'], function () {
   Route::get('', [CourseController::class, 'index']);
   Route::get('filter/{id}', [CourseController::class, 'filter']);
   Route::get('/photographic_register_export.pdf/{id}', [CourseController::class, 'photographicRegisterPDF']);
   Route::get('/curricular_design_export.pdf/{id}', [CourseController::class, 'curricularDesignPDF']);
});

Route::group(['prefix' => 'planification'], function () {
   Route::get('', [PlanificationController::class, 'index']);

});

Route::group(['prefix' => 'detail_planifications'], function () {
   Route::get('', [DetailPlanificationController::class, 'index']);
   //filter
   Route::get('/inscription/{id}', [DetailPlanificationController::class, 'showInscription']);
   //export.pdf
   Route::get('/monthly_planning_export.pdf/{id}', [DetailPlanificationController::class, 'planificationPDF']);
   Route::get('/needs_export.pdf/{id}', [DetailPlanificationController::class, 'needsPDF']);
   Route::get('/enrolled_export.pdf/{id}', [DetailPlanificationController::class, 'enrolledPDF']);
   Route::get('/inscription_export.pdf/{id}', [DetailPlanificationController::class, 'inscriptionPDF']);
});

//End router of reports




/**Notas de alumnos en Curso */
/* Route::resource('notes', 'Cecy\Note\NotesController'); */
 /**aistencia de alumnos en Curso */
/* Route::resource('assistances', 'Cecy\Assistance\AssistanceController'); */
 /** Home (por que no funciona el resto y debo trabajar )*/
/* Route::apiResource('home', 'Cecy\HomeController'); */
