<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INFORME DE NECESIDAD DEL CURSO</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
        body{
            font-size: 9;
            font-family: 'calibri';
        }

        #img th {
            padding-top: 5px;
            padding-bottom: 1px;
            text-align: center;
            background-color: #b8cce4;
            color: #000;
            width: 100%;
        }

        #header-table th, #header-table td{
            border: 1px solid #ddd;
            width: 100%;
            padding-top: 5px;
            padding-left: 5px;
        }

        #line-table td{
            padding-top: 80px;
            text-align: center;
        }

        #signing-taable th{
            padding-bottom: 50px;
            text-align: center;
        }

        #footer-table td{
            border: 1px solid #000;
            padding-left: 30PX;
            padding-top: 10px;
            padding-right: 30px;
            text-align: justify;
        }
    </style>
<body>
    <div style="overflow-x:auto;">
    @foreach($data as $courseNeed)
        <table>
            <tr id="img">
                <th colspan="4" >img</th>
                <th colspan="4">img</th>
                <th colspan="4">Código: {{$courseNeed->course->code}}</th>
                
            </tr>
            <tr id="img">
            <th colspan="12">
                    INSTITUTO TECNOLÓGICO SUPERIOR "{{$courseNeed->course->institution->name}}"
                    <h6>INFORME DE NECESIDAD DEL CURSO</h6>
                </th>
            </tr>
            <tr id="header-table">
                <th colspan="4">Nombre del Docente:</th>
                <td colspan="8">{{$courseNeed->instructor->user->first_name}} {{$courseNeed->instructor->user->second_name}} {{$courseNeed->instructor->user->first_lastname}} {{$courseNeed->instructor->user->second_lastname}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="4">Nombre del Curso:</th>
                <td colspan="8">{{$courseNeed->course->name}}</td>
            </tr>
            
            <tr id="header-table">
                <th colspan="4">Tipo de curso:</th>
                <td colspan="2">Administrativo:</td>
                @if($courseNeed->course->course->name == "ADMINISTRATIVO")
                    <td colspan="1">X</td> 
                @endif
                @if($courseNeed->course->course->name != "ADMINISTRATIVO")
                        <td colspan="1"></td>
                @endif 
                <td colspan="3">Técnico:</td>
                @if($courseNeed->course->course->name == "TECNICO")
                    <td colspan="2">X</td>  
                @endif
                @if($courseNeed->course->course->name != "TECNICO")
                        <td colspan="2"></td>
                @endif
            </tr>

            <tr id="header-table">
                <th colspan="4">Modalidad del curso:</th>
                <td colspan="2">Presencial:</td>
                @if($courseNeed->course->modality->name == "PRESENCIAL")
                    <td colspan="1">X</td> 
                @endif
                @if($courseNeed->course->modality->name != "PRESENCIAL")
                        <td colspan="1"></td>
                @endif
                <td colspan="3">Virtual:</td>
                @if($courseNeed->course->modality->name == "VIRTUAL")
                    <td colspan="2">X</td>  
                @endif
                @if($courseNeed->course->modality->name != "VIRTUAL")
                        <td colspan="2"></td>
                @endif
            </tr>

            <tr id="header-table">
                <th colspan="12">
                    Necesidad del Curso (Por que en el sector y por las carreras es necesario el curso)																	
                </th>
            </tr>

            @foreach($courseNeed->needs as $need)
                <tr id="header-table">
                    <td colspan="12">
                        {{$need}}																
                    </td>
                </tr>
            @endforeach

            
            <tr id="header-table">
                <th colspan="5">Duración del curso:</th>
                <td colspan="1">{{$courseNeed->course->duration}}</td>
                <td colspan="1">Horas</td>
                <td colspan="5"></td>
            </tr>
            
            <tr id="header-table">
                <th colspan="7">
                    Lugar / Lugares donde se dictará (Indicar si necesitará salidas de campo)
                </th>
                <td colspan="5">{{$courseNeed->course->local_proposal}}</td>
            </tr>

            @foreach($courseNeed->course->schedules as $schedule)
            <tr id="header-table">
                <th colspan="3" rowspan="2">Horario del Curso:</th>
                <td colspan="1" rowspan="2">{{$schedule->start_time}}</td>
                <td colspan="1" rowspan="2">A</td>
                <td colspan="1" rowspan="2">{{$schedule->end_time}}</td>
                <th colspan="1" rowspan="2">Días:</th>
                <td colspan="2">X</td>
                <td colspan="1"></td>
                <td colspan="1"></td>
                <td colspan="1"></td>
            </tr>
            @endforeach
            <tr id="header-table">
                <td colspan="2">Lunes-Viernes</td>
                <td colspan="1">Sábados</td>
                <td colspan="1">Lunes-Domin.</td>
                <td colspan="1">Domingos</td>
            </tr>

            <tr id="header-table">
                <th colspan="4">Fecha de Iniciación:</th>
                <td colspan="2">{{$courseNeed->date_start}}</td>
                <td colspan="3">Fecha real de finalización:</td>
                <td colspan="3">{{$courseNeed->date_end}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="5">Fecha prevista de finalización:</th>
                <td colspan="7">{{$courseNeed->planned_end_date}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="5">Participantes a ser inscritos:</th>
                <td colspan="7">{{$courseNeed->capacity}}</td>
            </tr>


            <tr id="header-table">
                <th colspan="12">
                    Resumen del Curso y Posible Proyecto:																	
                </th>
            </tr>

            <tr id="header-table">
                <th colspan="12">
                    {{$courseNeed->summary}}																
                </th>
            </tr>
            

            <tr id="header-table">
                <th colspan="12">
                    Indicar a que población se encuentra dirigido el curso: 																	
                </th>
            </tr>

                <tr id="header-table">
                    <th colspan="12">
                        {{$courseNeed->course->participant_type_id}}																
                    </th>
                </tr>


            <tr id="line-table">
                <td colspan="6">________________________</td>
                <td colspan="6">________________________</td>
            </tr>

            <tr id="signing-taable">
                <th colspan="6">REPRESENTANTE DEL OCS</th>
                <th colspan="6">Fecha:  {{\Carbon\Carbon::now()->format('Y/M/D')}}</th>
            </tr>

            <tr id="line-table">
                <td colspan="6">________________________</td>
                <td colspan="6">________________________</td>
            </tr>

            <tr id="signing-taable">
                <th colspan="6">VICERRECTOR </th>
                <th colspan="6">Fecha: {{\Carbon\Carbon::now()->format('Y/M/D')}}</th>
            </tr>

            <tr id="footer-table">
                <td colspan="12"> 
                    <p> <b>Nota:</b> 
                    Documento que deberá ser aprobado por parte del Organo Colegiado Superior (OCS) y del Vicerrector del Instituto, 
                    tal como lo indica la normativa y el Acuerdo 118 (Instructivo de Capacitación - Certificación por Competencias 
                    Laborales SENESCYT), en el caso que sea un registro digital, se adjuntan las firmas de responsabilidad como fotografìas 
                    en las celdas correspondientes 
                    </p>
                </td>
            </tr>
        </table>
    @endforeach 
    </div>
</body>

</html>