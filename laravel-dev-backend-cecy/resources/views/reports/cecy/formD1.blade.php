<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PROGRAMACIÓN DE CURSOS DE CAPACITACIÓN MENSUAL</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
    body {
        font-size: 9;
    }

    #img th {
        padding-top: 5px;
        padding-bottom: 1px;
        text-align: center;
        background-color: #b8cce4;
        color: #000;
        width: 100%;
    }

    #header-table th,
    #header-table td {
        border: 1px solid #ddd;
        width: 100%;
        padding-left: 5px;
    }

    #by-table td {
        padding-top: 90px;
    }

    #line-table td {
        padding-top: 30px;
    }

    #signing-taable td {
        padding-bottom: 50px;
    }

    #footer-table td {
        border: 1px solid #000;
        padding-left: 30PX;
        padding-top: 10px;
        padding-right: 30px;
        text-align: justify;
    }
</style>

<body>
    <div style="overflow-x:auto;">
    @foreach($data as $detailPlanification)
        <table>
            <tr id="img">
                <th colspan="3">img</th>
                <th colspan="6" style="width: 350px;">
                    INSTITUTO TECNOLÓGICO SUPERIOR "{{$detailPlanification->course->institution->name}}"
                    <h6>PROGRAMACIÓN DE CURSOS DE CAPACITACIÓN MENSUAL</h6>
                </th>
                <th colspan="3">img</th>
                <th colspan="1">Año: {{\Carbon\Carbon::now()->format('Y')}}</th>
            </tr>

            <tr id="header-table">
                <th>SECTOR</th>
                <th>ÁREA</th>
                <th>NOMBRE DEL CURSO</th>
                <th>¿CURSO OCC ? SI / NO </th>
                <th>DURACIÓN</th>
                <th>FECHAS INICIA</th>
                <th>FECHAS FINALIZA</th>
                <th>HORARIO DESDE</th>
                <th>HORARIO HASTA</th>
                <th>LUGAR DEL CURSO DICTADO</th>
                <th>Nro. DE PARTICIPANTES</th>
                <th>DOCENTE</th>
                <th>RESPONSABLE</th>
            </tr>

            <tr id="header-table">
                <td>{{$detailPlanification->course->specialty->name}}</td>
                <td>{{$detailPlanification->course->area->name}}</td>
                <td>{{$detailPlanification->course->name}}</td>
                <td>{{$detailPlanification->course->area->id}} Si</td>
                <td>{{$detailPlanification->course->duration}}</td>
                <td>{{$detailPlanification->date_start}}</td>
                <td>{{$detailPlanification->date_end}}</td>

                @foreach($detailPlanification->course->schedules as $schedule)
                <td>{{$schedule->start_time}}</td>
                <td>{{$schedule->end_time}}</td>
                @endforeach
                <td>{{$detailPlanification->course->local_proposal}}</td>
                <td>{{$detailPlanification->capacity}}</td>
                <td>{{$detailPlanification->instructor->user->first_name}} {{$detailPlanification->instructor->user->first_lastname}}</td>
                <td>{{$detailPlanification->course->user->first_name}} {{$detailPlanification->course->user->first_lastname}}</td>
            </tr>

            <tr id="by-table">
                <td colspan="5"><b>Elaborado por: </b></td>
                <td colspan="4"><b>Revisado por: </b></td>
                <td colspan="4"><b>Aprobado por: </b></td>
            </tr>

            <tr id="line-table">
                <td colspan="5">_________________________________</td>
                <td colspan="4">________________________</td>
                <td colspan="4">________________________</td>
            </tr>

            <tr id="signing-taable">
                <td colspan="5">Firma: Coordinador de Vinculación con la Comunidad</td>
                <td colspan="4">Firma: Vicerrector ITS </td>
                <td colspan="4">Firma: Representante del OCS.</td>
            </tr>

            <tr id="footer-table">
                <td colspan="13">
                    <p> <b>Nota:</b> Este documento deberá generarse de manera mensual sobre la planificación de cursos de capacitación tanto SENESCYT como OCC, respaldado en el Instructivo Capacitación - Certificación por Competencias Laborales de SENESCYT</p>
                </td>
            </tr>
        </table>
        @endforeach
    </div>
</body>

</html>