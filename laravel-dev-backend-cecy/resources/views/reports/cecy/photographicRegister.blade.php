<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>REGISTRO FOTOGRÁFICO </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
        body{
            font-size: 9;
        }

        #img th {
            padding-top: 5px;
            padding-bottom: 1px;
            text-align: center;
            background-color: #205867;
            color: #fff;
            width: 100%;
        }

        #header-table th, #header-table td{
            border: 1px solid #ddd;
            width: 100%;
            padding-left: 5px;
        }

        #header-table th{
            background-color: #205867;
            color: #fff;
        }

        #line-table td{
            padding-top: 80px;
            text-align: center;
        }

        #signing-taable th{
            padding-bottom: 50px;
            text-align: center;
        }

        #footer-table td{
            border: 1px solid #000;
            padding-left: 30PX;
            padding-top: 10px;
            padding-right: 30px;
            text-align: justify;
        }
    </style>
<body>
    <div style="overflow-x:auto;">
        <table>
            <tr id="img">
                <th colspan="4" >REGISTRO FOTOGRÁFICO </th>
                <th colspan="4">img</th>
                <th colspan="4">IMG</th>
            </tr>
            <tr >
                <td colspan="12" style="padding-top: 20px;"></td>
            </tr>
            @foreach($data as $course)
            <tr id="header-table">  
                <th colspan="3">Foto No. </th>
                <td colspan="5">{{$course->id}}</td>
                <td colspan="4" rowspan="3">{{$course->photo}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="3">Descripción</th>
                <td colspan="5">{{$course->name}}</td>
            </tr>
            
            <tr id="header-table">
                <th colspan="3">Fecha </th>
                <td colspan="5">{{$course->id}}</td>
            </tr>

            <tr >
                <td colspan="12" style="padding-top: 30px;"></td>
            </tr>
            @endforeach
            <tr id="footer-table">
                <td colspan="12"> 
                    <p> <b>Nota:</b> 
                    Arhivo para uso digital, en el caso de requerir incluir 
                    más medios de verificación se pueden usar las celdas inferiores. 
                    </p>
                </td>
            </tr>
        </table>
    </div>
</body>

</html>