<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INSCRIPCIÓN PARA CURSOS DE CAPACITACIÓN</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
        body{
            font-size: 9;
            font-family: 'calibri';
        }

        #img th {
            padding-top: 5px;
            padding-bottom: 1px;
            text-align: center;
            background-color: #b8cce4;
            color: #000;
            width: 100%;
        }

        #header-table th, #header-table td{
            border: 1px solid #ddd;
            width: 100%;
            padding-top: 5px;
            padding-left: 5px;
        }

        #line-table td{
            padding-top: 80px;
            text-align: center;
        }

        #signing-taable th{
            padding-bottom: 50px;
            text-align: center;
        }

        #footer-table td{
            border: 1px solid #000;
            padding-left: 30PX;
            padding-top: 10px;
            padding-right: 30px;
            text-align: justify;
        }
    </style>
<body>
<div style="overflow-x:auto;">
        @foreach($data as $inscription)
        <table>
            <tr id="img">
                <th colspan="4">img</th>
                <th colspan="4">INSTITUTO TECNOLÓGICO SUPERIOR "{{$inscription->detailRegistration->registration->planification->course->institution->name}}"</th>
                <th colspan="4">image 1</th>
            </tr>
            <tr id="img">
                <th colspan="12">
                    <h6>INSCRIPCIÓN PARA CURSOS DE CAPACITACIÓN</h6>
                </th>
            </tr>
            <tr id="header-table">
                <th colspan="4">Código del curso:</th>
                <td colspan="8">{{$inscription->detailRegistration->registration->planification->course->code}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="4">Nombre del Curso:</th>
                <td colspan="8">{{$inscription->detailRegistration->registration->planification->course->name}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="4">Modalidad del curso:</th>
                <td colspan="2">Presencial:</td>
                @if($inscription->detailRegistration->registration->planification->course->modality->name == "PRESENCIAL")
                <td colspan="1">X</td>
                @endif
                @if($inscription->detailRegistration->registration->planification->course->modality->name != "PRESENCIAL")
                <td colspan="1"></td>
                @endif
                <td colspan="3">Virtual:</td>
                @if($inscription->detailRegistration->registration->planification->course->modality->name == "VIRTUAL")
                <td colspan="2">X</td>
                @endif
                @if($inscription->detailRegistration->registration->planification->course->modality->name != "VIRTUAL")
                <td colspan="2"></td>
                @endif
            </tr>

            <!--PERSONAL INFORMATION-->
            <tr id="header-table">
                <th colspan="12" style="text-align: center; padding-top:10px">
                    <h6>DATOS PERSONALES</h6>
                </th>
            </tr>

            <tr id="header-table">
                <th colspan="4">Apellidos y Nombres:</th>
                <td colspan="8">{{$inscription->detailRegistration->registration->participant->user->first_lastname}} {{$inscription->detailRegistration->registration->participant->user->second_lastname}} {{$inscription->detailRegistration->registration->participant->user->first_name}} {{$inscription->detailRegistration->registration->participant->user->second_name}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="4">Cédula de Ciudadanía</th>
                <td colspan="2">{{$inscription->detailRegistration->registration->participant->user->identification}}</td>
                <th colspan="4">Fecha de nacimiento:</th>
                <td colspan="2">{{$inscription->detailRegistration->registration->participant->user->birthdate}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="2">Genero:</th>
                <td colspan="3">Femenino:</td>
                @if($inscription->detailRegistration->registration->participant->user->gender->name == "FEMENINO")
                <td colspan="2">X</td>
                @endif
                @if($inscription->detailRegistration->registration->participant->user->gender->name != "FEMENINO")
                <td colspan="2"></td>
                @endif
                <td colspan="3">Masculino:</td>
                @if($inscription->detailRegistration->registration->participant->user->gender->name == "MASCULINO")
                <td colspan="2">X</td>
                @endif
                @if($inscription->detailRegistration->registration->participant->user->gender->name != "MASCULINO")
                <td colspan="2"></td>
                @endif
            </tr>

            <tr id="header-table">
                <th colspan="2">Etnia:</th>

                <td colspan="1">Mestizo</td>
                @if($inscription->detailRegistration->registration->participant->user->id == "MESTIZO")
                <td colspan="1">X</td>
                @endif
                @if($inscription->detailRegistration->registration->participant->user->id != "MESTIZO")
                <td colspan="1"></td>
                @endif

                <td colspan="1">Indígena</td>
                @if($inscription->detailRegistration->registration->participant->user->id == "INDIGENA")
                <td colspan="1">X</td>
                @endif
                @if($inscription->detailRegistration->registration->participant->user->id != "INDIGENA")
                <td colspan="1"></td>
                @endif


                <td colspan="1">Blanco</td>
                @if($inscription->detailRegistration->registration->participant->user->id == "BLANCO")
                <td colspan="1">X</td>
                @endif
                @if($inscription->detailRegistration->registration->participant->user->id != "BLANCO")
                <td colspan="1"></td>
                @endif

                <td colspan="1">Afroecuatoriano</td>
                @if($inscription->detailRegistration->registration->participant->user->id == "NEGRO")
                <td colspan="1">X</td>
                @endif
                @if($inscription->detailRegistration->registration->participant->user->id != "NEGRO")
                <td colspan="1"></td>
                @endif

                <td colspan="1">Otros</td>
                @if($inscription->detailRegistration->registration->participant->user->id == "OTRO")
                <td colspan="1">X</td>
                @endif
                @if($inscription->detailRegistration->registration->participant->user->id != "OTRO")
                <td colspan="1"></td>
                @endif

            </tr>


            <tr id="header-table">
                <th colspan="4">Dirección Domiciliaria:</th>
                <td colspan="8"></td>
            </tr>

            <tr id="header-table">
                <th colspan="4">Nro. Telefónico:</th>
                <td colspan="2">Convencional:</td>
                <td colspan="2"></td>
                <th colspan="2">Celular:</th>
                <td colspan="2"></td>
            </tr>

            <tr id="header-table">
                <th colspan="4">Correo electrónico:</th>
                <td colspan="8">{{$inscription->detailRegistration->registration->participant->user->email}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="3">Nivel de instrucción:</th>
                <td colspan="2">Primaria:</td>
                @if($inscription->detailRegistration->registration->participant->user->id == "OTRO")
                <td colspan="1">X</td>
                @endif
                @if($inscription->detailRegistration->registration->participant->user->id != "OTRO")
                <td colspan="1"></td>
                @endif
                <th colspan="2">Secundaria:</th>
                @if($inscription->detailRegistration->registration->participant->user->id == "OTRO")
                <td colspan="1">X</td>
                @endif
                @if($inscription->detailRegistration->registration->participant->user->id != "OTRO")
                <td colspan="1"></td>
                @endif
                <th colspan="2">Superior:</th>
                @if($inscription->detailRegistration->registration->participant->user->id == "OTRO")
                <td colspan="1">X</td>
                @endif
                @if($inscription->detailRegistration->registration->participant->user->id != "OTRO")
                <td colspan="1"></td>
                @endif
            </tr>

            <!--LABOR INFORMATION / STUDIES-->
            <tr id="header-table">
                <th colspan="12" style="text-align: center; padding-top:10px">
                    <h6>INFORMACIÓN LABORAL / ESTUDIOS</h6>
                </th>
            </tr>

            <tr id="header-table">
                <th colspan="4">Institución donde trabaja / Estudia:</th>
                <td colspan="8">{{$inscription->detailRegistration->registration->additionalInformation->company_name}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="4">Dirección de la institución:</th>
                <td colspan="8">{{$inscription->detailRegistration->registration->additionalInformation->company_address}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="4">Correo electrónico de la institución:</th>
                <td colspan="8"></td>
            </tr>

            <tr id="header-table">
                <th colspan="4">Número telefónico de la institución:</th>
                <td colspan="8">{{$inscription->detailRegistration->registration->additionalInformation->company_phone}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="4">Actividad de la institución:</th>
                <td colspan="8">{{$inscription->detailRegistration->registration->additionalInformation->company_activity}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="4">¿El curso es auspiciado por la institución?:</th>
                <td colspan="2">Si:</td>
                @if($inscription->detailRegistration->registration->additionalInformation->company_sponsor == true)
                <td colspan="2">X</td>
                @endif
                @if($inscription->detailRegistration->registration->additionalInformation->company_sponsor != true)
                <td colspan="2"></td>
                @endif
                <th colspan="2">No:</th>
                @if($inscription->detailRegistration->registration->additionalInformation->company_sponsor == false)
                <td colspan="2">X</td>
                @endif
                @if($inscription->detailRegistration->registration->additionalInformation->company_sponsor != false)
                <td colspan="2"></td>
                @endif
            </tr>

            <tr id="header-table">
                <th colspan="5">Si está auspiciado, indique el nombre de contacto:</th>
                <td colspan="7">
                    @if($inscription->detailRegistration->registration->additionalInformation->company_sponsor == true)
                    {{$inscription->detailRegistration->registration->additionalInformation->name_contact}}
                    @endif
                    @if($inscription->detailRegistration->registration->additionalInformation->company_sponsor != true)

                    @endif
                </td>
            </tr>

            <tr id="header-table">
                <th colspan="5">¿Cómo se enteró del curso?</th>
            </tr>

            <tr id="header-table">
                <td colspan="7">
                    @foreach($inscription->detailRegistration->registration->additionalInformation->know_course as $knowCourse)
                    <p>{{$knowCourse}}</p>
                    @endforeach
                </td>
            </tr>


            <tr id="header-table">
                <th colspan="12">¿Qué otros cursos le gustaría seguir?</th>
            </tr>

            <tr id="header-table">
                <td colspan="7">
                    @foreach($inscription->detailRegistration->registration->additionalInformation->course_follow as $courseFollow)
                    <p>{{$courseFollow}}</p>
                    @endforeach
                </td>
            </tr>


            <tr id="line-table">
                <td colspan="6">________________________</td>
                <td colspan="6">________________________</td>
            </tr>

            <tr id="signing-taable">
                <th colspan="6">Firma del Solicitante</th>
                <th colspan="6">Nombre del responsable del registro:</th>
            </tr>


            <tr id="footer-table">
                <td colspan="12">
                    <p> <b>Nota:</b>
                        Para el caso de una inscripción fìsica, se tomará en cuenta la impresiòn del siguiente formulario,
                        en caso de que sea un registro digital, se solicita adjuntar unicamente la imagen de la firma del
                        solicitante en el documento para el archivo del Instituto.
                    </p>
                </td>
            </tr>
        </table>
        @endforeach
    </div>
</body>

</html>