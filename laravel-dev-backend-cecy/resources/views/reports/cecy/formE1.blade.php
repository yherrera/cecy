<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>REGISTRO DE PARTICIPANTES INSCRITOS Y MATRICULADOS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
        body{
            font-size: 9;
        }

        #img th {
            padding-top: 5px;
            padding-bottom: 1px;
            text-align: center;
            background-color: #b8cce4;
            color: #000;
            width: 100%;
        }

        #header-table th, #header-table td{
            border: 1px solid #ddd;
            width: 100%;
            padding-left: 5px;
        }

        #by-table td{
            padding-top: 90px;
        }

        #signing-taable th{
            padding-top: 30px;
            padding-bottom: 50px;
            text-align: left;
        }

        #footer-table td{
            border: 1px solid #000;
            padding-left: 30PX;
            padding-top: 10px;
            padding-right: 30px;
            text-align: justify;
        }
    </style>
<body>
<div style="overflow-x:auto;">
        @foreach($data as $detailPlanification)
        <table>
            <tr id="img">
                <th colspan="5">img</th>
                <th colspan="7" style="width: 350px;">
                    INSTITUTO TECNOLÓGICO SUPERIOR "{{$detailPlanification->detailRegistration->registration->planification->course->institution->name}}"
                    <h6>PROGRAMACIÓN DE CURSOS DE CAPACITACIÓN MENSUAL</h6>
                </th>
                <th colspan="5">img</th>
            </tr>


            <tr id="header-table">
                <th colspan="4">PROVINCIA</th>
                <td colspan="4"></td>
                <th colspan="3">TIPO DE CURSO</th>
                <td colspan="2">ADMINISTRATIVO:</td>
                <!--Administrativo-->
                @if($detailPlanification->detailRegistration->registration->planification->course->modality->name == "ADMINISTRATIVO")
                <td>X</td>
                @endif
                @if($detailPlanification->detailRegistration->registration->planification->course->modality->name != "ADMINISTRATIVO")
                <td></td>
                @endif
                <td colspan="2">TÉCNICO:</td>
                <!--TÉCNICO-->
                @if($detailPlanification->detailRegistration->registration->planification->course->modality->name == "TÉCNICO")
                <td>X</td>
                @endif
                @if($detailPlanification->detailRegistration->registration->planification->course->modality->name != "TÉCNICO")
                <td></td>
                @endif
            </tr>

            <tr id="header-table">
                <th colspan="4">CANTON</th>
                <td colspan="4"></td>
                <th colspan="3">MODALIDAD DEL CURSO:</th>
                <td colspan="2">PRESENCIAL:</td>
                <!--TÉCNICO-->
                @if($detailPlanification->detailRegistration->registration->planification->course->course->name == "PRESENCIAL")
                <td>X</td>
                @endif
                @if($detailPlanification->detailRegistration->registration->planification->course->modality->name != "PRESENCIAL")
                <td></td>
                @endif
                <td colspan="2">VIRTUAL:</td>
                <!--TÉCNICO-->
                @if($detailPlanification->detailRegistration->registration->planification->course->modality->name == "VIRTUAL")
                <td>X</td>
                @endif
                @if($detailPlanification->detailRegistration->registration->planification->course->modality->name != "VIRTUAL")
                <td></td>
                @endif
            </tr>
            
            <tr id="header-table">
                <th colspan="4">PARROQUIA</th>
                <td colspan="4"></td>
                <th colspan="3">DURACIÓN DEL CURSO:</th>
                <td colspan="6">{{$detailPlanification->detailRegistration->registration->planification->course->duration}} HORAS</td>
            </tr>

            <tr id="header-table">
                <th colspan="4">LOCAL DONDE SE DICTA</th>
                <td colspan="4">{{$detailPlanification->detailRegistration->registration->planification->course->local_proposal}}</td>
                <th colspan="3">FECHA DE INICIACION</th>
                <td colspan="6">{{$detailPlanification->date_start}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="4">CONVENIO</th>
                <td colspan="4"></td>
                <th colspan="4">FECHA PREVISTA DE FINALIZACION</th>
                <td colspan="1">{{$detailPlanification->detailRegistration->registration->planification->course->proposed_date}}</td>
                <th colspan="3">HORARIO DEL CURSO:</th>
                <td colspan="1">
                    @foreach($detailPlanification->detailRegistration->registration->planification->course->schedules as $schedule)
                    <p>{{$schedule->start_time}} - {{$schedule->end_time}}</p>
                    @endforeach
                </td>
            </tr>

            <tr id="header-table">
                <th colspan="4">NOMBRE DEL CURSO</th>
                <td colspan="4">{{$detailPlanification->detailRegistration->registration->planification->course->name}}</td>
                <th colspan="4">FECHA REAL DE FINALIZACION </th>
                <td colspan="1">{{$detailPlanification->date_end}}</td>
                <th colspan="3">CODIGO DEL CURSO:</th>
                <td colspan="1">{{$detailPlanification->detailRegistration->registration->planification->course->code}}</td>
            </tr>
            

            <tr id="header-table">
                <th colspan="1" rowspan="2">APELLIDOS Y NOMBRES</th>
                <th colspan="1" rowspan="2">DOCUMENTO DE IDENTIDAD</th>
                <th colspan="2" >SEXO</th>
                <th colspan="1" rowspan="2">EDAD / AÑOS</th>
                <th colspan="3">NIVEL DE INSTRUCCIÓN</th>
                <th colspan="4">DATOS DE LA EMPRESA</th>
                <th colspan="3">DATOS DEL PARTICIPANTE</th>
                <th colspan="2">RESULTADOS</th>
            </tr>

            <tr id="header-table">
                <th>M</th>
                <th>F</th>
                <th>PRI</th>
                <th>SEC</th>
                <th>SUP</th>
                <th>NOMBRE DE LA EMPRESA</th>
                <th>ACTIVIDAD DE LA EMPRESA</th>
                <th>DIRECCION  DE LA EMPRESA</th>
                <th>TELEFONO</th>
                <th>DIRECCION DOMICILIARIA</th>
                <th>CELULAR</th>
                <th>CONVEN</th>
                <th>INSCRITO</th>
                <th>MATRIC</th>
            </tr>

            
            <tr id="header-table">
                <!--Apellidos y nombres-->
                <td>{{$detailPlanification->detailRegistration->registration->participant->user->first_lastname}} {{$detailPlanification->detailRegistration->registration->participant->user->second_lastname}} {{$detailPlanification->detailRegistration->registration->participant->user->first_name}} {{$detailPlanification->detailRegistration->registration->participant->user->second_name}} </td>

                <!--Indentificación-->
                <td>{{$detailPlanification->detailRegistration->registration->participant->user->identification}}</td>

                <!--Masculino-->
                @if($detailPlanification->detailRegistration->registration->participant->user->gender->name == "MASCULINO")
                <td >X</td>
                @endif
                @if($detailPlanification->detailRegistration->registration->participant->user->gender->name != "MASCULINO")
                <td ></td>
                @endif
                <!--Femenino-->
                @if($detailPlanification->detailRegistration->registration->participant->user->gender->name == "FEMENINO")
                <td>X</td>
                @endif
                @if($detailPlanification->detailRegistration->registration->participant->user->gender->name != "FEMENINO")
                <td></td>
                @endif
                
                <!--Fecha de nacimientio-->
                <td>{{$detailPlanification->detailRegistration->registration->participant->user->birthdate}}</td>


                <!--Nivel de inscrucción-->
                @if($detailPlanification->detailRegistration->registration->additionalInformation->levelInstruction->name == "PRIMARIA")
                <td>X</td>
                @endif
                @if($detailPlanification->detailRegistration->registration->additionalInformation->levelInstruction->name != "PRIMARIA")
                <td></td>
                @endif

                @if($detailPlanification->detailRegistration->registration->additionalInformation->levelInstruction->name == "SECUNDARIA")
                <td>X</td>
                @endif
                @if($detailPlanification->detailRegistration->registration->additionalInformation->levelInstruction->name != "SECUNDARIA")
                <td></td>
                @endif

                @if($detailPlanification->detailRegistration->registration->additionalInformation->levelInstruction->name == "SUPERIOR")
                <td>X</td>
                @endif
                @if($detailPlanification->detailRegistration->registration->additionalInformation->levelInstruction->name != "SUPERIOR")
                <td></td>
                @endif

                <!--Datos empresa o institución-->
                <td>{{$detailPlanification->detailRegistration->registration->additionalInformation->company_name}}</td>
                <td>{{$detailPlanification->detailRegistration->registration->additionalInformation->company_activity}}</td>
                <td>{{$detailPlanification->detailRegistration->registration->additionalInformation->company_address}}</td>
                <td>{{$detailPlanification->detailRegistration->registration->additionalInformation->company_phone}}</td>
                
                <!--Datos del participante-->
                <td>{{$detailPlanification->detailRegistration->id}}</td>
                <td>{{$detailPlanification->detailRegistration->id}}</td>
                <td>{{$detailPlanification->detailRegistration->id}}</td>

                <!--Resultado-->
                <td>{{$detailPlanification->detailRegistration->id}}</td>
                <td>{{$detailPlanification->detailRegistration->id}}</td>
            </tr>            


            <tr id="line-table signing-taable">
                <th colspan="8">FECHA DE ELABORACION ____________________________</th>
                <th colspan="9">RESPONSABLE DE ELABORACION __________________________</th>    
            </tr>

            <tr id="footer-table">
                <td colspan="17"> 
                    <p> <b>Nota:</b> 
                    Este formulario es para uso directo del Docente Responsable del Curso de Capacitación, 
                    como archvo digital, puede hacer uso del mismo colocando las firmas de responsabilidad 
                    como imagen dento del documento Excell. 
                    </p>
                </td>
            </tr>
        </table>
        @endforeach
    </div>
</body>

</html>