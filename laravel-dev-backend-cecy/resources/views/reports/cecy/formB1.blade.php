<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INFORME DE NECESIDAD DEL CURSO</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
    body {
        font-size: 9;
    }

    #img th {
        padding-top: 5px;
        padding-bottom: 1px;
        text-align: center;
        background-color: #b8cce4;
        color: #000;
        width: 100%;
    }

    #table-title th {
        border: 1px solid #ddd;
        padding-top: 5px;
        text-align: center;
        background-color: #b8cce4;
        color: #000;
        width: 100%;
    }

    #header-table th,
    #header-table td {
        border: 1px solid #ddd;
        width: 100%;
        padding-top: 5px;
        padding-left: 5px;
    }

    #line-table td {
        padding-top: 80px;
        padding-left: 5px;
    }

    #signing-taable th {
        padding-bottom: 50px;
        padding-left: 5px;
    }

    #information-signing-table th,
    #information-signing-table td {
        padding-left: 5px;
    }

    #footer-table td {
        border: 1px solid #000;
        padding-left: 30PX;
        padding-top: 10px;
        padding-right: 30px;
        text-align: justify;
    }
</style>

<body>
<div style="overflow-x:auto;">
        @foreach($data as $curricular)
        <table>
            <tr id="img">
                <th colspan="4">img</th>
                <th colspan="5">
                    <h6>DIRECCIÓN DE CALIFICACIÓN Y RECONOCIMIENTO
                        FORMULARIO DE DISEÑO CURRICULAR</h6>
                    - CAPACITACIÓN CONTINUA -
                </th>
                <th colspan="3">Img</th>

            </tr>

            <tr id="header-table">
                <th colspan="4">Identificación del Curso.</th>
                <td colspan="8">{{$curricular->code}}</td>
            </tr>

            <tr>
                <th colspan="12" style="padding-top: 10px;"></th>
            </tr>
            <tr id="table-title">
                <th colspan="4">Nombre del curso</th>
                <th colspan="4">Nombre del area</th>
                <th colspan="4">Detalle de la especialidad</th>
            </tr>

            <tr id="header-table">
                <td colspan="4">{{$curricular->name}}</td>
                <td colspan="4">{{$curricular->area->name}}</td>
                <td colspan="4">{{$curricular->specialty->name}}</td>
            </tr>

            <tr>
                <th colspan="12" style="padding-top: 10px;"></th>
            </tr>
            <tr id="table-title">
                <th colspan="4">Tipo de Participante</th>
                <th colspan="4">Modalidades</th>
                <th colspan="4">Duración</th>
            </tr>

            <tr id="header-table">
                <td colspan="4">{{$curricular->area->id}}</td>
                <td colspan="4">{{$curricular->modality->name}}</td>
                <td colspan="4">{{$curricular->duration}} Horas</td>
            </tr>

            <tr id="header-table">
                <th colspan="12">Requisitos mínimos de entrada al Curso.</th>
            </tr>
            <tr id="header-table">
                <td colspan="12"></td>
            </tr>

            <tr id="header-table">
                <td colspan="12"></td>
            </tr>

            <tr id="header-table">
                <th colspan="12">Objetivo del Curso.</th>
            </tr>

            <tr id="header-table">
                <td colspan="12">{{$curricular->objective}}</td>
            </tr>

            <tr id="header-table">
                <th colspan="12">Contenidos del Curso.</th>
            </tr>
            <tr id="header-table">
                <th colspan="12" style="font-style:italic;">Temas Principales.</th>
            </tr>
            <tr id="header-table">
                <td colspan="12">
                    @foreach($curricular->main_topics as $topics)
                    <p>{{$topics}}</p>
                    @endforeach
                </td>
            </tr>
            <tr id="header-table">
                <th colspan="12" style="font-style:italic;">Temas secundarios o sub temas.</th>
            </tr>
            <tr id="header-table">
                <td colspan="12">
                    @foreach($curricular->secondary_topics as $subtopics)
                    <p>{{$subtopics}}</p>
                    @endforeach
                </td>
            </tr>
            <tr id="header-table">
                <th colspan="12" style="font-style:italic;">Temas Transversales.</th>
            </tr>
            <tr id="header-table">
                <td colspan="12">
                    @foreach($curricular->cross_cutting_topics as $crossTopics)
                    <p>{{$crossTopics}}</p>
                    @endforeach
                </td>
            </tr>

            <tr id="header-table">
                <th colspan="12">Estrategias de enseñanza - aprendizaje.</th>
            </tr>
            <tr id="header-table">
                <td colspan="12"></td>
            </tr>

            <tr id="header-table">
                <th colspan="12">Mecanismos de evaluación. </th>
            </tr>
            <tr>
                <th colspan="12" style="padding-top: 10px;"></th>
            </tr>
            <tr id="table-title">
                <th colspan="4">Evaluación diagnóstica</th>
                <th colspan="4">Evaluación proceso formativo </th>
                <th colspan="4">Evaluación final</th>
            </tr>
            <tr id="table-title">
                <th colspan="2">Técnica</th>
                <th colspan="2">Instrumento</th>
                <th colspan="2">Técnica </th>
                <th colspan="2">Instrumento</th>
                <th colspan="2">Técnica</th>
                <th colspan="2">Instrumento</th>
            </tr>


            <tr id="header-table">
                <td colspan="2"></td>
                <td colspan="2"></td>
                <td colspan="2"></td>
                <td colspan="2"></td>
                <td colspan="2"></td>
                <td colspan="2"></td>
            </tr>



            <tr id="header-table">
                <th colspan="12">Entorno de Aprendizaje</th>
            </tr>
            <tr>
                <th colspan="12" style="padding-top: 10px;"></th>
            </tr>
            <tr id="table-title">
                <th colspan="4">Instalaciones</th>
                <th colspan="4">Fase teórica</th>
                <th colspan="4">Fase práctica</th>
            </tr>

            <tr id="header-table">
                <td colspan="4">
                    @foreach($curricular->facilities as $facilitie)
                    <p>{{$facilitie}}</p>
                    @endforeach
                </td>
                <td colspan="4">
                    @foreach($curricular->theoretical_phase as $theoreticalphase)
                    <p>{{$theoreticalphase}}</p>
                    @endforeach
                </td>
                <td colspan="4">
                    @foreach($curricular->practical_phase as $practicalPhase)
                    <p>{{$practicalPhase}}</p>
                    @endforeach
                </td>
            </tr>

            <tr>
                <th colspan="12" style="padding-top: 10px;"></th>
            </tr>
            <tr id="table-title">
                <th colspan="4" rowspan="3">Carga horaria:</th>
            </tr>

            <tr id="header-table">
                <td colspan="4">Horas prácticas</td>
                <td colspan="4">{{$curricular->practice_hours}}</td>
            </tr>

            <tr id="header-table">
                <td colspan="4">Horas teóricas</td>
                <td colspan="4">{{$curricular->theory_hours}}</td>
            </tr>

            <tr id="table-title header-table">
                <th colspan="4">Bibliografía:</th>
                <td colspan="8">
                    @foreach($curricular->practical_phase as $practicalPhase)
                    <p>{{$practicalPhase}}</p>
                    @endforeach
                </td>
            </tr>
            <tr id="line-table">
                <td colspan="12">_________________________________</td>
            </tr>

            <tr id="signing-taable">
                <th colspan="12">firma del responsable del Diseño Curricular </th>
            </tr>

            <tr id="information-signing-table">
                <th colspan="4">Nombre y Apellido:</th>
                <td colspan="8"></td>
            </tr>

            <tr id="information-signing-table">
                <th colspan="4">Cédula No.:</th>
                <td colspan="8"></td>
            </tr>

            <tr id="information-signing-table">
                <th colspan="4">Indice Dactilar:</th>
                <td colspan="8"></td>
            </tr>

            <tr>
                <th colspan="12" style="padding-top: 30px;"></th>
            </tr>

            <tr id="footer-table">
                <td colspan="12">
                    <p> <b>Nota:</b>
                        En el caso de disponer sólo en archivos digitales, se puede insertar las
                        firmas de resposabilidad como imagen en las celdas correspondientes.
                    </p>
                </td>
            </tr>
        </table>
        @endforeach
    </div>
</body>

</html>