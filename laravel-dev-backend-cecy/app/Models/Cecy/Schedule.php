<?php

namespace App\Models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Cecy\Course;
use App\Models\Ignug\State;
use App\Models\Cecy\Catalogue;

class Schedule extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;

    protected $connection = 'pgsql-cecy';
    protected $table = 'cecy.schedules';
    protected $fillable = [
        'start_time',
        'end_time',
       
    ];
    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }
    public function day()
    {
        return $this->belongsTo(Catalogue::class, 'day_id');
    }
    /* public function scheduleable(){
        return $this->morphedByMany();
    } */
    public function courses(){
        return $this->morphedByMany(Course::class, 'scheduleable');
    }
}
