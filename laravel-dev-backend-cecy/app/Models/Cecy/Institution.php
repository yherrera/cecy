<?php

namespace App\Models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Ignug\State;

use App\Models\Cecy\Authority;

class Institution extends Model  implements Auditable
{

    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;


    protected $connection = 'pgsql-cecy';
    protected $fillable = [
        'name',
        'logo',
        'slogan',
        'code',
        'ruc'        
    ];

    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }

    public function authority()
    {
        return $this->belongsTo(Authority::class,'authority_id');
    }

}
