<?php

namespace App\Models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Ignug\State;
use App\Models\Cecy\Catalogue;
use App\Models\Cecy\Registration;

class AdditionalInformation extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;

    protected $connection = 'pgsql-cecy';
    protected $table = 'cecy.additional_informations';
    protected $fillable = [
        'company_name',
        'company_address',
        'company_phone',
        'company_activity',
        'company_sponsor',
        'name_contact',
        'know_course',
        'course_follow',
        'works',
    ];

    protected $casts = [
        'know_course' => 'array',  
        'course_follow' => 'array',
    ];

    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }
    /* public function registration()
    {
        return $this->belongsTo(Registration::class, 'registration_id');
    } */
    public function levelInstruction()
    {
        return $this->belongsTo(Catalogue::class, 'level_instruction');
    }
}
