<?php

namespace App\Models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Cecy\Catalogue;
use App\Models\Ignug\State;

class EvaluationMechanism extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;

    protected $connection = 'pgsql-cecy';
    protected $table = 'cecy.evaluation_mechanisms';
    protected $fillable = [
        'technique',
        'instrument'
    ];
    public function state()
    {
        return $this->belongsTo(State::class);
    }
    public function Type()
    {
        return $this->belongsTo(Catalogue::class);
    }
}
