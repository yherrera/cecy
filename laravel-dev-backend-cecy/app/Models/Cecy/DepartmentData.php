<?php

namespace App\Models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DepartmentData extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;


    protected $connection = 'pgsql-cecy';
    protected $fillable = [
        'name',
        'address',
    ];
    public function user()
    {
        return $this->belongsTo(Instructor::class,'charge_id');
    }
    public function schedule()
    {
        return $this->belongsTo(Catalogue::class,'schedule_id');
    }
    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }
}
