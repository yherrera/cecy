<?php

namespace App\Models\Cecy;


use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Cecy\Catalogue;
use App\Models\Ignug\State;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Authentication\User;


class Participant extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;


    protected $connection = 'pgsql-cecy';
    protected $fillable = [];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }

    public function personType()
    {
        return $this->belongsTo(Catalogue::class,'person_type_id');
    }
}
