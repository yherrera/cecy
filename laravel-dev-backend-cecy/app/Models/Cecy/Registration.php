<?php

namespace App\Models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Ignug\State;
use App\Models\Cecy\Catalogue;
use App\Models\Cecy\Participant;
use App\Models\Cecy\AdditionalInformation;


class Registration extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;


    protected $connection = 'pgsql-cecy';
    protected $fillable = [
        'date_registration',
        'number',
    ];
    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }
    public function participant()
    {
        return $this->belongsTo(Participant::class,'participant_id');
    }
    public function registration()
    {
        return $this->hasMany(Catalogue::class, 'type_id');
    }
    public function planification()
    {
        return $this->belongsTo(Planification::class, 'planification_id');
    }
    public function additionalInformation()
    {
        return $this->belongsTo(AdditionalInformation::class, 'additional_information_id');
    }
}
