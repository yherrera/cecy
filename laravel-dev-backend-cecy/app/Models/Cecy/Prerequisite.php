<?php

namespace App\Models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Cecy\EvaluationMechanism;
use App\Models\Cecy\Course;
use App\Models\Ignug\State;

class Prerequisite extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;

    protected $connection = 'pgsql-cecy';
    protected $table = 'cecy.prerequisites';
    protected $fillable = [
        
    ];
    public function state()
    {
        return $this->belongsTo(State::class);
    }
    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function parents()
    {
        return $this->belongsTo(EvaluationMechanism::class);
    }
}
