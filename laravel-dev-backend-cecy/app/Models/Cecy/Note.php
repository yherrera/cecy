<?php

namespace App\models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Cecy\Registration;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{   
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;

    
    protected $connection = 'pgsql-cecy';

    public function registration()
    {
        return $this->belongsTo(Registration::class, 'id_registration');
    }
}
