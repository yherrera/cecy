<?php

namespace App\models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Cecy\Registration;

class Assistance extends Model
{

    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;

    protected $connection = 'pgsql-cecy';
    protected $table = 'cecy.assistances';
    public function registration()
    {
        return $this->belongsTo(Registration::class, 'id_registration');
    }
}
