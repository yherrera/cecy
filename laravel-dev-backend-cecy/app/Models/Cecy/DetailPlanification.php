<?php

namespace App\Models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Cecy\Planification;
use App\Models\Cecy\Course;
use App\Models\Cecy\SchoolPeriod;
use App\Models\Cecy\DetailRegistration;
use App\Models\Cecy\Instructor;
use App\Models\Ignug\State;
use App\Models\Ignug\Catalogue;

class DetailPlanification extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;

    protected $connection = 'pgsql-cecy';
    protected $fillable = [
        'date_start',
        'date_end',
        'sumarry',
        'planned_end_date',
        'capacity',
        'observation',
        'needs',
        'need_date'
    ];
    protected $casts = [
        'needs'=>'array',
    ];
    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }
    public function instructor()
    {
        return $this->belongsTo(Instructor::class,'instructor_id');
    }
    public function course()
    {
        return $this->belongsTo(Course::class,'course_id');
    }
    public function detailRegistration()
    {
        return $this->belongsTo(DetailRegistration::class, 'detail_registration_id');
    }
    public function planification()
    {
        return $this->belongsTo(Planification::class,'planification_id');
    }
    public function school_period()
    {
        return $this->hasMany(SchoolPeriod::class,'school_period_id');
    }
    public function conference()
    {
        return $this->belongsTo(Catalogue::class,'conference');
    }
    public function parallel()
    {
        return $this->belongsTo(Catalogue::class,'parallel');
    }
}
