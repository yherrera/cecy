<?php

namespace App\Models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Ignug\State;
use App\Models\Cecy\Catalogue as CecyCatalogue;
use App\Models\Ignug\Catalogue;
use App\Models\Ignug\Institution;
use App\Models\Authentication\User;

class Course extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;


    protected $connection = 'pgsql-cecy';
    protected $table = 'cecy.courses';
    protected $fillable = [
        'code',
        'name',
        'cost',
        'photo',
        //'summary',
        'duration',
        'free',
        'observation',
        'objective',
        //'needs' => 'array',
        'facilities' => 'array',
        'theoretical_phase' => 'array',
        'practical_phase' => 'array',
        'main_topics' => 'array',
        'secondary_topics'=> 'array',
        'cross_cutting_topics' => 'array',
        'bibliography',
        'teaching_strategies' => 'array',
        'required_installing_sources',
        'practice_hours',
        'theory_hours',
        'practice_required_resources',
        'aimtheory_required_resources',
        'learning_teaching_strategy',
        'proposed_date',
        'approval_date',
        //'need_date',
        'local_proposal',
        'project',
        'capacity',
        'setec_name'
    ];

    protected $casts = [
        'facilities' => 'array',
        'theoretical_phase' => 'array',
        'practical_phase' => 'array',
        'main_topics' => 'array',
        'secondary_topics'=> 'array',
        'cross_cutting_topics' => 'array',
        'bibliography' => 'array',
        'teaching_strategies' => 'array'  
    ];

    public function modality()
    {
        return $this->belongsTo(CecyCatalogue::class, 'modality_id');
    }
    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }
    public function type()
    {
        return $this->belongsTo(Catalogue::class, 'participant_type_id');
    }
    public function area()
    {
        return $this->belongsTo(CecyCatalogue::class, 'area_id');
    }
    public function level()
    {
        return $this->belongsTo(Catalogue::class, 'level_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'person_proposal_id');
    }
    public function schedules()
    {
        return $this->morphToMany(Schedule::class,'scheduleable');
    }
    // public function classroom()
    // {
    //     return $this->belongsTo(Classroom::class,'classroom_id');
    // }
    public function course()
    {
        return $this->belongsTo(CecyCatalogue::class,'course_type_id');
    }
    public function specialty()
    {
        return $this->belongsTo(CecyCatalogue::class,'specialty_id');
    }
    public function institution()
    {
        return $this->belongsTo(Institution::class,'institution_id');
    }
    public function period()
    {
        return $this->belongsTo(Catalogue::class,'academic_period_id');
    }
}
