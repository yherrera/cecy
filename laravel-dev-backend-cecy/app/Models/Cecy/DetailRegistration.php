<?php

namespace App\Models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Cecy\Catalogue;
use App\Models\Cecy\Registration;
use App\Models\Ignug\State;

class DetailRegistration extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;


    protected $connection = 'pgsql-cecy';
    protected $fillable = [
        'final_grade',
        'certificate_withdrawn',
        'observation' => 'array',
    ];

    protected $casts = [
        'observation' => 'array',
    ];

    public function registration()
    {
        return $this->belongsTo(Registration::class,'registration_id');
    }
    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }
    public function status()
    {
        return $this->belongsTo(Catalogue::class,'status_id');
    }
    public function statusCertificate()
    {
        return $this->belongsTo(Catalogue::class,'status_certificate_id');
    }
}
