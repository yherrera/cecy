<?php

namespace App\Models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class AgreementCompany extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;


    protected $connection = 'pgsql-cecy';
    protected $table = 'cecy.agreement_companies';
    protected $fillable = [
        'objective',
        'date_agreement_signature',
        'expiry_date',
        'representative',
        'social_reason'
    ];
    public function agreements()
    {
        return $this->hasMany(Agreement::class,'agreement_id');
    }
    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }
}
