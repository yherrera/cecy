<?php

namespace App\Models\Cecy;

use App\Traits\StatusActiveTrait;
use App\Traits\StatusDeletedTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Cecy\Course;
use App\Models\Cecy\SchoolPeriod;
use App\Models\Ignug\State;


class Planification extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    use StatusActiveTrait;
    use StatusDeletedTrait;

    protected $connection = 'pgsql-cecy';
    protected $table = 'cecy.planifications';
    protected $fillable = [
        'date_start',
        'date_end',
        'needs'
    ];
    protected $casts = [
        'needs'=>'array',
        
    ];
    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }
    public function statu()
    {
        return $this->belongsTo(Statu::class,'status_id');
    }
    public function course()
    {
        return $this->belongsTo(Course::class,'course_id');
    }
    public function school_period()
    {
        return $this->hasMany(SchoolPeriod::class,'school_period_id');
    }
}
