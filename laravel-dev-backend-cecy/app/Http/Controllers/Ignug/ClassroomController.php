<?php

namespace App\Http\Controllers\Ignug;

 use Intervention\Image\ImageManagerStatic as Img ;
use App\Http\Controllers\Controller;
use App\Models\Ignug\Classroom;
use App\Models\Ignug\State;
use App\Models\Ignug\Image;

use App\Models\Ignug\Catalogue;
use Illuminate\Http\Request;
use PhpParser\Builder\Class_;

class ClassroomController extends Controller
{
    public function index(Request $request)
    {
        //$classroom = Classroom::all();
        $classroom = Classroom::with('type','state')->get();

        return response()->json([
            'data' => $classroom,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }
    

    public function filter(Request $request)
    {
        $classroom = Classroom::where('id', $request->id)->orderBy('id')->get();
        return response()->json([
            'data' => $classroom,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }
    public function show( $id)
    { 
        $classroom=Classroom::find($id);
        return response()->json([
            'data' => $classroom,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
  
    }

    public function store(Request $request)
    {

        $data = $request->json()->all();
        $dataType =  $data['type'];
        $dataClassroom = $data['classroom'];

        $classroom = new Classroom( $dataClassroom );
        $classroom->name = $dataClassroom['name'];
        $classroom->code = $dataClassroom['code'];
        $classroom->icon='/app/public/cecy/';
        $classroom->capacity = $dataClassroom['capacity'];
         
    
        $classroom-> type()->associate(Catalogue::findorfail($dataType['id']));
        $classroom-> state()->associate(State::where('code', '1')->first());
        
        $classroom -> save();


        return response()->json([
            'data' => $classroom,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }

    public function update(Request $request, $id)
    {
        $data = $request->json()->all();
        $dataType =  $data['type'];
        $dataClassroom = $data['classroom'];

        $classroom = Classroom::findOrfail($id);   
        $classroom->name = $dataClassroom['name'];
        //$classroom->code = $dataClassroom['code'];
        $classroom->capacity = $dataClassroom['capacity'];

        $type = Catalogue::findOrFail($dataType);
        $classroom-> type()->associate(Catalogue::findorfail($dataType['id']));
        
        $classroom->save();
        return response()->json([
            'data' => $classroom,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
       
    }

    public function destroy( Request $request, $id )
    {
        $state = State::where('code', '2')->first();
        $classroom = Classroom::findOrFail($id);
        $classroom->state_id = 2;
        $classroom->save();
        return response()->json([
            'data' => $classroom,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }

    public function upload(Request $request )
    {
        $image    = $request->image;

        $nombre     = 'logo.'.$image->guessExtension();
    
        $filePath=storage_path().'/app/public/cecy/'.($nombre);
    
        $img = Img::make($image->getRealPath())
            ->resize(100,100)->save($filePath);

            $classroom = Classroom::findOrFail($request->classroom_id);
            $logo = $classroom->images()->first();
            if (!$logo) {
                $logo = new Image([
                    'code' => 'asd',
                    'name' => 'Avatar',
                    'description' => 'Avatar',
                    'type' => 'logo',
                    'extension' => $image->guessExtension(),
                    'uri' => $filePath,
                ]);
                $logo->imageable()->associate($classroom);
                $logo->state()->associate(State::firstWhere('code', State::ACTIVE));
                $logo->save();
            }



            return response()->json([
                'data' => $image,
                'msg' => [
                    'summary' => 'success',
                    'detail' => '',
                    'code' => '200',
                ]], 200);
    }


}
    