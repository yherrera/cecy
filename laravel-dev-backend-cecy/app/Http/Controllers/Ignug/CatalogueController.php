<?php

namespace App\Http\Controllers\Ignug;

use App\Http\Controllers\Controller;
use App\Models\Ignug\Catalogue;
use App\Models\Ignug\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CatalogueController extends Controller
{
    protected $catalogue;

    public function __construct()
    {
        $this->catalogue = new Catalogue();
    }

    //Metodo get
    public function index()
    {
        $catalogue = $this->catalogue
            ->from('ignug.catalogues as s')
            ->select(
                DB::raw("(select name FROM ignug.catalogues as d where s.parent_id = d.id) as name_parent"),
                's.id as id',
                's.parent_id as parent_id',
                's.code as code',
                's.name as name',
                's.type as type',
                's.description as description',
                's.icon as icon',
                's.state_id as state_id',
                's.created_state as created_state'
            )
            ->where('state_id', 1)
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json([
            'data' => [
                'catalogues' => $catalogue,
            ],
        ]);
    }

    public function catalogue(Request $request)
    {
        $catalogues = Catalogue::all();
        return response()->json( $catalogues);

        return response()->json([
            'data' => [
                'catalogues' => $catalogues
            ],
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200'
            ]]);
    }

    //Metodo previo para actualizar
    public function show($id)
    {
        $catalogue = $this->catalogue
            ->where('state_id', 1)
            ->where('id', $id)
            ->get();
        return response()->json([
            'data' => [
                'catalogue' => $catalogue,
            ],
        ]);
    }

    //Metodo para guradar
    public function store(Request $request)
    {
        $data = $request->json()->all();

        //Campo parent_id
        $parentCode = $data['parent_code'];

        //campo code
        $arraType = [];
        $arraTypeNull = [];
        $arraTypeName = [];
        $codeCatalogue = $this->catalogue->all();

        //Validamos si el catálogo es un catálogo padre
        if ($parentCode === 'null') {
            $codeCatalogueNull = $this->catalogue->whereNull('parent_id')->get();

            foreach ($codeCatalogueNull as $item) {
                array_push($arraTypeNull, $item->id);
            }

            $contNull = count($arraTypeNull);

            if (empty($contNull)) {
                $contNull = 0;
            } else {
                $contNull = $contNull;
            }

            $contNull++;

            if ($contNull < 10) {
                $this->catalogue->code = $contNull;
            } else {
                $this->catalogue->code = $contNull;
            }
            //Validamos si el catálogo es ítem y no necesita una clase padre
        } 
        

        else if ($parentCode === 'parent_null') {

            
            foreach ($codeCatalogue as $item) {
                if ($data['type'] === $item->type) {
                    array_push($arraTypeName, $item->type);
                }
            }

            $contType = count($arraTypeName);

            if (empty($contType)) {
                $contType = 0;
            } else {
                $contType = $contType;
            }

            $contType++;

            if ($contType < 10) {
                $this->catalogue->code = $contType;
            } else {
                $this->catalogue->code = $contType;
            }
            //Validamos que el catálogo es un catálogo ítem
        } 
        
        else {

            $parent_id = $this->catalogue->where('name', $parentCode)->first();
            $this->catalogue->parent_id = $parent_id['id'];

            foreach ($codeCatalogue as $item) {
                if ($parent_id['id'] === $item->parent_id) {
                    array_push($arraType, $item->id);
                }
            }

            $cont = count($arraType);

            if (empty($cont)) {
                $cont = 0;
            } else {
                $cont = $cont;
            }

            $cont++;

            if ($cont < 10) {
                $this->catalogue->code = $cont;
            } else {
                $this->catalogue->code = $cont;
            }
        }

        //Campo name
        $this->catalogue->name = $data['name'];

        //Campo type
        $type_catalogue = $data['type'];
        $this->catalogue->type = strtoupper($type_catalogue);

        //Campo icon
        $this->catalogue->icon = $data['icon'];

        //Campo color
        $this->catalogue->color = $data['color'];

        //Campo description
        $this->catalogue->description = $data['description'];

        //Campo estado y status
        $state = State::where('code', '1')->first();
        $this->catalogue->state()->associate($state);
        $this->catalogue->state_id = $state['code'];
 

        //Guardamos la información recolectada en la base de datos
        $this->catalogue->save();

        return response()->json(
            [
                'data' => [
                    'catalogues' => $this->catalogue,
                ],
            ],
            201
        );
    }
    // Función para actualizar catálogos
    public function update(Request $request)
    {
        $data_request = $request->json()->all();
        $id = $data_request['id'];
        $data = $this->catalogue->where('id', $id)->first();

        if ($data_request['icon'] === 'null' || $data_request['icon'] === "Ninguno") {
            $data->icon = null;
        } else {
            $data->icon = $data_request['icon'];
        }

        $data->name = $data_request['name'];
        $data->color = $data_request['color'];
        $data->description = $data_request['description'];

        $data->save();
        return response()->json(
            [
                'data' => [
                    'catalogue' => $data,
                ],
            ],
            201
        );
    }

    // Función para eliminar catálogos
    public function destroy(Request $request)
    {
        $data = $request->json()->all();
        //parent_id
        $parentCode = $data['id'];

        $arrayParent = $this->catalogue->where('id', $parentCode)->first();

        //estado
        $state = State::where('code', '0')->first();
        $this->catalogue->state()->associate($state);
        $this->catalogue->state_id = $state['code'];
        $parent_id = $arrayParent['parent_id'];

        $array = [$arrayParent];

        if (count($array) >= 1 && $parent_id === null) {

            $arrayParentItems = $this->catalogue->where('parent_id', $parentCode)->get();

            foreach ($arrayParentItems as $comand) {

                $comand->state_id = $this->catalogue->state_id;
                
                $comand->save();
            }

            $arrayParent->state_id = $this->catalogue->state_id;
            
            
            $arrayParent->save();

            return response()->json(
                [
                    'data' => [
                        'catalogue' => $this->catalogue,
                    ],
                ],
                201
            );
        } else {
            $arrayParent->state_id = $this->catalogue->state_id;
            
            $arrayParent->save();

            return response()->json(
                [
                    'data' => [
                        'catalogue' => $this->catalogue,
                    ],
                ],
                201
            );
        }
    }

    // Funcion para deshabilitar catálogos
    public function updateIdCatalogue(Request $request)
    {
        $data = $request->json()->all();
        $parentCode = $data['id'];

        $array_data = explode(",", $parentCode);
        $array_unique = array_unique($array_data);

        //estado
        $state = State::where('code', '0')->first();
        $this->catalogue->state()->associate($state);
        $this->catalogue->state_id = $state['code'];

        foreach ($array_unique as $item) {
            if ($item !== 'VACIO') {
                $arrayParent = $this->catalogue->where('id', $item)->get();

                foreach ($arrayParent as $element) {
                    $element->state_id = $this->catalogue->state_id;
                    
                    $element->save();
                }
            }
        }

        return response()->json(
            [
                'data' => [
                    'catalogue' => $data,
                ],
            ],
            201
        );
    }

    // Función para obtener todos los catálogos deshabilitados
    public function restoreCatalogue()
    {
        $catalogue = $this->catalogue
            ->where('state_id', 0)
            ->get();

        return response()->json([
            'data' => [
                'catalogues' => $catalogue,
            ],
        ]);
    }

    // Función para habilitar catálogos deshabilitados en la base de datos
    public function updateCatalogue(Request $request)
    {
        $data = $request->json()->all();
        $parentCode = $data['id'];
        $array_data = explode(",", $parentCode);
        $array_unique = array_unique($array_data);

        //estado
        $state = State::where('code', '1')->first();
        $this->catalogue->state()->associate($state);
        $this->catalogue->state_id = $state['code'];

        foreach ($array_unique as $item) {
            $arrayParent = $this->catalogue->where('id', $item)->get();

            foreach ($arrayParent as $element) {
                $element->state_id = $this->catalogue->state_id;
                $element->save();
            }
        }

        return response()->json(
            [
                'data' => [
                    'catalogue' => $this->catalogue,
                ],
            ],
            201
        );
    }

    //Función para agregar la etiqueta -- nuevo --- al catálogo creado
    public function updateCreatedCatalogue(Request $request)
    {
        $data = $request->json()->all();
        $id = $data['id'];

        $data = $this->catalogue->where('id', $id)->first();
        $data->created_at = $data->created_at;
        $data->updated_at = $data->updated_at;
        $data->created_state = 'VISTO';
        $data->save();

        return response()->json(
            [
                'data' => [
                    'catalogue' => $data,
                ],
            ],
            201
        );
    }

    public function updateAllCreatedCatalogue(Request $request)
    {
        $data = $request->json()->all();
        $estado = $data['id'];

        if($estado === 'VISTO'){

            $arrayCheck = $this->catalogue->get();

            foreach ($arrayCheck as $item) {
            
                $item->created_state = 'VISTO';
                $item->save();
            }
        }
    }

    //Funcion para eliminar caracteresm especiales
    public function decodeString($str)
    {
        $string = str_replace(['\'', '"', ',', ';', '<', '>'], ' ', $str);
        return json_encode($string);
    }
}
