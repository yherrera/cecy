<?php

namespace App\Http\Controllers\Cecy;

use App\Http\Controllers\Controller;
use App\Models\Cecy\Participant;
use Illuminate\Http\Request;

class ParticipantController extends Controller
{
    public function index(Request $request)
    {
        $participant = Participant::with('personType','user')->get();

        return response()->json([
                'data' => [
                    'type' => 'participant',
                    'attributes' => $participant
                ]]
            , 200);
    }

    public function filter(Request $request)
    {
        $participant = Participant::where('free', $request->free)->orderBy('id')->get();
        return response()->json([
                'data' => [
                    'type' => 'participant',
                    'attributes' => $participant
                ]]
            , 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        Participant::create($data);
        return response()->json([
            'data' => [
                'attributes' => $data,
                'type' => 'participant'
            ]
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $Participant = Participant::where('id', $id)->update($data);
        return response()->json([
            'data' => [
                'type' => 'participant',
                'attributes' => $data
            ]
        ], 200);
    }

    public function destroy($id)
    {
        $participant = Participant::findOrFail($id);
        $participant->state_id = 2;
        $participant->save();
        return response()->json([
            'data' => $participant,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }
}
