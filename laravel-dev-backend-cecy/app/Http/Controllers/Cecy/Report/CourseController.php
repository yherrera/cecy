<?php

namespace App\Http\Controllers\Cecy\Report;

use Illuminate\Support\Facades\App;
use App\Cecy\Exports\CoursesExport;
use App\Http\Controllers\Controller;
use App\Models\Cecy\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

// pdf
use Barryvdh\DomPDF\Facade as PDF;


class CourseController extends Controller
{
    
    public function index(Request $request)
    {
        $courses = Course::with('area')->with('specialty')->with('period')
        ->with('level')->with('course')->with('modality')
        ->with('type')->with('institution')->with('schedules')
        ->get();
        
        return response()->json([
                'data' => $courses
                ], 200);
    }


    public function filter(Request $request)
    {
        $courses = Course::where('id', $request->id)->get();
        return response()->json([
            'data' => $courses
        ], 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        Course::create($data);
        return response()->json([
            'data' => [
                'attributes' => $data,
                'type' => 'courses'
            ]
        ], 201);
    }

    public function update(Request $request, $id, Course $Course)
    {
        $data = $request->all();

        $Course = Course::where('id', $id)->update($data);
        return response()->json([
            'data' => [
                'type' => 'courses',
                'attributes' => $data
            ]
        ], 200);
    }

    public function destroy($id)
    {
        $courses = Course::destroy($id);
        return response()->json([
            'data' => [
                'attributes' => $id,
                'type' => 'courses'
            ]
        ], 201);
    }


    //export PDF

    public function photographicRegisterPDF(Request $request)
    {
        $courses = Course::where('id', $request->id)->get();
        
            $pdf = new PDF();
            $pdf = PDF::loadView('reports.cecy.photographicRegister',['data' => $courses]);
            //$pdf->setPaper('A4', 'landscape'); //orietation sheet
            //$pdf->save(Storage::path('NAME_PATH.pdf')); //use storage
            return $pdf->stream('REGISTRO FOTOGRÁFICO.pdf');
            //return $pdf->download('NAME_PDF.pdf'); //only dowloand pdf
    }

    public function curricularDesignPDF(Request $request)
    {
        $courses = Course::where('id', $request->id)->get();
        
        $pdf = new PDF();
        $pdf = PDF::loadView('reports.cecy.formB1',['data' => $courses]);
        return $pdf->stream('INFORME DE NECESIDAD DEL CURSO.pdf');
    }
}
