<?php

namespace App\Http\Controllers\Cecy\Report;

use App\Http\Controllers\Controller;
use App\Models\Cecy\DetailPlanification;
use Illuminate\Http\Request;

// pdf
use Barryvdh\DomPDF\Facade as PDF;

class DetailPlanificationController extends Controller
{
    public function index(Request $request)
    {   
        $detailPlanifications = DetailPlanification::with('planification')
        ->with(['instructor' => function($instructor){
            $instructor->with('user');
        }])
        ->with(['course' => function($course)
        {
            $course->with('user')->with('area')->with('specialty')->with('modality')
            ->with('course')->with('institution')->with('schedules'); 
        }])->with('detailRegistration')->get();
        
        return response()->json([
                'data' => $detailPlanifications,
                ]
            , 200);
    }

    //get return information for form A2, E1
    public function showInscription(Request $request)
    {
        $detailPlanifications = DetailPlanification::where('detail_registration_id', $request->id)
        ->with(['detailRegistration' => function($detailRegistration){
            $detailRegistration->with(['registration' => function($registration){
                $registration->with(['participant' => function($participant){
                    $participant->with(['user' => function($user){
                        $user->with('ethnicOrigin')->with('gender');
                    }]);
                }])->with(['planification' => Function($planification){
                    $planification->with(['course' => function($course){
                        $course->with('institution')->with('modality')->with('schedules');
                    }]);
                }])->with(['additionalInformation' => function($additionalInformations){
                    $additionalInformations->with('levelInstruction');
                }]);
            }]);
        }])->get();

        /* $additionalInformations = AdditionalInformation::where('registration_id', $request->registration_id)->get();
        $detailPlanifications->push('additional_information',$additionalInformations); */
        return response()->json([
                'data' => $detailPlanifications
                ]
            , 200);
    }

    public function filter(Request $request)
    {
        $detailPlanifications = DetailPlanification::where('name', $request->name)->orderBy('name')->get();
        return response()->json([
                'data' =>  $detailPlanifications
                ]
            , 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        DetailPlanification::create($data);
        return response()->json([
            'data' => $data
        ], 201);
    }

    public function update(Request $request, $id, DetailPlanification $detailPlanifications)
    {
        $data = $request->all();

        $detailPlanifications = DetailPlanification::where('id', $id)->update($data);
        return response()->json([
            'data' =>  $data
            ], 200);
    }

    public function destroy($id)
    {
        $detailPlanifications = DetailPlanification::destroy($id);
        return response()->json([
            'data' => $id,
        ], 201);
    }

    //export PDF

    public function planificationPDF(Request $request)
    {  
        $detailPlanifications= DetailPlanification::where('id', $request->id)->get();
        
        $pdf = new PDF();
        $pdf = PDF::loadView('reports.cecy.formD1',["data" => $detailPlanifications]);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('PROGRAMACIÓN DE CURSOS DE CAPACITACIÓN MENSUAL.pdf');
    }

    public function inscriptionPDF(Request $request)
    {
        $detailPlanifications = DetailPlanification::where('id', $request->id)->get();

            $pdf = new PDF();
            $pdf = PDF::loadView('reports.cecy.formA2',['data' => $detailPlanifications]);
            return $pdf->stream('INSCRIPCIÓN PARA CURSOS DE CAPACITACIÓN.pdf');
    }

    public function needsPDF(Request $request)
    {
        $detailPlanifications= DetailPlanification::where('id', $request->id)->get();
        
        $pdf = new PDF();
        $pdf = PDF::loadView('reports.cecy.formF2',['data' => $detailPlanifications]);
        return $pdf->stream('INFORME DE NECESIDAD DEL CURSO.pdf');
    }

    public function enrolledPDF(Request $request)
    {
        $detailPlanifications= DetailPlanification::where('id', $request->id)->get();
        
        $pdf = new PDF();
        $pdf = PDF::loadView('reports.cecy.formE1',['data' => $detailPlanifications]);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('REGISTRO DE PARTICIPANTES INSCRITOS Y MATRICULADOS.pdf');
    }
    
}
