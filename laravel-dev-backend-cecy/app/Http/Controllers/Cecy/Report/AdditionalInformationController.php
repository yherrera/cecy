<?php

namespace App\Http\Controllers\Cecy\Report;

use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Models\Cecy\AdditionalInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Barryvdh\DomPDF\Facade as PDF;


class AdditionalInformationController extends Controller
{
    
    public function index(Request $request)
    {
        $additionalInformations = AdditionalInformation::all();
        
        return response()->json([
                'data' => $additionalInformations
                ], 200);
    }

    public function showInscription()
    {
        $additionalInformations = AdditionalInformation::with(['registration' => function($registartion)
        {
            $registartion->with('planification')->with(['participant' => function($user)
            {
                $user->with('user');
            }]);
        }])->with('course')->get();
        
        return response()->json([
                'data' => $additionalInformations
                ], 200);  
    }


    public function filter(Request $request)
    {
        //$additionalInformations = Course::where('{id}', $request)->get();
        /* return response()->json([
                'data' => $additionalInformations
                ]
            , 200);  */
        /* $additionalInformations =  Course::find($request->id)->with('area')->with('specialty')->with('period')
        ->with('level')->with('course')->with('modality')
        ->with('type')->with('institution')->with('schedules')
        ->get();
        return response()->json([
            'data' => $additionalInformations
        ], 200); */
    }

    public function store(Request $request)
    {
        $data = $request->all();

        AdditionalInformation::create($data);
        return response()->json([
            'data' => $data
        ], 201);
    }

    public function update(Request $request, $id,   AdditionalInformation $additionalInformations)
    {
        $data = $request->all();

        $Course = AdditionalInformation::where('id', $id)->update($data);
        return response()->json([
            'data' =>  $additionalInformations
        ], 200);
    }

    public function destroy($id)
    {
        $additionalInformations = AdditionalInformation::destroy($id);
        return response()->json([
            'data' => [
                'attributes' => $id,
                'type' => $additionalInformations
            ]
        ], 201);
    }


    //export PDF

    /* public function needsPDF(Request $request)
    {
        $additionalInformations = Course::with('area')->with('period')
        ->with('level')->with('course')->with('modality')->with('type')
        ->get();

            $pdf = new PDF();
            $pdf = PDF::loadView('reports.cecy.formF2',['data' => $additionalInformations]);
            // $pdf->setPaper('A4', 'landscape');
            // $pdf->save(Storage::path('/needs_export.pdf'));
            //return $pdf->download('INFORME DE NECESIDAD DEL CURSO.pdf');
            return $pdf->stream('INFORME DE NECESIDAD DEL CURSO.pdf');
    }
 */
    public function photographicRegisterPDF(Request $request)
    {
        $additionalInformations = AdditionalInformation::with('area')->with('period')
        ->with('level')->with('course')->with('modality')->with('type')
        ->get();
        
            $pdf = new PDF();
            $pdf = PDF::loadView('reports.cecy.photographicRegister',['data' => $additionalInformations]);
            //$pdf->setPaper('A4', 'landscape');
            //$pdf->save(Storage::path('/planification_export.pdf'));
            return $pdf->stream('PROGRAMACIÓN DE CURSOS DE CAPACITACIÓN MENSUAL.pdf');
            //return $pdf->download('PROGRAMACIÓN DE CURSOS DE CAPACITACIÓN MENSUAL.pdf');
    }

    
    public function inscriptionPDF(Request $request)
    {
        $additionalInformations = AdditionalInformation::findOrFail($this->request->id = 'id');
        $additionalInformations = AdditionalInformation::with('area')->with('period')
        ->with('level')->with('course')->with('modality')->with('type')
        ->get();
        
            $pdf = new PDF();
            $pdf = PDF::loadView('reports.cecy.formA2',['data' => $additionalInformations]);
            //$pdf->setPaper('A4', 'landscape');
            //$pdf->save(Storage::path('/planification_export.pdf'));
            return $pdf->stream('PROGRAMACIÓN DE CURSOS DE CAPACITACIÓN MENSUAL.pdf');
            //return $pdf->download('PROGRAMACIÓN DE CURSOS DE CAPACITACIÓN MENSUAL.pdf');
    }
}
