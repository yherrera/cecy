<?php

namespace App\Http\Controllers\Cecy;

use App\Http\Controllers\Controller;
use App\Models\Cecy\Course;
use App\Models\Cecy\Planification;
use App\Models\Cecy\Registration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function show($var = null, Request $request)
    {
        $attributes = [];
        $type = $var;
        switch($var) {
            case 'alumns':
                $atts = Registration::select()
                ->with('participants.user','planifications')
                ->whereHas('planifications', function ($query) use ($request) {
                    $query->where('course_id', $request->id_course);
                })
                ->get();
                array_push($attributes, [
                    'name' => 'Todos los Participantes',
                    'id' => 0
                ]);
                foreach($atts as $att){
                    $atr['name'] = $att->participants->user->first_lastname.' '.$att->participants->user->second_lastname.' '.$att->participants->user->first_name.' '.$att->participants->user->second_name;
                    $atr['id'] = $att->id;
                    $atr['school_period_id'] = $att->planifications->school_period_id;
                    $atr['course_id'] = $att->planifications->course_id;
                    $atr['planification'] = $att->planifications->id;
                    array_push($attributes, $atr);
                }
            break;

            case 'courses':
                $attributes = Course::select('courses.name','courses.id', 'note_base')
                ->join('planifications', 'planifications.course_id', 'courses.id')
                //->where('planifications.responsible_id', Auth::user()->id)//TODO: hacer con el login
                ->get();
            break;

            case 'typeNotes':
                $attributes = DB::connection('pgsql-cecy')
                ->table('type_notes')
                ->select('id', 'description_typ as name')
                ->get();
                break;

            case 'config':
                    $attributes = 
                    [
                        'base_calification' => 100,
                    ];
                    break;

            default:
                
            break;
        }
        return response()->json([
            'data' => [
                'type' => $type,
                'attributes' => $attributes
            ]]
        , 200);
    }
}
