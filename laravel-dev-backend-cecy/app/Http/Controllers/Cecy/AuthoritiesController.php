<?php

namespace App\Http\Controllers\Cecy;

use App\Http\Controllers\Controller;
use App\Models\Cecy\Authority;
use App\Models\Ignug\State;
use App\Models\Cecy\Catalogue;
use App\Models\Authentication\Role ;
use App\Models\Authentication\User ;
use Illuminate\Http\Request;

class AuthoritiesController extends Controller
{
    public function index(Request $request)
    {
        //$authorities = Authorities::all();
        $authorities = Authority::with('user','status','position','state')->get();

        return response()->json([
            'data' => $authorities,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }
    
    public function show( $id)
    { 
        $authorities=Authority::find($id);
        return response()->json([
            'data' => $authorities,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);  
    }

    public function filter(Request $request)
    {
        $authorities = Authority::where('name', $request->name)->orderBy('name')->get();
        return response()->json([
            'data' => $authorities,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }

    public function store(Request $request)
    {

        $data = $request->all();
        $dataUser =  $data['user'];
        $dataStatus =  $data['status'];
        $dataPosition =  $data['position'];
        $dataAuthorities = $data['authorities'];

        $authorities = new Authority( $dataAuthorities);
        $authorities->start_position=$dataAuthorities['start_position'];
        $authorities->end_position=$dataAuthorities['end_position'];
        $authorities->state()->associate(State::where('code', '1')->first());
        $authorities-> user()->associate(User::findorfail($dataUser['id']));
        $authorities-> position()->associate(Catalogue::findorfail($dataPosition['id']));
        $authorities-> status()->associate(Catalogue::findorfail($dataStatus['id']));
        
        $authorities-> save();
        return response()->json([
            'data' => $authorities,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }

    public function update(Request $request, $id, authority $Authorities)
    {
        $data = $request->all();
        $dataUser =  $data['user'];
        $dataStatus =  $data['status'];
        $dataPosition =  $data['position'];
        $dataAuthorities = $data['authorities'];

        $authorities = Authority::findOrfail($id);  
        $authorities->start_position = $dataAuthorities['start_position'];
        $authorities->end_position = $dataAuthorities['end_position']; 
        $authorities-> user()->associate(User::findorfail($dataUser['id']));
        $authorities-> position()->associate(Catalogue::findorfail($dataPosition['id']));
        $authorities-> status()->associate(Catalogue::findorfail($dataStatus['id']));

        $authorities-> save();
        return response()->json([
            'data' => $authorities,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }

    public function destroy($id)
    {
        $state = State::where('code', '2')->first();
        $authorities = Authority::findOrFail($id);
        $authorities->state_id = 2; 
        $authorities->save();
        return response()->json([
            'data' => $authorities,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }

     public function getUser()
    {
         $user = User::all();
         return response()->json([
            'data' => $user,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }
}
