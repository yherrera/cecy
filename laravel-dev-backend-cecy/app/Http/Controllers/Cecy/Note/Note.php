<?php

namespace App\Http\Controllers\Cecy\Note;

use App\models\Cecy\Note as ModelNote;
use Illuminate\Support\Facades\Auth;

class Note
{
    public $notes = [];
    public $note = null;
    public $statusNotes = false;
    public $typeNotes = [];
    public function __construct($data = [])
    {
        $this->note = $data;
    }

    public function getAllWithTypeNote()
    {
        $note = $this->note;
        $this->notes = ModelNote::select('notes.id', 'notes.id_registration', 'notes.id_type_note', 'notes.calification_not')
        ->with('registration', 'registration.planifications' , 'registration.participants.user')
        ->whereHas('registration.planifications', function ($query) use ($note) {
            if(key_exists('id_course', $note)){
                if($this->note['id_course'] > 0){
                    $query->where('planifications.course_id', $note['id_course']);
                }
            }
        });
        if(key_exists('id_alumn', $this->note)){
            if($this->note['id_alumn'] > 0){
                $this->notes = $this->notes
                ->where('id_registration', $this->note['id_alumn']);
            }
        }
        $this->notes = $this->notes
        ->where('status_not', 1)
        ->orderby('id', 'asc')
        ->get();
        
        $this->typeNotes = ModelNote::select('description_typ','type_notes.id')->with('registration', 'registration.planifications')
        ->whereHas('registration.planifications', function ($query) use ($note) {
            if(key_exists('id_course', $note)){
                if($this->note['id_course'] > 0){
                    $query->where('planifications.course_id', $note['id_course']);
                }
            }
        });
        if(key_exists('id_alumn', $this->note)){
            if($this->note['id_alumn'] > 0){
                $this->typeNotes = $this->typeNotes
                ->where('id_registration', $this->note['id_alumn']);
            }
        }
        $this->typeNotes = $this->typeNotes
        ->join('type_notes','type_notes.id', 'notes.id_type_note')
        ->distinct()
        ->get();
    }

    public function getNotesByCourse($id_course = 0)
    {
        $this->notes = ModelNote::get();
    }

    public function getNotesByAlumn($id_alunm = 0)
    {
        $this->notes = ModelNote::get();
    }

    public function create()
    {
        $note = new ModelNote();
        $note->calification_not = $this->note['calification'];
        $note->date_not = $this->note['date'];
        $note->id_registration = $this->note['id_registration'];
        if(key_exists('observation', $this->note))
        $note->observation_not = $this->note['observation'];
        $note->id_type_note = $this->note['id_typeNote'];
        $note->id_user = 1;//Auth::user()->id;TO-DO://poner con session iniciada
        if($note->save()){
            $this->statusNotes = true;
            $this->note = $note;
        }
    }

    public function validateNote($select = ['id'])
    {
        if(ModelNote::select($select)
        ->where('id_registration', $this->note['id_registration'])
        ->where('id_type_note', $this->note['id_typeNote'])
        ->first()){
            $this->statusNotes = true;
        }
    }

    public function getAllByIdRegistration($select = ['*'])
    {
        $this->notes = ModelNote::select($select)
        ->join('type_notes','type_notes.id', 'notes.id_type_note')
        ->where('id_registration', $this->note['id_registration'])
        ->where('status_not', 1)
        ->get();
    }

    public function update(){
        if(ModelNote::where('id', $this->note['id'])
        ->update($this->note)){
            $this->statusNotes = true;
        }
    }

}
