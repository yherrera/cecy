<?php

namespace App\Http\Controllers\Cecy\Note;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NotesController extends Controller
{
    public function index(Request $request)
    {
        $note = new Note($request->all());
        $note->getAllWithTypeNote();
        $columns = [
            'Alumno'
        ];
        $notesOut = [];
        foreach ($note->typeNotes as $valor) {
            array_push($columns, $valor->description_typ);
        }
        foreach($note->notes as $index => $not){
            if($index == 0)
                array_push($notesOut, collect($not));
            $si = false;
            foreach($notesOut as $not1){
                if($not1['id_registration'] == $not->id_registration)
                    $si = true;
            }
            if($si == false)
            array_push($notesOut, collect($not));
            foreach($notesOut as $value => $not2){
                foreach($note->typeNotes as $key => $valor){
                    if($not->id_type_note == $valor->id && $not2['id_registration'] == $not->id_registration){
                        $not2[$valor->description_typ]  = $not->calification_not;
                        $not2['note_'.$not->id]  = $not->id_type_note;
                    }
                }
            }
            unset($note->notes[$index]);
        }
        foreach($notesOut as  $not3){
            $noteEnd = 0;
            foreach($note->typeNotes as $valor){
                if(isset($not3[$valor->description_typ])){
                    $noteEnd += $not3[$valor->description_typ];
                }else{
                    $not3[$valor->description_typ] = 0;
                    $noteEnd += 0;
                }
            }
            $not3['Nota Final'] = round($noteEnd/count($note->typeNotes), 2);
            if($not3['registration']['planifications']['note_base'] >=  $not3['Nota Final']){
                $not3['Estado'] = 'NO Aprobado';
            }else{
                $not3['Estado'] = 'Aprobado';
            }
            $not3['id_course'] = $not3['registration']['planifications']['course_id'];
            $not3['Alumno'] = $not3['registration']['participants']['user']['first_lastname'].' '.$not3['registration']['participants']['user']['second_lastname'].' '.$not3['registration']['participants']['user']['first_name'].' '.$not3['registration']['participants']['user']['second_name'].' ';
        }
        array_push($columns, 'Nota Final', 'Estado');
        return[
            'notes' => $notesOut,
            'columns' => $columns
        ];
    }

    public function show($var, Request $request)
    {
        $notes = [];
        $note = new Note($request->all());
        switch($request->type){
            case 'allByCourse':
                $note->getNotesByCourse($var);
                return $note->notes;
            break;
            case 'allByAlumn':
                $note->getNotesByAlumn($var);
                return $note->notes;
            break;
        }
        return [
            'notes' => $notes
        ];
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), 
            [
                'calification' => 'required|numeric',
                'date' => 'required|date',
                'id_course' => 'required|numeric',
                'id_partaker' => 'required|numeric',
                'id_typeNote' => 'required|numeric',
            ],
            [
                'calification.required' => 'Ingrese una Calificación Válida'
            ]
        );
        $messages = [];
        $alert = 'Algo Salio Mal, Intentalo Nuevamente...';
        $response = false;
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            foreach ($errors as $key => $value) {
                $messages[$key] = $value;
            }
        }else{
            $data = $request->all();
            $data['id_registration'] = $request->id_partaker;
            $note = new Note($data);
            $note->validateNote();
            if($note->statusNotes == true){
                $alert = 'Ya Existe una Nota del Mismo Tipo y con el Mismo Participante';
            }else{
                $note->create();
                if($note->statusNotes == true){
                    $alert = 'Guardado Correctamente';
                }     
            }
            $response = $note->statusNotes;
        }
        return [
            'messages' => $messages,
            'alert' => $alert,
            'response' => $response,
        ];
    }

    public function edit($var)
    {
        $note = new Note(['id_registration' => $var]);
        $note->getAllByIdRegistration(['date_not as Fecha', 'calification_not as Nota', 'notes.id', 'type_notes.description_typ as Tipo de Nota']);
        return 
        [
            'notes' => $note->notes,
            'columnas1' => ['Fecha',  'Tipo de Nota', 'Nota'],
        ];
    }

    public function update($id, Request $request){
        $validator = Validator::make($request->all(), 
            [
                'calification_not' => 'required|numeric|min:0',
                'date_not' => 'required|date',
                'id' => 'required|numeric',
            ],
            [
                'hours_assi.required' => 'Ingrese una Hora de Falta Válida',
                'hours_assi.min' => 'Ingrese una Hora de Falta Mayor o Igual a 1',
            ]
        );
        $messages = [];
        $alert = 'Algo Salio Mal, Intentalo Nuevamente...';
        $response = false;
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            foreach ($errors as $key => $value) {
                $messages[$key] = $value;
            }
        }else{
            $note = new Note($request->all());
            $note->update();
            if($note->statusNotes == true){
                $alert = 'Actualizado Correctamente';
            }     
            $response = $note->statusNotes;
        }
        return [
            'messages' => $messages,
            'alert' => $alert,
            'response' => $response,
        ];
    }
}
