<?php

namespace App\Http\Controllers\Cecy;

use Intervention\Image\ImageManagerStatic as Img ;
use App\Http\Controllers\Controller;
use App\Models\Cecy\Institution;
use App\Models\Ignug\State;
use App\Models\Ignug\Image;
use Illuminate\Http\Request;
use App\Models\Cecy\Authority;


class InstitutionsController extends Controller
{
    public function index(Request $request)
    {
        //$institutions = Institution::all();
        //$institutions = Institution::with('state')->with('authorities')->get();
        $institutions =Institution::with('state')
        ->with(['authority'=> function($autority){
            $autority->with('user');
        }])->get();


        return response()->json([
            'data' => $institutions,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }

    public function show( $id)
    { 
        $institutions=Institution::find($id);
        return response()->json([
            'data' => $institutions,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
  
    }
    
    public function filter(Request $request)
    {
        $institutions = Institution::where('name', $request->name)->orderBy('name')->get();
        return response()->json([
            'data' => $institutions,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }

    public function store(Request $request)
    {

        $data = $request->all();
        $dataAutorities = $data['authority'];
        $dataInstitutions = $data['institutions'];

        $institutions = new Institution( $dataInstitutions );
        $institutions->code=$dataInstitutions['code'];
        $institutions->ruc=$dataInstitutions['ruc'];
        $institutions->name=$dataInstitutions['name'];
        $institutions->logo='assets/pages';
        $institutions->slogan=$dataInstitutions['slogan'];
        $institutions-> authority()->associate(Authority::findorfail($dataAutorities['id']));
        $institutions-> state()->associate(State::where('code', '1')->first());
        //return $institutions;
        $institutions -> save(); 
        return response()->json([
            'data' => $institutions,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
            }

            
    public function update(Request $request, $id)
    {
        $data = $request->json()->all();
        $dataAutorities = $data['authority'];
        $dataInstitutions = $data['institutions'];

        $institutions = Institution::findOrfail($id);
        $institutions->code=$dataInstitutions['code'];
        $institutions->ruc=$dataInstitutions['ruc'];
        $institutions->name=$dataInstitutions['name'];
        $institutions->slogan=$dataInstitutions['slogan'];

        $institutions-> authority()->associate(Authority::findorfail($dataAutorities['id']));

        $institutions->save();
        return response()->json([
            'data' => $institutions,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }

    public function destroy($id)
    {
        $state = State::where('code', '2')->first();
        $institutions = Institution::findOrFail($id);
        $institutions->state_id = 2;
        $institutions->save();
        return response()->json([
            'data' => $institutions,
            'msg' => [
                'summary' => 'success',
                'detail' => '',
                'code' => '200',
            ]], 200);
    }


    public function upload(Request $request )
    {
        $image    = $request->image;

        $nombre     = 'logo.'.$image->guessExtension();
    
        $filePath=storage_path().'/app/public/cecy/'.($nombre);
    
        $img = Img::make($image->getRealPath())
            ->resize(100,100)->save($filePath);

            $institutions = Institution::findOrFail($request->classroom_id);
            $logo = $institutions->images()->first();
            if (!$logo) {
                $logo = new Image([
                    'code' => 'asd',
                    'name' => 'Avatar',
                    'description' => 'Avatar',
                    'type' => 'logo',
                    'extension' => $image->guessExtension(),
                    'uri' => $filePath,
                ]);
                $logo->imageable()->associate($institutions);
                $logo->state()->associate(State::firstWhere('code', State::ACTIVE));
                $logo->save();
            }
            return response()->json([
                'data' => $image,
                'msg' => [
                    'summary' => 'success',
                    'detail' => '',
                    'code' => '200',
                ]], 200);
    }


}
