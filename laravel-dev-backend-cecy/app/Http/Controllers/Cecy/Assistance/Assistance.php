<?php

namespace App\Http\Controllers\Cecy\Assistance;

use App\models\Cecy\Assistance as ModelAssistance;
use App\Models\Cecy\Planification;
use App\Models\Cecy\Registration;
use Illuminate\Support\Facades\Auth;

class Assistance
{
    public $assistances = [];
    public $alumns = [];
    public $assistance = null;
    public $statusAssistances = false;
    public function __construct($data = [])
    {
        $this->assistance = $data;
    }

    public function getAll()
    {
        $assistance = $this->assistance;
        $this->assistances = ModelAssistance::select('assistances.id', 'assistances.id_registration', 'assistances.hours_assi', 'date_assi')
        ->with('registration', 'registration.planifications')
        ->whereHas('registration.planifications', function ($query) use ($assistance) {
            if(key_exists('id_course', $assistance))
                if($assistance['id_course'] > 0)
                    $query->where('planifications.course_id', $assistance['id_course']);
        });
        if(key_exists('id_alumn', $this->assistance)){
            if($this->assistance['id_alumn'] > 0){
                $this->assistances = $this->assistances
                ->where('id_registration', $this->assistance['id_alumn']);
            }
        }
        $this->assistances = $this->assistances
        ->where('status_assi', 1)
        ->get();
    }

    public function getassistancesByCourse($id_course = 0)
    {
        $this->assistances = ModelAssistance::get();
    }

    public function getassistancesByAlumn($id_alunm = 0)
    {
        $this->assistances = ModelAssistance::get();
    }

    public function create()
    {
        $assistance = new ModelAssistance();
        $assistance->hours_assi = $this->assistance['hours_lack'];
        $assistance->date_assi = $this->assistance['date'];
        $assistance->id_registration = $this->assistance['id_registration'];
        if(key_exists('observation', $this->assistance))
        $assistance->observation_assi = $this->assistance['observation'];
        $assistance->id_user = 1;//Auth::user()->id;TO-DO://poner con session iniciada
        if($assistance->save()){
            $this->statusAssistances = true;
            $this->assistance = $assistance;
        }
    }

    public function validateassistance($select = ['id'])
    {
        if(ModelAssistance::select($select)
        ->where('id_registration', $this->assistance['id_registration'])
        ->where('id_type_assistance', $this->assistance['id_typeassistance'])
        ->first()){
            $this->statusAssistances = true;
        }
    }

    public function getAllByIdRegistration($select = ['*'])
    {
        $this->assistances = ModelAssistance::select($select)
        ->where('id_registration', $this->assistance['id_registration'])
        ->where('status_assi', 1)
        ->orderBy('date_assi','asc')
        ->get();
    }

    public function getDataRegistrationByCourse()
    {
        return Planification::select()
        ->with('school_period','course')
        ->where('course_id', $this->assistance['id_course'])
        ->first();
    }

    public function getAllRegistrationsByCourse()
    {
        $assistance = $this->assistance;
        $this->alumns = Registration::select()
        ->with('planifications' , 'registration.participants.user')
        ->whereHas('planifications', function ($query) use ($assistance) {
            if(key_exists('id_course', $assistance)){
                if($assistance['id_course'] > 0){
                    $query->where('course_id', $assistance['id_course']);
                }
            }
        });
        if(key_exists('id_alumn', $this->assistance)){
            if($this->assistance['id_alumn'] > 0){
                $this->alumns = $this->alumns
                ->where('id', $this->assistance['id_alumn']);
            }
        }
        $this->alumns = $this->alumns
        ->get();
    }

    public function update(){
        if(ModelAssistance::where('id', $this->assistance['id'])
        ->update($this->assistance)){
            $this->statusAssistances = true;
        }
    }
}
