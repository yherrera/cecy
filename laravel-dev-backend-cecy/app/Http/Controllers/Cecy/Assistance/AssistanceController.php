<?php

namespace App\Http\Controllers\Cecy\Assistance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class AssistanceController extends Controller
{
    public function index(Request $request)
    {
        
        $assistance = new Assistance($request->all());
        $assistance->getAll();
        $columns = [
            'Alumno'
        ];
        $courseIn = $assistance->getDataRegistrationByCourse();
        $assistance->getAllRegistrationsByCourse();
        $course = [
            'date_init' => $courseIn->school_period->start_date,
            'date_end' => $courseIn->school_period->end_date,
        ];
        $assistancesOut = [];
        $date_start = $course['date_init'];
        array_push($columns, $date_start);
        for($i = 0; $date_start < $course['date_end']; $i++){
            $new_date = date("Y-m-d",strtotime($date_start."+ 1 days")); 
            array_push($columns,$new_date);
            $date_start = $new_date;
        }
        $duration = $courseIn->course->duration;
        foreach($assistance->alumns as $index => $alumn){
            foreach($columns as $col){
                $assistancesOut[$index][$col] = '-';
            }
            $assistancesOut[$index]['Total de Faltas'] = 0;
            foreach($assistance->assistances as $value => $ass){
                foreach($columns as $col){
                    if($col == 'Alumno')
                        $assistancesOut[$index][$col] = $alumn->participants->user->first_lastname.' '.$alumn->participants->user->second_lastname.' '.$alumn->participants->user->first_name.' '.$alumn->participants->user->second_name;
                    else{
                        if($ass->date_assi == $col && $ass->id_registration == $alumn->id){
                            $assistancesOut[$index][$col] = $ass->hours_assi;
                            $assistancesOut[$index]['Total de Faltas'] += $ass->hours_assi;
                        }
                    }
                }
            }
            if((100/$duration)*$assistancesOut[$index]['Total de Faltas'] > $courseIn->assistance_base){
                $assistancesOut[$index]['Estado'] = 'NO-Aprobado';
            }else{
                $assistancesOut[$index]['Estado'] = 'Aprobado';
            }
            $assistancesOut[$index]['Total de Faltas'] = $assistancesOut[$index]['Total de Faltas'].' horas. /'.$duration;
            $assistancesOut[$index]['id_registration'] = $alumn->id;
        }
        array_push($columns, 'Total de Faltas', 'Estado');
        return[
            'assistances' => $assistancesOut,
            'columns' => $columns,
        ];
    }

    public function show($var, Request $request)
    {
        $assistances = [];
        $assistance = new Assistance($request->all());
        switch($request->type){
            case 'allByCourse':
                $assistance->getassistancesByCourse($var);
                return $assistance->assistances;
            break;
            case 'allByAlumn':
                $assistance->getassistancesByAlumn($var);
                return $assistance->assistances;
            break;
        }
        return [
            'assistances' => $assistances
        ];
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), 
            [
                'hours_lack' => 'required|numeric|min:1',
                'date' => 'required|date',
                'id_course' => 'required|numeric',
                'id_partaker' => 'required|numeric',
            ],
            [
                'hours_lack.required' => 'Ingrese una Hora de Falta Válida',
                'hours_lack.min' => 'Ingrese una Hora de Falta Mayor o Igual a 1',
            ]
        );
        $messages = [];
        $alert = 'Algo Salio Mal, Intentalo Nuevamente...';
        $response = false;
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            foreach ($errors as $key => $value) {
                $messages[$key] = $value;
            }
        }else{
            $data = $request->all();
            $data['id_registration'] = $request->id_partaker;
            $assistance = new Assistance($data);
            $assistance->create();
            if($assistance->statusAssistances == true){
                $alert = 'Guardado Correctamente';
            }     
            $response = $assistance->statusAssistances;
        }
        return [
            'messages' => $messages,
            'alert' => $alert,
            'response' => $response,
        ];
    }

    public function edit($var)
    {
        $assistance = new Assistance(['id_registration' => $var]);
        $assistance->getAllByIdRegistration(['id','date_assi as Fecha','hours_assi as Horas','observation_assi as Observacion']);
        return 
        [
            'assistances' => $assistance->assistances,
            'columnas1' => ['Fecha', 'Horas', 'Observacion'],

        ];
    }

    public function update($id, Request $request){
        $validator = Validator::make($request->all(), 
            [
                'hours_assi' => 'required|numeric|min:0',
                'date_assi' => 'required|date',
                'id' => 'required|numeric',
                'observation_assi' => 'required|max:200',
            ],
            [
                'hours_assi.required' => 'Ingrese una Hora de Falta Válida',
                'hours_assi.min' => 'Ingrese una Hora de Falta Mayor o Igual a 1',
            ]
        );
        $messages = [];
        $alert = 'Algo Salio Mal, Intentalo Nuevamente...';
        $response = false;
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            foreach ($errors as $key => $value) {
                $messages[$key] = $value;
            }
        }else{
            $assistance = new Assistance($request->all());
            $assistance->update();
            if($assistance->statusAssistances == true){
                $alert = 'Actualizado Correctamente';
            }     
            $response = $assistance->statusAssistances;
        }
        return [
            'messages' => $messages,
            'alert' => $alert,
            'response' => $response,
        ];
    }
}
