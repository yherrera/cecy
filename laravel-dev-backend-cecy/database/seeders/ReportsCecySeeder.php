<?php

namespace Database\Seeders;

use App\Models\Authentication\User;
use Illuminate\Database\Seeder;
use App\Models\Cecy\AdditionalInformation;
use App\Models\Cecy\Catalogue;
use App\Models\Cecy\Schedule;
use App\Models\Cecy\Course;
use App\Models\Cecy\Planification;
use App\Models\Cecy\DetailRegistration;
use App\Models\Cecy\DetailPlanification ;
use App\Models\Cecy\Authority;
USE App\Models\Cecy\Instructor;
use App\Models\Cecy\Institution;
use App\Models\Cecy\EvaluationMechanism;
use App\Models\Cecy\Prerequisite;
use App\Models\Cecy\Participant;
use App\Models\Cecy\Registration;
use App\Models\Cecy\SchoolPeriod;
use Illuminate\Support\Facades\DB;

class ReportsCecySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Catalogue::factory()->create([
            'code' => 'A',
            'name' => 'ADMINISTRACIÓN Y LEGISLACIÓN',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'B',
            'name' => 'AGRONOMÍA',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'C',
            'name' => 'ZOOTECNIA',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'D',
            'name' => 'ALIMENTACIÓN, GASTRONOMÍA Y TURISMO',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'E',
            'name' => 'TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIÓN',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'F',
            'name' => 'FINANZAS, COMERCIO Y VENTAS',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'H',
            'name' => 'CONSTRUCCIÓN E INFRAESTRUCTURA',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'I',
            'name' => 'FORESTAL, ECOLOGÍA Y AMBIENTE',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'J',
            'name' => 'EDUCACIÓN Y CAPACITACIÓN',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'K',
            'name' => 'ELECTRICIDAD Y ELECTRÓNICA',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'L',
            'name' => 'ESPECIES ACUÁTICAS Y PESCA',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'M',
            'name' => 'COMUNICACIÓN Y ARTES GRÁFICAS',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'N',
            'name' => 'MECÁNICA AUTOMOTRIZ',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'O',
            'name' => 'MECÁNICA INDUSTRIAL Y MINERÍA',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'P',
            'name' => 'PROCESOS INDUSTRIALES',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'Q',
            'name' => 'TRANSPORTE Y LOGÍSTICA',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'R',
            'name' => 'ARTES Y ARTESANÍA',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'S',
            'name' => 'SERVICIOS SOCIOCULTURALES Y A LA COMUNIDAD',
            'type' => 'areas',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'T',
            'name' => 'INDUSTRIA AGROPECUARIA ',
            'type' => 'areas',
            'state_id' => 1,
        ]);

        Catalogue::factory()->create([
            'code' => 'T',
            'name' => 'INDUSTRIA AGROPECUARIA ',
            'type' => 'areas',
            'state_id' => 1,
        ]);

        // participant_type
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Adultos',
            'type' => 'participant_type',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Estudiantes',
            'type' => 'participant_type',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Profesores',
            'type' => 'participant_type',
            'state_id' => 1,
        ]);

        // modality
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'PRESENCIAL',
            'type' => 'modality',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'DUAL',
            'type' => 'modality',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'VIRTUAL',
            'type' => 'modality',
            'state_id' => 1,
        ]);

        // levels
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Primero',
            'type' => 'levels',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Segundo',
            'type' => 'levels',
            'state_id' => 1,
        ]);

        // schedule
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => '13:00 - 15:00',
            'type' => 'schedule',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => '15:00 - 17:00',
            'type' => 'schedule',
            'state_id' => 1,
        ]);

        // course_type
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Administrativo',
            'type' => 'course_type',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Técnico',
            'type' => 'course_type',
            'state_id' => 1,
        ]);

        // academic_period
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'PRIMERO',
            'type' => 'academic_period',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'SEGUNDO',
            'type' => 'academic_period',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'TERCERO',
            'type' => 'academic_period',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'CUARTO',
            'type' => 'academic_period',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'QUINTO',
            'type' => 'academic_period',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'SEXTO',
            'type' => 'academic_period',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'SEPTIMO',
            'type' => 'academic_period',
            'state_id' => 1,
        ]);

        // specialty
        
        Catalogue::factory()->create([
            'parent_code_id' => 1,
            'code' => 'A.1',
            'name' => 'Administración General (Pública, Empresas, Microempresas, Cooperativas, Aduanera, Agrícola, Agropecuaria, Agroindustrial, Bancaria, Financiera, Forestal, Hospitalaria, Hotelera, Inmobiliaria, Pesquera, Minera, Etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 1,
            'code' => 'A.2',
            'name' => 'Gestión del Talento Humano (Manejo de Personal, Desempeño, Motivación, Liderazgo, Coaching, Trabajo en Equipo, Selección por Competencias, Plan Interno de Carrera, Comunicación Organizacional, Profesiogramas)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 1,
            'code' => 'A.3',
            'name' => 'Administración Contable y de Costos (Matemática Financiera, Estadística, Tributaria, Normas de Contabilidad, Auditorías Financieras, Contables, de Costos y Relacionadas, Normas Internacionales de Información Financiera Niif)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 1,
            'code' => 'A.4',
            'name' => 'Evaluación de Proyectos (Económica, Financiera)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 1,
            'code' => 'A.5',
            'name' => 'Atención y Servicios de Oficina: Secretariado (Operación de Máquinas de Oficina, Taquigrafía, Lectura Rápida, Oratoria, Redacción Y Ortografía), Recepción, Servicio al Cliente, Archivo, Conserjería, Limpieza. Relaciones Humanas',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 1,
            'code' => 'A.6',
            'name' => 'Legislación (Aduanera, Negociación, Mediación, Arbitraje, Patentes, Propiedad Intelectual, Tributaria, Laboral, Previsión Social, Agrícola, Financiera, Etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 1,
            'code' => 'A.7',
            'name' => 'Gestión de la Calidad (Normas, Auditorías de Sistemas de Calidad y Mejoramiento Continuo)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 2,
            'code' => 'B.1',
            'name' => 'Agricultura Orgánica',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 2,
            'code' => 'B.2',
            'name' => 'Semillas',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 2,
            'code' => 'B.3',
            'name' => 'Semillas',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 2,
            'code' => 'B.4',
            'name' => 'Cultivos (Siembra, Cosecha, Postcosecha, Manejo Nutricional de las Plantas)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 2,
            'code' => 'B.5',
            'name' => 'Leguminosas',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 2,
            'code' => 'B.7',
            'name' => 'Floricultura',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 2,
            'code' => 'B.8',
            'name' => 'Fruticultura',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 2,
            'code' => 'B.9',
            'name' => 'Jardinería y Poda',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 2,
            'code' => 'B.10',
            'name' => 'Horticultura',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 2,
            'code' => 'B.11',
            'name' => 'Sanidad Vegetal (Control Fitosanitario, Control de Plagas y Malezas)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 2,
            'code' => 'B.12',
            'name' => 'Suelos y Agua (Manejo de Insumos Agrícolas, Fertilizantes, Riego, Abonos)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 2,
            'code' => 'B.13',
            'name' => 'Viticultura y Enología',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 3,
            'code' => 'C.1',
            'name' => 'Sanidad Pecuaria (Veterinaria)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 3,
            'code' => 'C.6',
            'name' => 'Esquila (Ovejas, Conejos, Llamas, Cabras)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 3,
            'code' => 'C.9',
            'name' => 'Ganadería Mayor (Bovinos-leche/carne-,Ovino, Caprino, Camélido, Equinos)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 3,
            'code' => 'C.10',
            'name' => 'Ganadería Menor (Cuy, Conejo, Aves, Abejas, Anfibios, Moluscos, Porcinos, Anélidos)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 3,
            'code' => 'C.11',
            'name' => 'Helicicultura (Caracoles)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 3,
            'code' => 'C.12',
            'name' => 'Inseminación Artificial y Técnicas de Manejo Genético',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 3,
            'code' => 'C.15',
            'name' => 'Producción de Pastos',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 3,
            'code' => 'C.16',
            'name' => 'Alimentación de Rumiantes',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 3,
            'code' => 'C.17',
            'name' => 'Alimentación de Monogástricos',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 4,
            'code' => 'D.1',
            'name' => 'Elaboración, Tecnología y Producción de Alimentos (Higiene, Manipulación, Seguridad Alimentaria, Empaques, Etiquetado y Trazabilidad); y, Hazard.',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 4,
            'code' => 'D.2',
            'name' => 'Banquetería',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 4,
            'code' => 'D.3',
            'name' => 'Cocina Nacional e Internacional (Chef, Cocinero)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 4,
            'code' => 'D.4',
            'name' => 'Panadería y Pastelería',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 4,
            'code' => 'D.5',
            'name' => 'Repostería y Confitería',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 4,
            'code' => 'D.6',
            'name' => 'Catering y Servicio de Bar y Comedores (Barman, Mesero)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 4,
            'code' => 'D.7',
            'name' => 'Servicio de Recepción, Limpieza, Pisos y Afines (Recepcionista, Ama de Llaves, Botones, Camarera de Pisos, Encargado de Mantenimiento)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 4,
            'code' => 'D.8',
            'name' => 'Turismo (Ecoturismo, Agroturismo, Etnoturismo, Turismo de Aventura, Turismo Comunitario, Guía Nacional, Guía Especializado, Información, Organización y Coordinación de Eventos)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 4,
            'code' => 'D.9',
            'name' => 'Servicio de Agencias de Viaje (Operación, Transporte, Seguridad, Ventas, Operadores, Reservas)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 4,
            'code' => 'D.10',
            'name' => 'Diversificación de destinos y Desarrollo de Inclusión Comunitaria',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.1',
            'name' => 'Servicios Telemáticos y Generados de Valor Agregado',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.2',
            'name' => 'Telecomunicaciones (Comunicación Telefónica, Telegráfica, Satelital)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.3',
            'name' => 'Instalación, Mantenimiento y Reparación de Fibra Óptica',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.4',
            'name' => 'Enlace de datos',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.5',
            'name' => 'Transmisión, Emisión y Recepción de Información',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.6',
            'name' => 'Servicios de Comunicación en Voz y Datos',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.7',
            'name' => 'Base de Datos Relacional (Oracle, Sybase, Sql, Server, Db2, Access, Informix, Datacom, Unicenter Tng)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.7',
            'name' => 'Base de Datos Relacional (Oracle, Sybase, Sql, Server, Db2, Access, Informix, Datacom, Unicenter Tng)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.7',
            'name' => 'Base de Datos Relacional (Oracle, Sybase, Sql, Server, Db2, Access, Informix, Datacom, Unicenter Tng)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.8',
            'name' => 'Control de Calidad (Auditoría Computacional, Evaluación de Software, Sistemas de Seguridad)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.9',
            'name' => 'Hardware y Equipos (Arquitectura de Pc, Mantenimiento, Configuración)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.10',
            'name' => 'Internet E Intranet(Administración de Firewall, Correo Electrónico, Navegadores)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.11',
            'name' => 'Programas de Escritorio (Office, Hojas Electrónicas, Procesadores de Texto, Power Point), Computación Básica U Operación de Computadoras.',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.12',
            'name' => 'Software Especializado (Flex, Smartsuit, Autocad, Softland, Arc View, 3d)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.13',
            'name' => 'Redes (Comunidades de Redes Tecnológicas, Servicios, Protocolos, Señalización, Armado, Operación, Mantenimiento y Conectividad) ',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.14',
            'name' => 'Sistema Operativo (Ms-Dos, Windows 3xx, 95, 98, 2000, Vms, Computación  Básica u Operación de Comput, Solaris, Novell, Unix)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.15',
            'name' => 'Análisis de Sistemas ( Proyectos Informáticos, Problemas, Modelamiento de Información,  Reingeniería)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.16',
            'name' => 'Lenguaje de Programación (Pascal, Básic, Cobol, Visual Básic, C+++, Power Builder, Clipper, Java, PHP, Puntonet)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 5,
            'code' => 'E.17',
            'name' => 'Codificación y Decodificación de Señales en Medios de Comunicación',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 6,
            'code' => 'F.1',
            'name' => 'Marketing y Ventas (Negociación, Comercialización, Marketing y Ventas de Productos y Servicios)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 6,
            'code' => 'F.2',
            'name' => 'Comercio Exterior y Cambios',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 6,
            'code' => 'F.3',
            'name' => 'Comercio y Distribución Interna',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 6,
            'code' => 'F.4',
            'name' => 'Economía Aplicada',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 6,
            'code' => 'F.5',
            'name' => 'Crédito y Cobranzas',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 6,
            'code' => 'F.6',
            'name' => 'Detección de Circulante y Documentos Falsos',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 6,
            'code' => 'F.7',
            'name' => 'Negocios y Comercio Electrónico',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 6,
            'code' => 'F.8',
            'name' => 'Mercado Financiero (Bolsa de Valores, Capitales, Monetarios, Futuros, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 6,
            'code' => 'F.9',
            'name' => 'Presupuestos y Flujo de Caja',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 6,
            'code' => 'F.10',
            'name' => 'Riesgo Financiero (Análisis, Solvencia, Liquidez, Endeudamiento, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 6,
            'code' => 'F.11',
            'name' => 'Seguros (Análisis, Costos, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 6,
            'code' => 'F.12',
            'name' => 'Trámites de exportación e Importación',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.1',
            'name' => 'Albañilería',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.2',
            'name' => 'Cañonería (Conducción de Agua, Gas, Petróleo)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.3',
            'name' => 'Carpintería de Obra Gruesa (Paneles, Puertas, Vigas, Ventanas)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.4',
            'name' => 'Gasfitería',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.5',
            'name' => 'Carpintería y Estructura Metálica',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.6',
            'name' => 'Hojalatería (Bajadas de Agua, Canales)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.7',
            'name' => 'Instalaciones Sanitarias (Alcantarillado, Gasfitería)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.8',
            'name' => 'Mantenimiento de Edificios y Acabados',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.9',
            'name' => 'Obras (Caminos, Puentes, Túneles)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.10',
            'name' => 'Enfierradura',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.11',
            'name' => 'Recubrimiento de Interiores y Exteriores (Pintura, Alfombra, Azulejos)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.13',
            'name' => 'Tecnología de la Construcción (Planos, Materiales, Estructuras, Equipos, Etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.14',
            'name' => 'Arquitectura y Urbanismo (Proyectos, Restauración de Edificios y Vivienda)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.15',
            'name' => 'Dibujo Técnico',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.16',
            'name' => 'Construcciones Rurales',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 7,
            'code' => 'H.17',
            'name' => 'Plomería',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 8,
            'code' => 'I.1',
            'name' => 'Contaminación Ambiental',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 8,
            'code' => 'I.2',
            'name' => 'Gestión e Impacto Ambiental',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 8,
            'code' => 'I.3',
            'name' => 'Manejo y Conservación de Recursos Naturales',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 8,
            'code' => 'I.4',
            'name' => 'Producción Limpia',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 8,
            'code' => 'I.5',
            'name' => 'Tratamiento de Residuos (Líquidos, Sólidos, Gaseosos)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 8,
            'code' => 'I.6',
            'name' => 'Remediación Ambiental',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 8,
            'code' => 'I.7',
            'name' => 'Economía Ambiental',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 8,
            'code' => 'I.8',
            'name' => 'Combate de Incendios Forestal',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 8,
            'code' => 'I.9',
            'name' => 'Plantación, Conservación y Explotación de especies forestales (Poda, Raleo Forestación, Reforestación, Agroforestería, Viveros)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 8,
            'code' => 'I.10',
            'name' => 'Sanidad  y Manejo Forestal',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
        'parent_code_id' => 8,
        'code' => 'I.11',
        'name' => 'Silvicultura',
        'type' => 'specialty',
        'state_id' => 1,
    ]);
        
    Catalogue::factory()->create([
            'parent_code_id' => 8,
            'code' => 'I.12',
            'name' => 'Geofísica (Sismología, Meteorología, Climatología)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);

        
        Catalogue::factory()->create([
            'parent_code_id' => 8,
            'code' => 'I.13',
            'name' => 'Energías Alternativas',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
        'parent_code_id' => 9,
        'code' => 'J.1',
        'name' => 'Capacitación (Identificación de Necesidades, Procesos de Capacitación Continua y por Competencias Laborales, Evaluación y Seguimiento)',
        'type' => 'specialty',
        'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 9,
            'code' => 'J.2',
            'name' => 'Diseño Educativo y Curricular (Elaboración de Proyectos Educativos, Planes y Programas de Educación, Capacitación y Formación)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 9,
            'code' => 'J.3',
            'name' => 'Evaluación del Aprendizaje',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 9,
            'code' => 'J.4',
            'name' => 'Formación de Instructores, Facilitadores, Monitores, Maestros, Guías, Formadores',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 9,
            'code' => 'J.5',
            'name' => 'Medios y Materiales Didácticos (Diseño, Elaboración)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 9,
            'code' => 'J.6',
            'name' => 'Metodología y Técnica De Aprendizaje (Pre Básica, Básica, Media,  Diferencial, Adulto, Superior)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 9,
            'code' => 'J.7',
            'name' => 'Orientación Vocacional',
            'type' => 'specialty',
            'state_id' => 1,
            ]);
        
            Catalogue::factory()->create([
            'parent_code_id' => 10,
            'code' => 'K.1',
            'name' => 'Electricidad Domiciliaria (Reparación, Manejo y Mantenimiento)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 10,
            'code' => 'K.2',
            'name' => 'Electricidad  Automotriz',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 10,
            'code' => 'K.3',
            'name' => 'Electrodomésticos (Reparación, Manejo y Mantenimiento)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 10,
            'code' => 'K.4',
            'name' => 'Electromecánica (Instalación y Mantenimiento de Motores Eléctricos)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 10,
            'code' => 'K.5',
            'name' => 'Electrónica Industrial',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 10,
            'code' => 'K.6',
            'name' => 'Electrotecnia y Luminotecnia (Uso Industrial y Artístico del Sistema de Alumbrado, Voltaje, Resistencia)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 10,
            'code' => 'K.7',
            'name' => 'Instalación Telefónica (Reparación, Manejo y mantenimiento)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 10,
            'code' => 'K.8',
            'name' => 'Redes Eléctricas (Baja, Media y Alta Tensión, Instalaciones)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 10,
            'code' => 'K.9',
            'name' => 'Electricidad Industrial (Reparación, Manejo y Mantenimiento)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 10,
            'code' => 'K.10',
            'name' => 'Electrónica Automotriz (Inyección)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 11,
            'code' => 'L.1',
            'name' => 'Biología Marina (Selección Genética de Especies Acuáticas)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 11,
            'code' => 'L.2',
            'name' => 'Manejo de Especies Acuáticas',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 11,
            'code' => 'L.3',
            'name' => 'Cultivo de Especies Acuáticas',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 11,
            'code' => 'L.4',
            'name' => 'Pesca Artesanal y Buceo',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 11,
            'code' => 'L.5',
            'name' => 'Pesca Industrial',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 11,
            'code' => 'L.6',
            'name' => 'Tratamiento de Especies Acuáticas',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 11,
            'code' => 'L.7',
            'name' => 'Patologías de Especies Acuáticas',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 11,
            'code' => 'L.8',
            'name' => 'Piscicultura (Producción de Peces)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 12,
            'code' => 'M.2',
            'name' => 'Medios de Comunicación Social (Televisión, Radio, Prensa Escrita)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 12,
            'code' => 'M.3',
            'name' => 'Medios Audiovisuales (Videos, Películas, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 12,
            'code' => 'M.4',
            'name' => 'Métodos y Técnicas de Promoción y Difusión',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 12,
            'code' => 'M.5',
            'name' => 'Traducción e Interpretación',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 12,
            'code' => 'M.6',
            'name' => 'Lenguaje (Señas, Tacto, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 12,
            'code' => 'M.8',
            'name' => 'Grabados y Litografía',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 12,
            'code' => 'M.9',
            'name' => 'Gráfica (Impresión, Encuadernación, Diseño y Diagramación Gráfica, Fotomecánica Full Color, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 12,
            'code' => 'M.10',
            'name' => 'Periodismo e Investigación (Radio, TV. y Prensa)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 12,
            'code' => 'M.11',
            'name' => 'Edición',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 12,
            'code' => 'M.12',
            'name' => 'Fotografía (Digital y No Digital)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 13,
            'code' => 'N.1',
            'name' => 'Ajuste y Mantenimiento de Motores',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 13,
            'code' => 'N.2',
            'name' => 'Carrocería (Mantenimiento, Reparación, Enderezada y Pintura)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 13,
            'code' => 'N.3',
            'name' => 'Diagnóstico y Reparación de Sistemas Automotrices',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 13,
            'code' => 'N.4',
            'name' => 'Interpretación de Catálogos y Diagramas',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 13,
            'code' => 'N.5',
            'name' => 'Mecánica General (Básica)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 13,
            'code' => 'N.6',
            'name' => 'Sistemas de Dirección, Frenos, Suspensión, Transmisión',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 13,
            'code' => 'N.7',
            'name' => 'Vulcanización (Montaje y Desmontaje Neumáticos, Balanceo de Ruedas, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.1',
            'name' => 'Construcción y Reparación de Hornos',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.2',
            'name' => 'Exploración y Explotación Minera (Extracción, Perforación de Cobre, Hierro, Petróleo, Otros)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.3',
            'name' => 'Forja (Fabricación de Piezas Mediante Calor y Compresión)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.4',
            'name' => 'Fresado (Fabricación de Piezas, Engranajes, etc., Mediante Fresadora)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.5',
            'name' => 'Fundición (Fabricación de Piezas Mediante la Fusión de Metales)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.6',
            'name' => 'Matricería (Fabricación de Moldes y Matrices de Piezas en Serie)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.7',
            'name' => 'Mecánica de Banco (Fabricación de Piezas Mediante Herramientas de Mano)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.8',
            'name' => 'Metalmecánica Metalurgia (Estructuras Metálicas, Autopartes a fin de Obtener Plantas de Proceso Llave en Mano, Superestructuras, Equipos con Alto Grado de Automatización y Componente Tecnológico)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.9',
            'name' => 'Balance Metalúrgico (Preparación de Muestras, Análisis Químico y Balance de Materiales)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.10',
            'name' => 'Geología (Mineralogía, Petrología, Cristalografía, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.11',
            'name' => 'Rectificación (Terminación de Piezas y Medidas Mediante Abrasivos)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.12',
            'name' => 'Soldadura (Eléctrica y Oxigas, Radiografía, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.13',
            'name' => 'Tornería (Fabricación de Piezas y Partes Mediante Torno)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.14',
            'name' => 'Tratamientos Térmicos (Mejoramiento de Propiedades de los Metales Mediante Calor y Frío)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.15',
            'name' => 'Hidráulica',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 14,
            'code' => 'O.15',
            'name' => 'Explosivos',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.1',
            'name' => 'Petróleo (Exploración, Extracción, Procesamiento, Tratamiento y Distribución)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.3',
            'name' => 'Anticorrosivos (Cromado, Niquelado, Plastificado)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.4',
            'name' => 'Automatización Industrial y Robótica',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.5',
            'name' => 'Madera (Diseño, Técnicas, Procesamiento y Acabado, Muebles de Hogar, Cocina, Oficina, Industria de la Construcción, Puertas, Ventanas, Pallets)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.6',
            'name' => 'Cemento (Materiales de Construcción )',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.7',
            'name' => 'Cerámica y Vidrio (Diseño, Técnicas, Tallado, Procesamiento y Acabado, Diversificación en la Concentración del Sector Cerámico)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.8',
            'name' => 'Cuero y Calzado (Diseño, Técnicas y Acabado)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.9',
            'name' => 'Envases y Embalajes',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.10',
            'name' => 'Refrigeración (Cadena de Frio)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.11',
            'name' => 'Textil (Diseño, Patronaje y Confección de Prendas, Transformación de Plantillas, Costura, Sastrería)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.12',
            'name' => 'Tapicería',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.14',
            'name' => 'Seguridad, Prevención de Riesgos e Higiene Industrial',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.15',
            'name' => 'Industria Química (Galvanoplastia, Tinturas, Abonos, Plaguicidas, Barnices,  Lacas, Jabones, Cosméticos, Farmoquímica, Petroquímica, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.16',
            'name' => 'Lavandería, Tintorería y Planchado Industrial',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.17',
            'name' => 'Lubricantes',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.19',
            'name' => 'Calderos (Operación, Mantenimiento y Reparación)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.20',
            'name' => 'Operación, Reparación y Mantenimiento de Máquinas y Equipos (Agrícola, Agropecuario, Forestal, de Construcción, Textil, Minera, Pesquera, Médicos, de Comunicación, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.21',
            'name' => 'Papeles y Cartones',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.23',
            'name' => 'Plásticos y Cauchos',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.24',
            'name' => 'Prácticas de Manufactura (Estrategia de Producción y Gestión de Materia Prima, Programas de Diversificación Sectorial, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 15,
            'code' => 'P.25',
            'name' => 'Energía Renovable: Bioethanol (Materia Prima: Caña de Azúcar, Rechazo de Banano, Sorgo Dulce, Algas, Desechos, Bioethanol Artesanal, etc.), Biodiesel (Materia Prima: Aceite de Palma Africana, Piñón, Colza, Soya, Biodiesel Artesanal), Biogás (Materia Prima: Residuos Orgánicos), Extracción de Alcohol Artesanal.',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 16,
            'code' => 'Q.1',
            'name' => 'Conducción de Vehículos Terrestres',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 16,
            'code' => 'Q.3',
            'name' => 'Mantenimiento de Aeronaves y Naves',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 16,
            'code' => 'Q.4',
            'name' => 'Transporte de Carga y de Pasajeros (Aéreo, Fluvial, Marítimo, Terrestre)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 16,
            'code' => 'Q.5',
            'name' => 'Aeronáutica (Control de Operaciones, Tránsito Aéreo, Diseño y Construcción de Aeronaves, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 16,
            'code' => 'Q.6',
            'name' => 'Pilotaje  y Técnicas de Navegación (Aéreo, Fluvial, Marítimo, Visual,  Instrumental, y/o Radar, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 16,
            'code' => 'Q.7',
            'name' => 'Logística Integral (Diseño, Producción, Entrega y Uso de un Producto o Servicio en el Mercado)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 16,
            'code' => 'Q.8',
            'name' => 'Cadenas de Abastecimiento (Información, Trazabilidad, Integración, Gestión de Nodos Logísticos Productivos Locales, Centros de Distribución Logística Internacional)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 16,
            'code' => 'Q.9',
            'name' => 'Manejo Integral de Bodegas y Almacenes',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 16,
            'code' => 'Q.10',
            'name' => 'Sistemas de Información Geográfica y Rutas',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 16,
            'code' => 'Q.10',
            'name' => 'Sistemas de Información Geográfica y Rutas',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 16,
            'code' => 'Q.11',
            'name' => 'Geodesia (Agrimensura, Cartografía, Fotogrametría, Topografía)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 17,
            'code' => 'R.12',
            'name' => 'Peluquería y Belleza, Barbería y Estilismo',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 17,
            'code' => 'R.13',
            'name' => 'Cosmetología',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 17,
            'code' => 'R.14',
            'name' => 'Artesanía (Cuero, Madera, Vidrio, Piedras, Metales, Telas, Cerámica, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 18,
            'code' => 'S.1',
            'name' => 'Gestión Cultural',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 18,
            'code' => 'S.4',
            'name' => 'Género',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 18,
            'code' => 'S.6',
            'name' => 'Salud y Medicina (Medicina General Tradicional y Alternativa, Nutrición, Tratamientos y Atención Infantil, Familiar, Ocupacional, Primeros Auxilios, Emergencias y Catástrofes, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 18,
            'code' => 'S.10',
            'name' => 'Servicios Domésticos',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 18,
            'code' => 'S.12',
            'name' => 'Servicios de Seguridad Física, Guardianía',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 19,
            'code' => 'T.1',
            'name' => 'Transformación de Productos, Subproductos (Agrícola, Ganadero, Pesca, Forestal)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 19,
            'code' => 'T.2',
            'name' => 'Conglomerados Agroindustriales (Cárnico, Madera, Lácteos, Frutas y Vegetales, Pescado, etc.)',
            'type' => 'specialty',
            'state_id' => 1,
        ]);

        // ethnic_origin
        
        Catalogue::factory()->create([
            'code' => '1',
            'name' => 'INDIGENA',
            'type' => 'ethnic_origin',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '2',
            'name' => 'AFROECUATORIANO',
            'type' => 'ethnic_origin',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '3',
            'name' => 'NEGRO',
            'type' => 'ethnic_origin',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '4',
            'name' => 'MULATO',
            'type' => 'ethnic_origin',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '5',
            'name' => 'MONTUBIO',
            'type' => 'ethnic_origin',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '6',
            'name' => 'MESTIZO',
            'type' => 'ethnic_origin',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '7',
            'name' => 'BLANCO',
            'type' => 'ethnic_origin',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '8',
            'name' => 'OTRO',
            'type' => 'ethnic_origin',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '9',
            'name' => 'NO REGISTRA',
            'type' => 'ethnic_origin',
            'state_id' => 1,
        ]);

        // sex
        
        Catalogue::factory()->create([
            'code' => '1',
            'name' => 'HOMBRE',
            'type' => 'sex',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '2',
            'name' => 'MUJER',
            'type' => 'sex',
            'state_id' => 1,
        ]);

        // gender
        
        Catalogue::factory()->create([
            'code' => '1',
            'name' => 'MASCULINO',
            'type' => 'gender',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '2',
            'name' => 'FEMENINO',
            'type' => 'gender',
            'state_id' => 1,
        ]);

        // identification_type
        
        Catalogue::factory()->create([
            'code' => '1',
            'name' => 'CEDULA',
            'type' => 'identification_type',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '2',
            'name' => 'PASAPORTE',
            'type' => 'identification_type',
            'state_id' => 1,
        ]);

        // blood_type
        
        Catalogue::factory()->create([
            'code' => '1',
            'name' => 'A+',
            'type' => 'blood_type',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '2',
            'name' => 'A-',
            'type' => 'blood_type',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '3',
            'name' => 'B+',
            'type' => 'blood_type',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '4',
            'name' => 'B-',
            'type' => 'blood_type',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '5',
            'name' => 'AB+',
            'type' => 'blood_type',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '6',
            'name' => 'AB-',
            'type' => 'blood_type',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '7',
            'name' => 'O+',
            'type' => 'blood_type',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => '8',
            'name' => 'O-',
            'type' => 'blood_type',
            'state_id' => 1,
        ]);

        // location
        
        Catalogue::factory()->create([
            'code' => 'ec',
            'name' => 'ECUADOR',
            'type' => 'country',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 30,
            'code' => '17',
            'name' => 'PICHINCHA',
            'type' => 'province',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'parent_code_id' => 30,
            'code' => '1',
            'name' => 'QUITO',
            'type' => 'canton',
            'state_id' => 1,
        ]);

          //catalogos jornada
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'MATUTINA',
            'type' => 'jornada',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'NOCTURNA',
            'type' => 'jornada',
            'state_id' => 1,
        ]);
        //catalogos paralelo
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'A',
            'type' => 'paralelo',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'B',
            'type' => 'paralelo',
            'state_id' => 1,
        ]);
        //display
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Lunes',
            'type' => 'Dias',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Martes',
            'type' => 'Dias',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Miercoles',
            'type' => 'Dias',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Jueves',
            'type' => 'Dias',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Viernes',
            'type' => 'Dias',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Sábado',
            'type' => 'Dias',
            'state_id' => 1,
        ]);
        
        Catalogue::factory()->create([
            'code' => 'pruebas',
            'name' => 'Domingo',
            'type' => 'Dias',
            'state_id' => 1,
        ]);

       //Authorities
       Catalogue::factory()->create([
        'code' => 'authoritiesState',
        'name' => 'Activo',
        'type' => 'Status',
        'state_id' => 1,
    ]);
       Catalogue::factory()->create([
        'code' => 'authoritiesState',
        'name' => 'Suspendido',
        'type' => 'Status',
        'state_id' => 1,
    ]);
    Catalogue::factory()->create([
        'code' => 'authoritiesState',
        'name' => 'Retirado',
        'type' => 'Status',
        'state_id' => 1,
    ]);
    Catalogue::factory()->create([
        'code' => 'authoritiesPosition',
        'name' => 'especialista en Cecy',
        'type' => 'position',
        'state_id' => 1,
    ]);
    Catalogue::factory()->create([
        'code' => 'authoritiesPosition',
        'name' => 'responsable del Cecy',
        'type' => 'position',
        'state_id' => 1,
    ]);
    
    Catalogue::factory()->create([
        'code' => 'authoritiesPosition',
        'name' => 'Logistica',
        'type' => 'position',
        'state_id' => 1,
    ]);

    Catalogue::factory()->create([
        'code' => 'estado',
        'name' => 'Abierto',
        'type' => 'state',
        'state_id' => 1,
    ]);
    Catalogue::factory()->create([
        'code' => 'estado',
        'name' => 'Cerrado',
        'type' => 'state',
        'state_id' => 1,
    ]);

        //evaluation_mechanisms -->catalogue
        Catalogue::factory()->create([
            'code' => '001',
            'name' => 'Evaluación Diagnostico',
            'type' => 'Evaluacion_mechanisms',
            'state_id' => 1,
        ]);

        Catalogue::factory()->create([
            'code' => '002',
            'name' => 'Evaluación proceso formativo',
            'type' => 'Evaluacion_mechanisms',
            'state_id' => 1,
        ]);

        Catalogue::factory()->create([
            'code' => '003',
            'name' => 'Evaluación final',
            'type' => 'Evaluacion_mechanisms',
            'state_id' => 1,
        ]);

        //level instruction

        Catalogue::factory()->create([
            'code' => 'P001',
            'name' => 'Primaria',
            'type' => 'level_instruction',
            'state_id' => 1,
        ]);

        Catalogue::factory()->create([
            'code' => 'P002',
            'name' => 'Secundaria',
            'type' => 'level_instruction',
            'state_id' => 1,
        ]);

        Catalogue::factory()->create([
            'code' => 'P003',
            'name' => 'Superior',
            'type' => 'level_instruction',
            'state_id' => 1,
        ]);

        Catalogue::factory()->create([
            'code' => 'P004',
            'name' => 'Universidad',
            'type' => 'level_instruction',
            'state_id' => 1,
        ]);



        // roles
        // Role::factory()->create([
        //    'id' => 12,
        //    'code' => '3',
        //    'name' => 'PARTICIPANTE',
        //    'system_id' => 1,
        //    'state_id' => 1,
        //]); */
        // Role::factory()->create([
        //    'code' => $catalogues['role']['name']['CECY_PARTICIPANTS'],
        //    'name' => 'PARTICIPANTE',
        //    'system_id' => $systemIgnug->id
        //]); */


        

        $user1 = User::factory()->create([
            'ethnic_origin_id' => 3,
            'identification' => "172307894",
            'first_name' => "Jefferson",
            'second_name' => "Alexander",
            'first_lastname' => "Aguirre",
            'second_lastname' => "Pizarro",
            'sex_id' => 22,
            'gender_id' => 24,
//          'personal_email' => $faker->unique()->safeEmail,
            'birthdate' => "02/12/2001",
//          'blood_type_id' => random_int(16, 23),
            'avatar' => "img.jpg",
            'username' => "Jeffer0212",
            'email' => "jeffersonaguirre02@outlook.com",
            'state_id' => 1,
            'status_id' => 1,
            'password' => '123456789', // 123456789
        ]);

        $user2 = User::factory()->create([
            'ethnic_origin_id' => 4,
            'identification' => "1723674586",
            'first_name' => "Ariana",
            'second_name' => "Mishell",
            'first_lastname' => "Binueza",
            'second_lastname' => "Sandoval",
            'sex_id' => 23,
            'gender_id' => 25,
//          'personal_email' => $faker->unique()->safeEmail,
            'birthdate' => "02/09/2002",
//          'blood_type_id' => random_int(16, 23),
            'avatar' => "img_2.jpg",
            'username' => "Ariana0212",
            'email' => "arianaSandoval2@outlook.com",
            'state_id' => 1,
            'status_id' => 1,
            'password' => '12345678', // 12345678
        ]);
        
        $user3 = User::factory()->create([
            'ethnic_origin_id' => 5,
            'identification' => "1723674586",
            'first_name' => "Diana",
            'second_name' => "Mishell",
            'first_lastname' => "Binueza",
            'second_lastname' => "Sandoval",
            'sex_id' => 23,
            'gender_id' => 25,
//          'personal_email' => $faker->unique()->safeEmail,
            'birthdate' => "28/12/2003",
//          'blood_type_id' => random_int(16, 23),
            'avatar' => "img_3.jpg",
            'username' => "Diana0212",
            'email' => "dianaSandoval2@outlook.com",
            'state_id' => 1,
            'status_id' => 1,
            'password' => '12345678', // 12345678
        ]);

        $user4 = User::factory()->create([
            'ethnic_origin_id' => 6,
            'identification' => "1723674586",
            'first_name' => "Carla",
            'second_name' => "Mishell",
            'first_lastname' => "Binueza",
            'second_lastname' => "Sandoval",
            'sex_id' => 23,
            'gender_id' => 25,
//          'personal_email' => $faker->unique()->safeEmail,
            'birthdate' => "01/11/2004",
//          'blood_type_id' => random_int(16, 23),
            'avatar' => "img_4.jpg",
            'username' => "Carla0212",
            'email' => "carlaSandoval2@outlook.com",
            'state_id' => 1,
            'status_id' => 1,
            'password' => '12345678', // 12345678
        ]);

        $user5 = User::factory()->create([
            'ethnic_origin_id' => 7,
            'identification' => "1723674586",
            'first_name' => "Camila",
            'second_name' => "Bella",
            'first_lastname' => "Chema",
            'second_lastname' => "Sandoval",
            'sex_id' => 23,
            'gender_id' => 25,
//          'personal_email' => $faker->unique()->safeEmail,
            'birthdate' => "01/07/2005",
//          'blood_type_id' => random_int(16, 23),
            'avatar' => "img_5.jpg",
            'username' => "Camila0212",
            'email' => "camilaSandoval2@outlook.com",
            'state_id' => 1,
            'status_id' => 1,
            'password' => '12345678', // 12345678
        ]);

        $user6 = User::factory()->create([
            'ethnic_origin_id' => 8,
            'identification' => "1723674586",
            'first_name' => "Jeniffer",
            'second_name' => "Mishell",
            'first_lastname' => "Binueza",
            'second_lastname' => "Sandoval",
            'sex_id' => 23,
            'gender_id' => 25,
//          'personal_email' => $faker->unique()->safeEmail,
            'birthdate' => "19/02/2005",
//          'blood_type_id' => random_int(16, 23),
            'avatar' => "img_6.jpg",
            'username' => "Jennifer0212",
            'email' => "jenniSandoval2@outlook.com",
            'state_id' => 1,
            'status_id' => 1,
            'password' => '12345678', // 12345678
        ]);

        $user7 = User::factory()->create([
            'ethnic_origin_id' => 9,
            'identification' => "1723674586",
            'first_name' => "Lourde",
            'second_name' => "Mishell",
            'first_lastname' => "Binueza",
            'second_lastname' => "Sandoval",
            'sex_id' => 23,
            'gender_id' => 25,
//         'personal_email' => $faker->unique()->safeEmail,
            'birthdate' => "19/02/2006",
//          'blood_type_id' => random_int(16, 23),
            'avatar' => "img_7.jpg",
            'username' => "Lourde0212",
            'email' => "lourdeSandoval2@outlook.com",
            'state_id' => 1,
            'status_id' => 1,
            'password' => '12345678', // 12345678
        ]);

        $user8 = User::factory()->create([
            'ethnic_origin_id' => 10,
            'identification' => "1723674586",
            'first_name' => "Gaby",
            'second_name' => "Mishell",
            'first_lastname' => "Binueza",
            'second_lastname' => "Sandoval",
            'sex_id' => 23,
            'gender_id' => 25,
//          'personal_email' => $faker->unique()->safeEmail,
            'birthdate' => "19/02/2007",
//          'blood_type_id' => random_int(16, 23),
            'avatar' => "img_8.jpg",
            'username' => "Gaby0212",
            'email' => "gabySandoval2@outlook.com",
            'state_id' => 1,
            'status_id' => 1,
            'password' => '12345678', // 12345678
        ]);

        $user9 = User::factory()->create([
            'ethnic_origin_id' => 4,
            'identification' => "1723674586",
            'first_name' => "Maria",
            'second_name' => "Mishell",
            'first_lastname' => "Binueza",
            'second_lastname' => "Sandoval",
            'sex_id' => 23,
            'gender_id' => 25,
//          'personal_email' => $faker->unique()->safeEmail,
            'birthdate' => "19/02/2008",
//          'blood_type_id' => random_int(16, 23),
            'avatar' => "img_9.jpg",
            'username' => "Maria0212",
            'email' => "mariaSandoval2@outlook.com",
            'state_id' => 1,
            'status_id' => 1,
            'password' => '12345678', // 12345678
        ]);

        $user10 = User::factory()->create([
            'ethnic_origin_id' => 6,
            'identification' => "1723674586",
            'first_name' => "Juanita",
            'second_name' => "Mishell",
            'first_lastname' => "Binueza",
            'second_lastname' => "Sandoval",
            'sex_id' => 23,
            'gender_id' => 25,
//          'personal_email' => $faker->unique()->safeEmail,
            'birthdate' => "19/02/2009",
//          'blood_type_id' => random_int(16, 23),
            'avatar' => "img_10.jpg",
            'username' => "Juanita0212",
            'email' => "juanitaSandoval2@outlook.com",
            'state_id' => 1,
            'status_id' => 1,
            'password' => '12345678', // 12345678
        ]);

         //horarios
         Schedule::factory()->create([
            'state_id' => 1,
            'start_time' => '14:00',
            'end_time' => '16:00',
            'day_id' => 268,
        ]);
        Schedule::factory()->create([
            'state_id' => 1,
            'start_time' => '14:00',
            'end_time' => '16:00',
            'day_id' => 274,
        ]);

        //Authoritie
        $authoritie1 = Authority::factory()->create([
            'user_id' => 1,
            'state_id'=> 1,
            'status_id' => 275,
            'position_id' => 277,
            'start_position' => "2020-01-11",
            'end_position'=> "2020-12-11",
        ]);
        $authoritie2 = Authority::factory()->create([
            'user_id' => 2,
            'state_id'=> 1,
            'status_id' => 275,
            'position_id' => 277,
            'start_position' => "2020-01-11",
            'end_position'=> "2020-12-11",
        ]);
        $authoritie3 = Authority::factory()->create([
            'user_id' => 3,
            'state_id'=> 1,
            'status_id' => 275,
            'position_id' => 277,
            'start_position' => "2020-01-11",
            'end_position'=> "2020-12-11",
        ]);
        $authoritie1 = Authority::factory()->create([
            'user_id' => 4,
            'state_id'=> 1,
            'status_id' => 275,
            'position_id' => 277,
            'start_position' => "2020-01-11",
            'end_position'=> "2020-12-11",
        ]);

        // use models ignug,institution -->polimorfismos
        $institution1 = Institution::factory()->create([
            'state_id' => 1, //activo
            'authority_id' => 1, 
            'ruc' => "0123456789124",
            'logo' => "http://logo.jpg",
            'name' => "INSTITUTO TECNOLOGICO SUPERIOS DE PATRIMONIO Y CULTURA YAVIRAC",
            'slogan' => "YAVIRAC",
            'code' => "I001",
        ]);

        
        // Instructors
        $instructors1 = Instructor::factory()->create([
            'state_id' => 1,
            'user_id' => 2,
        ]);

        $instructors2 = Instructor::factory()->create([
            'state_id' => 1,
            'user_id' => 3,
        ]);

        $instructors3 = Instructor::factory()->create([
            'state_id' => 1,
            'user_id' => 4,
        ]);

        $instructors4 = Instructor::factory()->create([
            'state_id' => 1,
            'user_id' => 5,
        ]);

        $instructors5 = Instructor::factory()->create([
            'state_id' => 1,
            'user_id' => 6,
        ]);

        $instructors6 = Instructor::factory()->create([
            'state_id' => 1,
            'user_id' => 7,
        ]);

        $instructors7 = Instructor::factory()->create([
            'state_id' => 1,
            'user_id' => 8,
        ]);

        $instructors7 = Instructor::factory()->create([
            'state_id' => 1,
            'user_id' => 9,
        ]);

        //participant
        $participant1 = Participant::factory()->create([
            "user_id" => 3,
            "person_type_id" => 21, //Adulto
            "state_id" => 1,
        ]);

        $participant2 = Participant::factory()->create([
            "user_id" => 4,
            "person_type_id" => 21, //Adulto
            "state_id" => 1,
        ]);
        
        $participant3 = Participant::factory()->create([
            "user_id" => 5,
            "person_type_id" => 22, //Adulto
            "state_id" => 1,
        ]);

        $participant4 = Participant::factory()->create([
            "user_id" => 6,
            "person_type_id" => 21, //Adulto
            "state_id" => 1,
        ]);

        $participant5 = Participant::factory()->create([
            "user_id" => 7,
            "person_type_id" => 22, //estudiante
            "state_id" => 1,
        ]);

        $participant6 = Participant::factory()->create([
            "user_id" => 8,
            "person_type_id" => 23, //Profesor
            "state_id" => 1,
        ]);

        
        // courses
        $course1 = Course::factory()->create([
            'code' => 'YEC-ST',
            'name' => 'STARTER',
            'cost' => 0,
            'photo'  => 'imagen.jpg',
            //'summary' => '',
            'duration' => 80,
            'modality_id' => 24, //Presencial
            'free' => true,
            'state_id' => 1, //activo
            'observation' => 'Happiness is like a kiss, you must share it to enjoy it.',
            'objective' => 'Be kind whenever possible. It is always possible',
           // 'needs' => '["test"]',
            'facilities' => [
                'CENEPA',
                'YAVIRAC',
            ],
            'theoretical_phase' => [   
                'At the end of the day, if I can say I had fun, it was a good day.',
                ' If you love someone, set them free. If they come back they’re yours; if they don’t they never were.',
            ],
            'practical_phase' => [
                'You only live once, but if you do it right, once is enough.',
                'Everything seems impossible until it is done.',
            ],
            'main_topics'=> [
                'TEMA Hazardous Materials Operations Level Course',
                'Hazardous Materials Team Operations Course',
                'Radiation Response Training Levels',
                'MERRTT: Modular Emergency Response Radiological Transportation Training',
                'FCRR: Fundamentals Course for Radiological Response',
                'Professional Development Series'
            ],//temas principales
            'secondary_topics' => [
                'subtopic 1',
                'subtopic 2',
                'subtopic 3',
                'subtopic 4',
                'subtopic 5'
            ],//subtemas
            'cross_cutting_topics' => [
                'cross cutting topics 1',
                'cross cutting topics 2',
                'cross cutting topics 3',
                'cross cutting topics 4',
                'cross cutting topics 5',
            ],//temas traversales
            'bibliography' => [
                'A riveting, deeply personal account of history 
                in the making—from the president who inspired us 
                to believe in the power of democracy.'
            ],
            'teaching_strategies' => [
                'Love is the only force capable of transforming an enemy into a friend.',
                'Love is life. And if you miss love, you miss life'
            ],
            'participant_type_id' => 21, //estudiantes
            'area_id' => 2,
            "level_id" => 26, //niveles curso
            "required_installing_sources" => "need install system(www.system_test.com)",
            "practice_hours" => 40,
            "theory_hours" => 40,
            "practice_required_resources" => "need computer or smartphone, internet",
            "aimtheory_required_resources" => "need to pass the first course",
            "learning_teaching_strategy" => "It’s one thing to fall in love. It’s another to feel someone else fall in love with you, and to feel a responsibility toward that love.",
            "person_proposal_id" => 1,
            "proposed_date" => "2020-08-26",
            "approval_date" => "2020-08-26",
            //"need_date" => "2020-08-26",
            "local_proposal" => "CENEPA",
            "project" => "",
            "capacity"=>20,
            // "classroom_id" => "",
            "course_type_id" => 31,
            "specialty_id" => 51,//especialida de la area
            "academic_period_id" => 32,
            "institution_id" => 1,
            "setec_name" => "CAPACITACIÓN CONTINUA EN EL IDIOMA INGLÉS STARTER",
        ]);
        $course2 = Course::factory()->create([
            'code' => 'YEC-A1.1',
            'name' => 'A1.1',
            'cost' => 0,
            'photo'  => 'imagen.jpg',
           // 'summary' => '',
            'duration' => 80,
            'modality_id' => 26, //virtual
            'free' => true,
            'state_id' => 1, //activo
            'observation' => 'Happiness is like a kiss, you must share it to enjoy it.',
            'objective' => 'Be kind whenever possible. It is always possible',
            //'needs' => '["test"]',
            'facilities' => [
                'CENEPA',
            ],
            'theoretical_phase' => [   
                'At the end of the day, if I can say I had fun, it was a good day.',
                ' If you love someone, set them free. If they come back they’re yours; if they don’t they never were.',
            ],
            'practical_phase' => [
                'You only live once, but if you do it right, once is enough.',
                'Everything seems impossible until it is done.',
            ],
            'main_topics'=> [
                'TEMA Hazardous Materials Operations Level Course',
                'Hazardous Materials Team Operations Course',
                'Radiation Response Training Levels',
                'MERRTT: Modular Emergency Response Radiological Transportation Training',
                'FCRR: Fundamentals Course for Radiological Response',
                'Professional Development Series'
            ],//temas principales
            'secondary_topics' => [
                'subtopic 1',
                'subtopic 2',
                'subtopic 3',
                'subtopic 4',
                'subtopic 5'
            ],//subtemas
            'cross_cutting_topics' => [
                'cross cutting topics 1',
                'cross cutting topics 2',
                'cross cutting topics 3',
                'cross cutting topics 4',
                'cross cutting topics 5',
            ],//temas traversales
            'bibliography' => [
                'A riveting, deeply personal account of history 
                in the making—from the president who inspired us 
                to believe in the power of democracy.'
            ],
            'teaching_strategies' => [
                'Love is the only force capable of transforming an enemy into a friend.',
                'Love is life. And if you miss love, you miss life'
            ],
            'participant_type_id' => 21, //estudiantes
            'area_id' => 3,
            "level_id" => 26, //niveles curso
            "required_installing_sources" => "need install system(www.system_test.com)",
            "practice_hours" => 40,
            "theory_hours" => 40,
            "practice_required_resources" => "need computer or smartphone, internet",
            "aimtheory_required_resources" => "need to pass the first course",
            "learning_teaching_strategy" => "It’s one thing to fall in love. It’s another to feel someone else fall in love with you, and to feel a responsibility toward that love.",
            "person_proposal_id" => 1,
            "proposed_date" => "2020-08-26",
            "approval_date" => "2020-08-26",
           // "need_date" => "2020-08-26",
            "local_proposal" => "CENEPA",
            "project" => "",
            "capacity"=>20,
            // "classroom_id" => "",
            "course_type_id" => 31,
            "specialty_id" => 41,
            "academic_period_id" => 64,
            "institution_id" => 1,
            "setec_name" => "CAPACITACIÓN CONTINUA EN EL IDIOMA INGLÉS A1.1",
        ]);
        $course3 = Course::factory()->create([
            'code' => 'YEC-A1.2',
            'name' => 'A1.2',
            'cost' => 0,
            'photo'  => 'imagen.jpg',
           // 'summary' => '',
            'duration' => 80,
            'modality_id' => 24, //virtual
            'free' => true,
            'state_id' => 1, //activo
            'observation' => 'Happiness is like a kiss, you must share it to enjoy it.',
            'objective' => 'Be kind whenever possible. It is always possible',
            //'needs' => '["test"]',
            'facilities' => [
                'CENEPA',
                'YAVIRAC',
            ],
            'theoretical_phase' => [   
                'At the end of the day, if I can say I had fun, it was a good day.',
                ' If you love someone, set them free. If they come back they’re yours; if they don’t they never were.',
            ],
            'practical_phase' => [
                'You only live once, but if you do it right, once is enough.',
                'Everything seems impossible until it is done.',
            ],
            'main_topics'=> [
                'TEMA Hazardous Materials Operations Level Course',
                'Hazardous Materials Team Operations Course',
                'Radiation Response Training Levels',
                'MERRTT: Modular Emergency Response Radiological Transportation Training',
                'FCRR: Fundamentals Course for Radiological Response',
                'Professional Development Series'
            ],//temas principales
            'secondary_topics' => [
                'subtopic 1',
                'subtopic 2',
                'subtopic 3',
                'subtopic 4',
                'subtopic 5'
            ],//subtemas
            'cross_cutting_topics' => [
                'cross cutting topics 1',
                'cross cutting topics 2',
                'cross cutting topics 3',
                'cross cutting topics 4',
                'cross cutting topics 5',
            ],//temas traversales
            'bibliography' => [
                'A riveting, deeply personal account of history 
                in the making—from the president who inspired us 
                to believe in the power of democracy.'
            ],
            'teaching_strategies' => [
                'Love is the only force capable of transforming an enemy into a friend.',
                'Love is life. And if you miss love, you miss life'
            ],
            'participant_type_id' => 21, //estudiantes
            'area_id' => 5,
            "level_id" => 26, //niveles curso
            "required_installing_sources" => "need install system(www.system_test.com)",
            "practice_hours" => 40,
            "theory_hours" => 40,
            "practice_required_resources" => "need computer or smartphone, internet",
            "aimtheory_required_resources" => "need to pass the first course",
            "learning_teaching_strategy" => "It’s one thing to fall in love. It’s another to feel someone else fall in love with you, and to feel a responsibility toward that love.",
            "person_proposal_id" => 1,
            "proposed_date" => "2020-08-26",
            "approval_date" => "2020-08-26",
           // "need_date" => "2020-08-26",
            "local_proposal" => "CENEPA",
            "project" => "",
            "capacity"=>20,
            // "classroom_id" => "",
            "course_type_id" => 31,
            "specialty_id" => 88,
            "academic_period_id" => 34,
            "institution_id" => 1,
            "setec_name" => "CAPACITACIÓN CONTINUA EN EL IDIOMA INGLÉS A1",
        ]);
        $course4 = Course::factory()->create([
            'code' => 'YEC-A2.1',
            'name' => 'A2.1',
            'cost' => 0,
            'photo'  => 'imagen.jpg',
           // 'summary' => '',
            'duration' => 80,
            'modality_id' => 24, //presencial
            'free' => true,
            'state_id' => 1, //activo
            'observation' => 'Happiness is like a kiss, you must share it to enjoy it.',
            'objective' => 'Be kind whenever possible. It is always possible',
           // 'needs' => '["test"]',
            'facilities' => [
                'YAVIRAC',
            ],
            'theoretical_phase' => [   
                'At the end of the day, if I can say I had fun, it was a good day.',
                ' If you love someone, set them free. If they come back they’re yours; if they don’t they never were.',
            ],
            'practical_phase' => [
                'You only live once, but if you do it right, once is enough.',
                'Everything seems impossible until it is done.',
            ],
            'main_topics'=> [
                'TEMA Hazardous Materials Operations Level Course',
                'Hazardous Materials Team Operations Course',
                'Radiation Response Training Levels',
                'MERRTT: Modular Emergency Response Radiological Transportation Training',
                'FCRR: Fundamentals Course for Radiological Response',
                'Professional Development Series'
            ],//temas principales
            'secondary_topics' => [
                'subtopic 1',
                'subtopic 2',
                'subtopic 3',
                'subtopic 4',
                'subtopic 5'
            ],//subtemas
            'cross_cutting_topics' => [
                'cross cutting topics 1',
                'cross cutting topics 2',
                'cross cutting topics 3',
                'cross cutting topics 4',
                'cross cutting topics 5',
            ],//temas traversales
            'bibliography' => [
                'A riveting, deeply personal account of history 
                in the making—from the president who inspired us 
                to believe in the power of democracy.'
            ],
            'teaching_strategies' => [
                'Love is the only force capable of transforming an enemy into a friend.',
                'Love is life. And if you miss love, you miss life'
            ],
            'participant_type_id' => 21, //estudiantes
            'area_id' => 5,
            "level_id" => 26, //niveles curso
            "required_installing_sources" => "need install system(www.system_test.com)",
            "practice_hours" => 40,
            "theory_hours" => 40,
            "practice_required_resources" => "need computer or smartphone, internet",
            "aimtheory_required_resources" => "need to pass the first course",
            "learning_teaching_strategy" => "It’s one thing to fall in love. It’s another to feel someone else fall in love with you, and to feel a responsibility toward that love.",
            "person_proposal_id" => 1,
            "proposed_date" => "2020-08-26",
            "approval_date" => "2020-08-26",
           // "need_date" => "2020-08-26",
            "local_proposal" => "CENEPA",
            "project" => "",
            "capacity"=>20,
            // "classroom_id" => "",
            "course_type_id" => 31,
            "specialty_id" => 89,
            "academic_period_id" => 35,
            "institution_id" => 1,
            "setec_name" => "CAPACITACIÓN CONTINUA EN EL IDIOMA INGLÉS A2.1",
        ]);
        $course5 = Course::factory()->create([
            'code' => 'YEC-A2.2',
            'name' => 'A2.2',
            'cost' => 0,
            'photo'  => 'imagen.jpg',
        //    'summary' => '',
            'duration' => 80,
            'modality_id' => 24, //presencial
            'free' => true,
            'state_id' => 1, //activo
            'observation' => 'Happiness is like a kiss, you must share it to enjoy it.',
            'objective' => 'Be kind whenever possible. It is always possible',
           // 'needs' => '["test"]',
            'facilities' => [
                'CENEPA',
                'YAVIRAC',
            ],
            'theoretical_phase' => [   
                'At the end of the day, if I can say I had fun, it was a good day.',
                ' If you love someone, set them free. If they come back they’re yours; if they don’t they never were.',
            ],
            'practical_phase' => [
                'You only live once, but if you do it right, once is enough.',
                'Everything seems impossible until it is done.',
            ],
            'main_topics'=> [
                'TEMA Hazardous Materials Operations Level Course',
                'Hazardous Materials Team Operations Course',
                'Radiation Response Training Levels',
                'MERRTT: Modular Emergency Response Radiological Transportation Training',
                'FCRR: Fundamentals Course for Radiological Response',
                'Professional Development Series'
            ],//temas principales
            'secondary_topics' => [
                'subtopic 1',
                'subtopic 2',
                'subtopic 3',
                'subtopic 4',
                'subtopic 5'
            ],//subtemas
            'cross_cutting_topics' => [
                'cross cutting topics 1',
                'cross cutting topics 2',
                'cross cutting topics 3',
                'cross cutting topics 4',
                'cross cutting topics 5',
            ],//temas traversales
            'bibliography' => [
                'A riveting, deeply personal account of history 
                in the making—from the president who inspired us 
                to believe in the power of democracy.'
            ],
            'teaching_strategies' => [
                'Love is the only force capable of transforming an enemy into a friend.',
                'Love is life. And if you miss love, you miss life'
            ],
            'participant_type_id' => 21, //estudiantes
            'area_id' => 6,
            "level_id" => 26, //niveles curso
            "required_installing_sources" => "need install system(www.system_test.com)",
            "practice_hours" => 40,
            "theory_hours" => 40,
            "practice_required_resources" => "need computer or smartphone, internet",
            "aimtheory_required_resources" => "need to pass the first course",
            "learning_teaching_strategy" => "It’s one thing to fall in love. It’s another to feel someone else fall in love with you, and to feel a responsibility toward that love.",
            "person_proposal_id" => 1,
            "proposed_date" => "2020-08-26",
            "approval_date" => "2020-08-26",
        //    "need_date" => "2020-08-26",
            "local_proposal" => "CENEPA",
            "project" => "",
            "capacity"=>20,
            // "classroom_id" => "",
            "course_type_id" => 31,
            "specialty_id" => 39,
            "academic_period_id" => 36,
            "institution_id" => 1,
            "setec_name" => "CAPACITACIÓN CONTINUA EN EL IDIOMA INGLÉS A2",
        ]);
        $course6 = Course::factory()->create([
            'code' => 'YEC-B1.1',
            'name' => 'B1.1',
            'cost' => 0,
            'photo'  => 'imagen.jpg',
           // 'summary' => '',
            'duration' => 80,
            'modality_id' => 26, //Virtual
            'free' => true,
            'state_id' => 1, //activo
            'observation' => 'Happiness is like a kiss, you must share it to enjoy it.',
            'objective' => 'Be kind whenever possible. It is always possible',
            //'needs' => '["test"]',
            'facilities' => [
                'YAVIRAC',
            ],
            'theoretical_phase' => [   
                'At the end of the day, if I can say I had fun, it was a good day.',
                ' If you love someone, set them free. If they come back they’re yours; if they don’t they never were.',
            ],
            'practical_phase' => [
                'You only live once, but if you do it right, once is enough.',
                'Everything seems impossible until it is done.',
            ],
            'main_topics'=> [
                'TEMA Hazardous Materials Operations Level Course',
                'Hazardous Materials Team Operations Course',
                'Radiation Response Training Levels',
                'MERRTT: Modular Emergency Response Radiological Transportation Training',
                'FCRR: Fundamentals Course for Radiological Response',
                'Professional Development Series'
            ],//temas principales
            'secondary_topics' => [
                'subtopic 1',
                'subtopic 2',
                'subtopic 3',
                'subtopic 4',
                'subtopic 5'
            ],//subtemas
            'cross_cutting_topics' => [
                'cross cutting topics 1',
                'cross cutting topics 2',
                'cross cutting topics 3',
                'cross cutting topics 4',
                'cross cutting topics 5',
            ],//temas traversales
            'bibliography' => [
                'A riveting, deeply personal account of history 
                in the making—from the president who inspired us 
                to believe in the power of democracy.'
            ],
            'teaching_strategies' => "[
                'Love is the only force capable of transforming an enemy into a friend.',
                'Love is life. And if you miss love, you miss life'
            ]",
            'participant_type_id' => 21, //estudiantes
            'area_id' => 7,
            "level_id" => 26, //niveles curso
            "required_installing_sources" => "need install system(www.system_test.com)",
            "practice_hours" => 40,
            "theory_hours" => 40,
            "practice_required_resources" => "need computer or smartphone, internet",
            "aimtheory_required_resources" => "need to pass the first course",
            "learning_teaching_strategy" => "It’s one thing to fall in love. It’s another to feel someone else fall in love with you, and to feel a responsibility toward that love.",
            "person_proposal_id" => 1,
            "proposed_date" => "2020-08-26",
            "approval_date" => "2020-08-26",
           // "need_date" => "2020-08-26",
            "local_proposal" => "CENEPA",
            "project" => "",
            "capacity"=>20,
            // "classroom_id" => "",
            "course_type_id" => 123,
            "specialty_id" => 39,
            "academic_period_id" => 37,
            "institution_id" => 1,
            "setec_name" => "CAPACITACIÓN CONTINUA EN EL IDIOMA INGLÉS B1.1",
        ]);
        $course7 = Course::factory()->create([
            'code' => 'YEC-B1.2',
            'name' => 'B1.2',
            'cost' => 0,
            'photo'  => 'imagen.jpg',
          //  'summary' => '',
            'duration' => 80,
            'modality_id' => 26, //Virtual
            'free' => true,
            'state_id' => 1, //activo
            'observation' => 'Happiness is like a kiss, you must share it to enjoy it.',
            'objective' => 'Be kind whenever possible. It is always possible',
           // 'needs' => '["test"]',
            'facilities' => [
                'CENEPA',
                'YAVIRAC',
            ],
            'theoretical_phase' => [   
                'At the end of the day, if I can say I had fun, it was a good day.',
                ' If you love someone, set them free. If they come back they’re yours; if they don’t they never were.',
            ],
            'practical_phase' => [
                'You only live once, but if you do it right, once is enough.',
                'Everything seems impossible until it is done.',
            ],
            'main_topics'=> [
                'TEMA Hazardous Materials Operations Level Course',
                'Hazardous Materials Team Operations Course',
                'Radiation Response Training Levels',
                'MERRTT: Modular Emergency Response Radiological Transportation Training',
                'FCRR: Fundamentals Course for Radiological Response',
                'Professional Development Series'
            ],//temas principales
            'secondary_topics' => [
                'subtopic 1',
                'subtopic 2',
                'subtopic 3',
                'subtopic 4',
                'subtopic 5'
            ],//subtemas
            'cross_cutting_topics' => [
                'cross cutting topics 1',
                'cross cutting topics 2',
                'cross cutting topics 3',
                'cross cutting topics 4',
                'cross cutting topics 5',
            ],//temas traversales
            'bibliography' => "[
                'A riveting, deeply personal account of history 
                in the making—from the president who inspired us 
                to believe in the power of democracy.'
            ]",
            'teaching_strategies' => "[
                'Love is the only force capable of transforming an enemy into a friend.',
                'Love is life. And if you miss love, you miss life'
            ]",
            'participant_type_id' => 21, //estudiantes
            'area_id' => 4,
            "level_id" => 26, //niveles curso
            "required_installing_sources" => "need install system(www.system_test.com)",
            "practice_hours" => 40,
            "theory_hours" => 40,
            "practice_required_resources" => "need computer or smartphone, internet",
            "aimtheory_required_resources" => "need to pass the first course",
            "learning_teaching_strategy" => "It’s one thing to fall in love. It’s another to feel someone else fall in love with you, and to feel a responsibility toward that love.",
            "person_proposal_id" => 1,
            "proposed_date" => "2020-08-26",
            "approval_date" => "2020-08-26",
            //"need_date" => "2020-08-26",
            "local_proposal" => "CENEPA",
            "project" => "",
            "capacity"=>20,
            // "classroom_id" => "",
            "course_type_id" => 31,
            "specialty_id" => 71,
            "academic_period_id" => 38,
            "institution_id" => 1,
            "setec_name" => "CAPACITACIÓN CONTINUA EN EL IDIOMA INGLÉS B1",
        ]);

        // school_periods
        SchoolPeriod::factory()->create([
            'code' => "test",
            'name' => "test",
            'start_date' => "2020-08-28",
            'end_date' => "2020-08-28",
            'ordinary_start_date' => "2020-08-28",
            'ordinary_end_date' => "2020-08-28",
            'extraordinary_start_date' => "2020-08-28",
            'extraordinary_end_date' => "2020-08-28",
            'especial_start_date' => "2020-08-28",
            'especial_end_date' => "2020-08-28",
            "state_id" => 1,
            "status_id" => 281,
        ]);

        //evaluation_Mechanisms

        $evaluation1 = EvaluationMechanism::factory()->create([
            "state_id" => 1,
            'type_id' => 274,
            'technique' => "test",
            'instrument' => "test2",
        ]);

        $evaluation2 = EvaluationMechanism::factory()->create([
            "state_id" => 1,
            'type_id' => 275,
            'technique' => "test",
            'instrument' => "test2",
        ]);

        $evaluation3 = EvaluationMechanism::factory()->create([
            "state_id" => 1,
            'type_id' => 276,
            'technique' => "test",
            'instrument' => "test2",
        ]);

        //prerequisites

        $prerequisites1 = Prerequisite::factory()->create([
            'course_id' => 1,
            "state_id" => 1,
            'parent_code_id' => 1,
        ]);

        $prerequisites2 = Prerequisite::factory()->create([
            'course_id' => 1,
            "state_id" => 1,
            'parent_code_id' => 2,
        ]);

        $prerequisites3 = Prerequisite::factory()->create([
            'course_id' => 1,
            "state_id" => 1,
            'parent_code_id' => 3,
        ]);
        
        //$prerequisites1->parents()->associate($evaluation2);
        
        
        
        //horarios
        $schedule1 = Schedule::factory()->create([
            "state_id" => 1,
            'start_time' =>'14:00',
            'end_time' =>'16:00',
            'day_id' => 29,
        ]);
        $schedule2 = Schedule::factory()->create([
            "state_id" => 1,
            'start_time' =>'16:00',
            'end_time' =>'18:00',
            'day_id' => 268,
        ]);
        $schedule3 = Schedule::factory()->create([
            "state_id" => 1,
            'start_time' =>'09:00',
            'end_time' =>'11:00',
            'day_id' => 268,
        ]);
        
        $schedule1->courses()->attach($course1->id);
        $schedule1->courses()->attach($course2->id);
        $schedule1->courses()->attach($course3->id);
        $schedule2->courses()->attach($course4->id);
        $schedule2->courses()->attach($course5->id);
        $schedule3->courses()->attach($course6->id);
        $schedule3->courses()->attach($course7->id);

        //planificacion
        Planification::factory()->create([
            'date_start' => '2020-01-01',
            'date_end' => '2020-01-28',
            'course_id' => 1,
            'needs' => ['sndfk','afdsa'],
            //'school_period_id' => null,
            'state_id' => 1,
           // 'status_id' => 1,
        ]);
        
        Planification::factory()->create([
            'date_start' => '2020-02-01',
            'date_end' => '2020-02-28',
            'course_id' => 2    ,
            'needs' => ['sndfk','afdsa'],
            //'school_period_id' => null,
            'state_id' => 1,
           // 'status_id' => 1,
        ]);

        Planification::factory()->create([
            'date_start' => '2020-03-08',
            'date_end' => '2020-03-28',
            'course_id' => 3,
            'needs' => ['sndfk','afdsa'],
            //'school_period_id' => 1,
            'state_id' => 1,
           // 'status_id' => 1,
        ]);

        Planification::factory()->create([
            'date_start' => '2020-04-08',
            'date_end' => '2020-04-28',
            'course_id' => 4,
            'needs' => ['sndfk','afdsa'],
            //'school_period_id' => 1,
            'state_id' => 1,
           // 'status_id' => 1,
        ]);

        Planification::factory()->create([
            'date_start' => '2020-05-09',
            'date_end' => '2020-05-28',
            'course_id' => 5,
            'needs' => ['sndfk','afdsa'],
            //'school_period_id' => 1,
            'state_id' => 1,
           // 'status_id' => 1,
        ]);

        Planification::factory()->create([
            'date_start' => '2020-06-02',
            'date_end' => '2020-06-28',
            'course_id' => 6,
            'needs' => ['sndfk','afdsa'],
            //'school_period_id' => 1,
            'state_id' => 1,
           // 'status_id' => 1,
        ]);

        Planification::factory()->create([
            'date_start' => '2020-07-28',
            'date_end' => '2020-07-28',
            'course_id' => 7,
            'needs' => ['sndfk','afdsa'],
            //'school_period_id' => 1,
            'state_id' => 1,
           // 'status_id' => 1,
        ]);

        Planification::factory()->create([
            'date_start' => '2020-08-18',
            'date_end' => '2020-08-28',
            'course_id' => 1,
            'needs' => ['sndfk','afdsa'],
            //'school_period_id' => 1,
            'state_id' => 1,
           // 'status_id' => 1,
        ]);

        Planification::factory()->create([
            'date_start' => '2020-09-01',
            'date_end' => '2020-09-28',
            'course_id' => 2,
            'needs' => ['sndfk','afdsa'],
            //'school_period_id' => 1,
            'state_id' => 1,
           // 'status_id' => 1,
        ]);

        Planification::factory()->create([
            'date_start' => '2020-10-09',
            'date_end' => '2020-10-28',
            'course_id' => 3,
            'needs' => ['sndfk','afdsa'],
            //'school_period_id' => 1,
            'state_id' => 1,
           // 'status_id' => 1,
        ]);

        Planification::factory()->create([
            'date_start' => '2020-11-02',
            'date_end' => '2020-11-28',
            'course_id' => 4,
            'needs' => ['sndfk','afdsa'],
            //'school_period_id' => 1,
            'state_id' => 1,
           // 'status_id' => 1,
        ]);

        Planification::factory()->create([
            'date_start' => '2020-12-02',
            'date_end' => '2020-12-28',
            'course_id' => 3,
            'needs' => ['sndfk','afdsa'],
            //'school_period_id' => 1,
            'state_id' => 1,
           // 'status_id' => 1,
        ]);

        //additional information
        $additionalInformation1 = AdditionalInformation::factory()->create([
            'company_name' => 'ABC',
            'company_address' => 'Av. colon y Av. Amazonas',
            'company_phone' => '30997225',
            'company_activity' => 'textil',
            'company_sponsor'=> true,
            'name_contact' => 'Carlos',
            'know_course'=> [
                'For company',
                'and saw on social network'
            ],
            'course_follow' => [
                'methodology',
                'Excel',
            ],
            'works' => true,
            'state_id' => 1,
            //'registration_id' => 1,
            'level_instruction' => 278, 
        ]);

        $additionalInformation2 = AdditionalInformation::factory()->create([
            'company_name' => 'ABC',
            'company_address' => 'Av. colon y Av. Amazonas',
            'company_phone' => '30997225',
            'company_activity' => 'textil',
            'company_sponsor'=> true,
            'name_contact' => 'Carlos',
            'know_course'=> [
                'For company',
                'and saw on social network'
            ],
            'course_follow' => [
                'methodology',
                'Excel',
            ],
            'works' => true,
            'state_id' => 1,
            //'registration_id' => 1,
            'level_instruction' => 278, 
        ]);

        $additionalInformation3 = AdditionalInformation::factory()->create([
            'company_name' => 'ABC',
            'company_address' => 'Av. colon y Av. Amazonas',
            'company_phone' => '30997225',
            'company_activity' => 'textil',
            'company_sponsor'=> false,
            'name_contact' => 'Carlos',
            'know_course'=> [
                'For company',
                'and saw on social network'
            ],
            'course_follow' => [
                'methodology',
                'Excel',
            ],
            'works' => true,
            'state_id' => 1,
            //'registration_id' => 2,
            'level_instruction' => 279, 
        ]);

        $additionalInformation4 = AdditionalInformation::factory()->create([
            'company_name' => 'ABC',
            'company_address' => 'Av. colon y Av. Amazonas',
            'company_phone' => '30997225',
            'company_activity' => 'textil',
            'company_sponsor'=> true,
            'name_contact' => 'Carlos',
            'know_course'=> [
                'For company',
                'and saw on social network'
            ],
            'course_follow' => [
                'methodology',
                'Excel',
            ],
            'works' => false,
            'state_id' => 1,
            //'registration_id' => 3,
            'level_instruction' => 280, 
        ]);


        $additionalInformation5 = AdditionalInformation::factory()->create([
            'company_name' => 'ABC',
            'company_address' => 'Av. colon y Av. Amazonas',
            'company_phone' => '30997225',
            'company_activity' => 'textil',
            'company_sponsor'=> false,
            'name_contact' => 'Carlos',
            'know_course'=> [
                'For company',
                'and saw on social network'
            ],
            'course_follow' => [
                'methodology',
                'Excel',
            ],
            'works' => false,
            'state_id' => 1,
            //'registration_id' => 4,
            'level_instruction' => 279, 
        ]);


        $additionalInformation6 = AdditionalInformation::factory()->create([
            'company_name' => 'ABC',
            'company_address' => 'Av. colon y Av. Amazonas',
            'company_phone' => '30997225',
            'company_activity' => 'textil',
            'company_sponsor'=> true,
            'name_contact' => 'Carlos',
            'know_course'=> [
                'For company',
                'and saw on social network'
            ],
            'course_follow' => [
                'methodology',
                'Excel',
            ],
            'works' => true,
            'state_id' => 1,
            //'registration_id' => 5,
            'level_instruction' => 280, 
        ]);


        $additionalInformation7 = AdditionalInformation::factory()->create([
            'company_name' => 'ABC',
            'company_address' => 'Av. colon y Av. Amazonas',
            'company_phone' => '30997225',
            'company_activity' => 'textil',
            'company_sponsor'=> true,
            'name_contact' => 'Carlos',
            'know_course'=> [
                'For company',
                'and saw on social network'
            ],
            'course_follow' => [
                'methodology',
                'Excel',
            ],
            'works' => true,
            'state_id' => 1,
            //'registration_id' => 6,
            'level_instruction' => 278, 
        ]);

        //registrations
        $registrations1 = Registration::factory()->create([
            'date_registration' => '2021-01-01',
            'participant_id' => 1,
            'state_id' => 1,
            'type_id' => 21,
            'number' => '0992399556',
            'additional_information_id' => 1,
            'planification_id' => 1,
        ]);

        $registrations2 = Registration::factory()->create([
            'date_registration' => '2021-01-01',
            'participant_id' => 2,
            'state_id' => 1,
            'type_id' => 22,
            'number' => '0992399556',
            'additional_information_id' => 2,
            'planification_id' => 1,
            
        ]);

        $registrations3 = Registration::factory()->create([
            'date_registration' => '2021-01-01',
            'participant_id' => 3,
            'state_id' => 1,
            'type_id' => 22,
            'number' => '0992399556',
            'additional_information_id' => 3,
            'planification_id' => 2,
            
        ]);

        $registrations4 = Registration::factory()->create([
            'date_registration' => '2021-01-01',
            'participant_id' => 4,
            'state_id' => 1,
            'type_id' => 21,
            'number' => '0992399556',
            'additional_information_id' => 4,
            'planification_id' => 2,
            
        ]);

        $registrations5 = Registration::factory()->create([
            'date_registration' => '2021-01-01',
            'participant_id' => 5,
            'state_id' => 1,
            'type_id' => 22,
            'number' => '0992399556',
            'additional_information_id' => 5,
            'planification_id' => 3,
            
        ]);


        $registrations6 = Registration::factory()->create([
            'date_registration' => '2021-01-01',
            'participant_id' => 6,
            'state_id' => 1,
            'type_id' => 21,
            'number' => '0992399556',
            'additional_information_id' => 6,
            'planification_id' => 3,
            
        ]);

        $registrations7 = Registration::factory()->create([
            'date_registration' => '2021-01-01',
            'participant_id' => 1,
            'state_id' => 1,
            'type_id' => 22,
            'number' => '0992399556',
            'additional_information_id' => 2,
            'planification_id' => 4,
            
        ]);

        $registrations8 = Registration::factory()->create([
            'date_registration' => '2021-01-01',
            'participant_id' => 2,
            'state_id' => 1,
            'type_id' => 22,
            'number' => '0992399556',
            'additional_information_id' => 2,
            'planification_id' => 5,
            
        ]);


        $registrations10 = Registration::factory()->create([
            'date_registration' => '2021-01-01',
            'participant_id' =>3,
            'state_id' => 1,
            'type_id' => 22,
            'number' => '0992399556',
            'additional_information_id' => 1,
            'planification_id' => 6,
            
        ]);

        //detail registration
        $detailRegistration1 = DetailRegistration::factory()->create([
            'registration_id'=> 1,
            'state_id' => 1,
            'status_id' => 100,
            'status_certificate_id' => 210,
            'final_grade' => '100',
            'certificate_withdrawn' => '2020-12-30',
            'observation' => [
                'Registration on process',
            ],
        ]);

        $detailRegistration2 = DetailRegistration::factory()->create([
            'registration_id'=> 2,
            'state_id' => 1,
            'status_id' => 100,
            'status_certificate_id' => 210,
            'final_grade' => '100',
            'certificate_withdrawn' => '2020-12-30',
            'observation' => [
                'Registration on process',
            ],
        ]);

        $detailRegistration3 = DetailRegistration::factory()->create([
            'registration_id'=> 3,
            'state_id' => 1,
            'status_id' => 100,
            'status_certificate_id' => 210,
            'final_grade' => '100',
            'certificate_withdrawn' => '2020-12-30',
            'observation' => [
                'Registration on process',
            ],
        ]);

        //datail planification
        $detailPlanification1 = DetailPlanification::factory()->create([
            'date_start' => '2020-01-28',
            'date_end' => '2020-01-28',
            'summary' => 'sfdsf',
            'planned_end_date' =>'2020-10-28',
            'planification_id' => 1,
            'course_id' => 1,
            'detail_registration_id' => 1,
            'instructor_id'  => 1,
            'state_id' => 1,
            //'status_id' => 1,
            //'schedule_id' => 1,
            //'school_period_id' => null,
            //'classroom_id' => 1,
            'capacity' => '20',
            'observation' => 'assddgf',
            'conference' => 263,
            //'responsible_id' => 1,
            'parallel' => 265,
            'needs' => ['sndfk','afdsa'],
            'need_date' =>'2020-10-28',    
        ]);

        $detailPlanification2 = DetailPlanification::factory()->create([
            'date_start' => '2020-02-28',
            'date_end' => '2020-02-28',
            'summary' => 'sfdsf',
            'planned_end_date' =>'2020-10-28',
            'planification_id' => 2,
            'course_id' => 2,
            'detail_registration_id' => 2,
            'instructor_id'  => 1,
            'state_id' => 1,
            //'status_id' => 1,
            //'schedule_id' => 1,
            //'school_period_id' => null,
            //'classroom_id' => 1,
            'capacity' => '20',
            'observation' => 'assddgf',
            'conference' => 263,
            //'responsible_id' => 1,
            'parallel' => 265,
            'needs' => ['sndfk','afdsa'],
            'need_date' =>'2020-10-28',    
        ]);

        $detailPlanification3 = DetailPlanification::factory()->create([
            'date_start' => '2020-03-28',
            'date_end' => '2020-03-28',
            'summary' => 'sfdsf',
            'planned_end_date' =>'2020-10-28',
            'planification_id' => 3,
            'course_id' => 3,
            'detail_registration_id' => 3,
            'instructor_id'  => 1,
            'state_id' => 1,
            //'status_id' => 1,
            //'schedule_id' => 1,
            //'school_period_id' => null,
            //'classroom_id' => 1,
            'capacity' => '20',
            'observation' => 'assddgf',
            'conference' => 263,
            //'responsible_id' => 1,
            'parallel' => 265,
            'needs' => ['sndfk','afdsa'],
            'need_date' =>'2020-10-28',    
        ]);

        $detailPlanification4 = DetailPlanification::factory()->create([
            'date_start' => '2020-04-28',
            'date_end' => '2020-04-28',
            'summary' => 'sfdsf',
            'planned_end_date' =>'2020-10-28',
            'planification_id' => 4,
            'course_id' => 4,
            'detail_registration_id' => 1,
            'instructor_id'  => 1,
            'state_id' => 1,
            //'status_id' => 1,
            //'schedule_id' => 1,
            //'school_period_id' => null,
            //'classroom_id' => 1,
            'capacity' => '20',
            'observation' => 'assddgf',
            'conference' => 263,
            //'responsible_id' => 1,
            'parallel' => 265,
            'needs' => ['sndfk','afdsa'],
            'need_date' =>'2020-10-28',    
        ]);

        $detailPlanification5 = DetailPlanification::factory()->create([
            'date_start' => '2020-03-28',
            'date_end' => '2020-05-28',
            'summary' => 'sfdsf',
            'planned_end_date' =>'2020-10-28',
            'planification_id' => 5,
            'course_id' => 5,
            'detail_registration_id' => 2,
            'instructor_id'  => 1,
            'state_id' => 1,
            //'status_id' => 1,
            //'schedule_id' => 1,
            //'school_period_id' => null,
            //'classroom_id' => 1,
            'capacity' => '20',
            'observation' => 'assddgf',
            'conference' => 263,
            //'responsible_id' => 1,
            'parallel' => 265,
            'needs' => ['sndfk','afdsa'],
            'need_date' =>'2020-10-28',    
        ]);

        $detailPlanification6 = DetailPlanification::factory()->create([
            'date_start' => '2020-03-28',
            'date_end' => '2020-06-28',
            'summary' => 'sfdsf',
            'planned_end_date' =>'2020-10-28',
            'planification_id' => 6,
            'course_id' => 6,
            'detail_registration_id' => 3,
            'instructor_id'  => 1,
            'state_id' => 1,
            //'status_id' => 1,
            //'schedule_id' => 1,
            //'school_period_id' => null,
            //'classroom_id' => 1,
            'capacity' => '20',
            'observation' => 'assddgf',
            'conference' => 263,
            //'responsible_id' => 1,
            'parallel' => 265,
            'needs' => ['sndfk','afdsa'],
            'need_date' =>'2020-10-28',    
        ]);

        $detailPlanification7 = DetailPlanification::factory()->create([
            'date_start' => '2020-03-28',
            'date_end' => '2020-07-28',
            'summary' => 'sfdsf',
            'planned_end_date' =>'2020-10-28',
            'planification_id' => 7,
            'course_id' => 7,
            'detail_registration_id' => 1,
            'instructor_id'  => 1,
            'state_id' => 1,
            //'status_id' => 1,
            //'schedule_id' => 1,
            //'school_period_id' => null,
            //'classroom_id' => 1,
            'capacity' => '20',
            'observation' => 'assddgf',
            'conference' => 263,
            //'responsible_id' => 1,
            'parallel' => 265,
            'needs' => ['sndfk','afdsa'],
            'need_date' =>'2020-10-28',    
        ]);

        $detailPlanification8 = DetailPlanification::factory()->create([
            'date_start' => '2020-03-28',
            'date_end' => '2020-08-28',
            'summary' => 'sfdsf',
            'planned_end_date' =>'2020-10-28',
            'planification_id' => 8,
            'course_id' => 7,
            'detail_registration_id' => 2,
            'instructor_id'  => 1,
            'state_id' => 1,
            //'status_id' => 1,
            //'schedule_id' => 1,
            //'school_period_id' => null,
            //'classroom_id' => 1,
            'capacity' => '20',
            'observation' => 'assddgf',
            'conference' => 263,
            //'responsible_id' => 1,
            'parallel' => 265,
            'needs' => ['sndfk','afdsa'],
            'need_date' =>'2020-10-28',    
        ]);
        
        $detailPlanification1->course()->associate($course1);
        $detailPlanification1->course()->associate($course2);
        $detailPlanification1->course()->associate($course3);
        $detailPlanification1->course()->associate($course4);
        $detailPlanification1->course()->associate($course5);
        $detailPlanification1->course()->associate($course6);
        $detailPlanification1->course()->associate($course7);

        $detailPlanification2->course()->associate($course1);
        $detailPlanification2->course()->associate($course2);
        $detailPlanification2->course()->associate($course3);
        $detailPlanification2->course()->associate($course4);
        $detailPlanification2->course()->associate($course5);
        $detailPlanification2->course()->associate($course6);
        $detailPlanification2->course()->associate($course7);
        
        $detailPlanification3->course()->associate($course1);
        $detailPlanification3->course()->associate($course2);
        
        $detailPlanification4->course()->associate($course3);
        
        $detailPlanification5->course()->associate($course4);
        
        $detailPlanification6->course()->associate($course3);
        
        $detailPlanification7->course()->associate($course1);
        
        $detailPlanification8->course()->associate($course3);
            
    }
}

//bdd
/*          drop schema if exists public cascade;
            drop schema if exists authentication cascade;
            drop schema if exists attendance cascade;
            drop schema if exists ignug cascade;
			drop schema if exists community cascade;
            drop schema if exists job_board cascade;
            drop schema if exists web cascade;
            drop schema if exists teacher_eval cascade;
            drop schema if exists cecy cascade;


            create schema community;
            create schema authentication;
            create schema attendance;
            create schema ignug;
            create schema job_board;
            create schema web;
            create schema teacher_eval;
            create schema cecy; */