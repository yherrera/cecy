<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql-cecy')->create('planifications', function (Blueprint $table) {
            $table->id();
            $table->date('date_start'); //fecha_inicio
            $table->date('date_end'); //fecha_fin
            $table->foreignId('course_id')->constrained('courses'); //course_id
            $table->json('needs'); //necesidades del curso es un array
            //$table->foreignId('responsable_id')->constrained('responsables'); //responsable_id
            //$table->foreignId('school_period_id')->constrained('ignug.school_periods')->nullable(); //school_period_id
            $table->foreignId('state_id')->constrained('ignug.states'); //states_id
            //$table->foreignId('status_id')->constrained('ignug.status'); //statu_id
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planifications');
    }
}
