<?php

namespace Database\Factories\Cecy;

use App\Models\Cecy\SchoolPeriod;
use Illuminate\Database\Eloquent\Factories\Factory;

class SchoolPeriodFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SchoolPeriod::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code' => $this->faker->word,
            'name' => $this->faker->word,
            'start_date' => $this->faker->date,
            'end_date' => $this->faker->date,
            'ordinary_start_date' => $this->faker->date,
            'ordinary_end_date' => $this->faker->date,
            //'cancel_ordinary' => $this->faker->date,
            'extraordinary_start_date' => $this->faker->date,
            'extraordinary_end_date' => $this->faker->date,
            //'extraordinary_cancel_end' => $this->faker->date,
            'especial_start_date' => $this->faker->date,
            'especial_end_date' => $this->faker->date,
            'state_id' => 1,
        ];
    }
}
