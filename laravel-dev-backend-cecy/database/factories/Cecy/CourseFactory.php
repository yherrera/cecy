<?php

namespace Database\Factories\Cecy;

use App\Models\Cecy\Course;
use Illuminate\Database\Eloquent\Factories\Factory;

class CourseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Course::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
        'code' => $this->faker->word,
        'name' => $this->faker->word,
        'cost' => $this->faker->word,
        'photo' => $this->faker->word,
        //'summary' => $this->faker->word,
        'duration' => $this->faker->word,
        'free' => $this->faker->word,
        'observation' => $this->faker->word,
        'objective' => $this->faker->word,
        //'needs' => $this->faker->json_decode,
        'facilities' => $this->faker->word,
        'theoretical_phase' => $this->faker->word,
        'practical_phase' => $this->faker->word,
        'cross_cutting_topics' => $this->faker->word,
        'bibliography' => $this->faker->word,
        'teaching_strategies' => $this->faker->word,
        'required_installing_sources' => $this->faker->word,
        'practice_hours' => $this->faker->word,
        'theory_hours' => $this->faker->word,
        'practice_required_resources' => $this->faker->word,
        'aimtheory_required_resources' => $this->faker->word,
        'learning_teaching_strategy' => $this->faker->word,
        'proposed_date' => $this->faker->date,
        'approval_date'=> $this->faker->date,
        //'need_date' => $this->faker->date,
        'local_proposal' => $this->faker->word,
        'project' => $this->faker->word,
        'capacity' => $this->faker->word,
        'setec_name'=> $this->faker->word,
        ];
    }
}
