<?php

namespace Database\Factories\Cecy;

use App\Models\Cecy\Prerequisite;
use Illuminate\Database\Eloquent\Factories\Factory;

class PrerequisiteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Prerequisite::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
