<?php

namespace Database\Factories\Cecy;

use App\Models\Cecy\EvaluationMechanism;
use Illuminate\Database\Eloquent\Factories\Factory;

class EvaluationMechanismFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EvaluationMechanism::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
