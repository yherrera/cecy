<?php

namespace Database\Factories\Cecy;

use App\Models\Cecy\Planification;
use Illuminate\Database\Eloquent\Factories\Factory;

class PlanificationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Planification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
