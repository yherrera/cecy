<?php

namespace Database\Factories\Cecy;

use App\Models\Cecy\DetailRegistration;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetailRegistrationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetailRegistration::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
