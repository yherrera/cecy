
import { State, Catalogue } from '../ignug/models.index';
import { DetailRegistration } from './detailRegistration';
import { Course, Instructor, Planification } from './models.index';

export interface DetailPlanification {
    id?: number;
    start_date?: Date;
    end_date?: Date;
    summary?: string;
    planned_end_date?: Date;
    planification?: Planification;
    detailRegistration?: DetailRegistration;
    course?: Course;
    instructor?: Instructor;
    state?: State;
    capacity?: number;
    observation?: string;
    conference?: Catalogue;
    parallel?: Catalogue
    needs?: JSON;
    need_date?: Date;
   
}

