import { Authorities } from './authorities';
import { Image } from './../ignug/image';
import { State } from '../ignug/state';

export interface Institution {
    id?:number;
    name?:string;
    slogan?:string;
    code?:string;
    ruc?:string;
    state?: State;
    authority_id?:number;
    logo?:Image;
    authority?:Authorities;
}