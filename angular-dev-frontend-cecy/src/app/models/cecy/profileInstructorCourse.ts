import { from } from 'rxjs';
import {State} from '../ignug/models.index';
import { Course } from './models.index';

export interface Instructor {
    id?: number;
    course?: Course;
    state?: State;
    required_knowledge?: String;
    required_experience?: String;
    required_skills?: String;

}

