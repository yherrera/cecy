import { State, Catalogue } from '../ignug/models.index';
import { Registration } from './models.index';

export interface AdditionalInformation {
    id?: number;
    company_name?: string;
    company_address?: string;
    company_phone?: string;
    company_activity?: string;
    company_sponsor?: boolean;
    name_contact?: string;
    know_course?: JSON;
    course_follow?: JSON;
    works?: boolean;
    state?: State;
    registration?: Registration;
    level_instruction?: Catalogue;
}

