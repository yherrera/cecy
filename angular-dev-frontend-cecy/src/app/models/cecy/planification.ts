import {State} from '../ignug/models.index';
import { Course } from './models.index';


export interface Planification {
    id?: number;
    start_date?: Date;
    summary?: string;
    course?: Course;
    state?: State;
    needs?: JSON;
}

