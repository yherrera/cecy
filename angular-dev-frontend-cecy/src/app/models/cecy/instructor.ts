import {State} from '../ignug/models.index';
import { User } from '../auth/user';


export interface Instructor {
    id?: number;
    user?: User;
    state?: State;
}

