import { from } from 'rxjs';
import { Catalogue, State } from '../ignug/models.index';
import { Planification, Participant } from './models.index';

export interface Registration {
    id?: number;
    date_registration?: string;
    participant?: Participant;
    state?: State;
    type?: Catalogue;
    number?: string;
    planification?: Planification;

}

