
import { State, Catalogue } from '../ignug/models.index';
import { Course, Instructor, Planification } from './models.index';
import { Registration } from './registration';

export interface DetailRegistration {
    id?: number;
    registration?: Registration;
    state_id?: State;
    //status?: ;
    //status_certificate?: ;
    final_grade?: string;
    certificate_withdrawn?: string;
    observation?: JSON;
}

