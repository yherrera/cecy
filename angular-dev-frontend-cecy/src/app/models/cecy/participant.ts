import { from } from 'rxjs';
import { User } from '../auth/user';
import { State, Catalogue } from '../ignug/models.index';

export interface Participant {
    id?: number;
    user?: User;
    person_type?: Catalogue;
    state?: State;
    
}

