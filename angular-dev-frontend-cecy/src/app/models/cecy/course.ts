import { Institution, Schedule} from './models.index';
import {State, Catalogue} from '../ignug/models.index';
import { User } from '../auth/models.index';

export interface Course {
    id?: number;
    code: string;
    name: string
    cost: string;
    photo?: string;
    duration?: string;
    free?: string;
    observation?: JSON;
    objective?: JSON;
    facilities?: JSON;
    theoretical_phase?: JSON;
    practical_phase?: JSON;
    main_topics?: JSON;
    secondary_topics?:JSON;
    cross_cutting_topics?:JSON;
    bibliography?: JSON;
    teaching_strategies?: JSON;
    required_installing_sources?:JSON;
    practice_hours?:string;
    theory_hours?: string;
    practice_required_resources?: string;
    aimtheory_required_resources?:string;
    learning_teaching_strategy?:string;
    proposed_date?:string;
    approval_date?:string;
    local_proposal?:string;
    project?:string;
    capacity?:string;
    setec_name?:string;
    modality?: Catalogue;
    state?: State;
    type?: Catalogue;
    area?: Catalogue;
    level?: Catalogue
    user?: User;
    schedules?: Schedule;
    course?: Catalogue;
    specialty?: Catalogue;
    institution?: Institution;
    period: Catalogue;
}