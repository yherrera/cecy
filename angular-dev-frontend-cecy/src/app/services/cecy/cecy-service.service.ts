import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import {User} from '../../models/auth/user';
import { Course } from '../../models/cecy/course';
import { utf8Encode } from '@angular/compiler/src/util';
import { dateToLocalArray } from '@fullcalendar/core/datelib/marker';
import { from, Observable } from "rxjs";



@Injectable({
  providedIn: "root",
})
export class CecyServiceService {
  headers: HttpHeaders;
  user: User;

  constructor(private _http: HttpClient) {
    localStorage.setItem("accessToken", "pruebas");
  }

  get(url: string) {
    this.headers = new HttpHeaders()
      .set("X-Requested-With", "XMLHttpRequest")
      .append("Content-Type", "application/json")
      .append("Accept", "application/json")
      .append(
        "Authorization",
        "Bearer " + localStorage.getItem("accessToken").replace('"', "")
      );
    url = environment.API_URL_CECY + url;
    return this._http.get(url, { headers: this.headers });
  }

  getDetail(url: string, id: string):Observable<Course> {
    this.headers = new HttpHeaders()
      .set("X-Requested-With", "XMLHttpRequest")
      .append("Content-Type", "application/json")
      .append("Accept", "application/json")
      .append(
        "Authorization",
        "Bearer " + localStorage.getItem("accessToken").replace('"', "")
      );
    url = environment.API_URL_CECY + url + id;
    return this._http.get<Course>(url, { headers: this.headers });
  }

  getPdf(url: string, id: any) {
    this.headers = new HttpHeaders()
      .set("X-Requested-With", "XMLHttpRequest")
      .append("Content-Type", "application/json")
      .append("Accept", "application/json")
      .append(
        "Authorization",
        "Bearer " + localStorage.getItem("accessToken").replace('"', "")
      );
    url = environment.API_URL_CECY + url + id;
    return this._http.get(url, { headers: this.headers });
  }

  getIncrption(url: string, id: any, registration_id: any) {
    this.headers = new HttpHeaders()
      .set("X-Requested-With", "XMLHttpRequest")
      .append("Content-Type", "application/json")
      .append("Accept", "application/json")
      .append(
        "Authorization",
        "Bearer " + localStorage.getItem("accessToken").replace('"', "")
      );
    url = environment.API_URL_CECY + url + id + "/" + registration_id;
    return this._http.get(url, { headers: this.headers });
  }

  post(url: string, data: any) {
    this.headers = new HttpHeaders()
      .set("X-Requested-With", "XMLHttpRequest")
      .append("Content-Type", "application/json")
      .append("Accept", "application/json")
      .append(
        "Authorization",
        "Bearer " + localStorage.getItem("accessToken").replace('"', "")
      );
    url = environment.API_URL_CECY + url;
    return this._http.post(url, data, { headers: this.headers });
  }

  update(url: string, data: any) {
    this.headers = new HttpHeaders()
      .set("X-Requested-With", "XMLHttpRequest")
      .append("Content-Type", "application/json")
      .append("Accept", "application/json")
      .append(
        "Authorization",
        "Bearer " + localStorage.getItem("accessToken").replace('"', "")
      );
    url = environment.API_URL_CECY + url;
    return this._http.put(url, data, { headers: this.headers });
  }

  delete(url: string) {
    this.headers = new HttpHeaders()
      .set("X-Requested-With", "XMLHttpRequest")
      .append("Content-Type", "application/json")
      .append("Accept", "application/json")
      .append(
        "Authorization",
        "Bearer " + localStorage.getItem("accessToken").replace('"', "")
      );
    url = environment.API_URL_CECY + url;
    return this._http.delete(url, { headers: this.headers });
  }

  academic_record() {
    this.user = JSON.parse(localStorage.getItem('user')) as User;
      const url = environment.API_URL_CECY + 'academic_record'+ this.user.id;
      this.headers = new HttpHeaders().set('Content-Type', 'application/json').append('X-Requested-With', 'XMLHttpRequest');
      return this._http.get(url);
  }
}
