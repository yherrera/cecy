import {Routes} from '@angular/router';
//import {AuthGuard} from '../../shared/auth-guard/auth.guard';
import {CatalogueComponent} from './catalogue/catalogue.component';
import { UserCatalogueComponent } from './user-catalogue/user-catalogue.component';

export const IgnugRoutes: Routes = [
    {
        path: 'catalogues',
        component: CatalogueComponent,
        //canActivate: [AuthGuard]
    },

    {
        path: 'usercatalogues',
        component: UserCatalogueComponent,
        //canActivate: [AuthGuard]
    }
];
