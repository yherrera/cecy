import {Component, OnInit} from '@angular/core';
import {MessageService} from 'primeng/api';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Catalogue} from '../../../models/ignug/catalogue';
import {IgnugService} from '../../../services/ignug/ignug.service';
import {SelectItem } from "primeng/api";
import icons from '../../../../catalogues/icons.json';
import color from '../../../../catalogues/color.json';



@Component({
    selector: 'app-user-catalogue',
    templateUrl: './user-catalogue.component.html',
    styleUrls: ['./user-catalogue.component.css'],
})

export class UserCatalogueComponent implements OnInit {

    catalogues: Catalogue[];
    iconName: any[];
    colorName: any[];
    cols: any[];
    formCatalogue: FormGroup;
    updateFormCatalogue: FormGroup;
    updateForm: Catalogue[];
    catalogueRestore: Catalogue[];
    itemRestore: Catalogue[];
    totalRecords: Number;
    allCatalogues: SelectItem[];
    listIcons: SelectItem[];
    listColor: SelectItem[];
    selectedItem: SelectItem;

    constructor(
                private _fb: FormBuilder,
                private _ignugService: IgnugService,
                private _messageService: MessageService,   
    ) {

       this.catalogues = [];
       this.updateForm = [];
       this.catalogueRestore = [];
       this.itemRestore = [];
       this.buildFormCatalogueItem();
       this.iconName = icons;
       this.colorName = color;
       this.listIcons = [];
       this.listColor = [];
       this.allCatalogues = [];

    }

    ngOnInit(): void{
        this.getJsonElement();
        this.getCatalogues();
        this.catalogues = [];
        this.cols = [
            //{field: 'id', header: 'ID'},
            {field: 'parent_id', header: 'Referencia padre'},
            {field: 'code', header: 'Código'},
            {field: 'name', header: 'Nombre'},
            {field: 'description', header: 'Descripción'},
            {field: 'type', header: 'Tipo'},
            {field: 'icon', header: 'Icono'},
            {field: 'state_id', header: 'Estado'},
            //{field: 'Acción', header: 'Acción'}     
        ];
        this.totalRecords=this.cols.length;
        this.hideModal();
      
    }

    //función para leer archivo JSON
    getJsonElement() : void{

        this.iconName.forEach(element => {

            this.listIcons.push({ label: element.name, value: element.name });

        });

        this.colorName.forEach(element => {

            this.listColor.push({ label: element.name, value: element.name });

        });


    }
   
    //método get
    getCatalogues(): void {

        this.showModal();
        this._ignugService.get('catalogues').subscribe(
            response => {
                this.hideModal();
                this.catalogues = response['data']['catalogues'];
                this.catalogues.forEach(catalogue => {
                    this.allCatalogues.push({ label: catalogue.name, value: catalogue.name });  
                });
                
            }, error => {
                this.hideModal();
                this._messageService.add({
                    key: 'tst',
                    severity: 'error',
                    summary: 'Oops! Problemas con el servidor',
                    detail: 'Vuelve a intentar más tarde',
                    life: 5000
                });
            });

    }

    //construimos el formulario post
	buildFormCatalogueItem() {
		this.formCatalogue = this._fb.group({
			name: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50), Validators.pattern(/^[A-Za-z ]*$/)]],
			parent_id: ['', [Validators.required]],
			icon: [''],
			color: [''],
			description: [''],
			type: ['', Validators.required]
		});
	}


    //método post ítem catálogo 
	createCatalogueItem(): void {

		var name = this.formCatalogue.value['name'];
		var parent_code = this.formCatalogue.value['parent_id'];
		var icon = this.formCatalogue.value['icon'];
		var color = this.formCatalogue.value['color'];
		var description = this.formCatalogue.value['description'];
		var type = this.formCatalogue.value['type'];

		var checked_element = document.getElementById('nan_reference') as HTMLInputElement;

		if(checked_element.checked) { 

            parent_code = 'parent_null';

        } else {  

            parent_code = parent_code; 
		}  
		

		if (icon === undefined || icon === 'Ninguno') {
			icon = null;
		} else {
			icon = icon;
		}

		if (color === undefined || color === null) {
			color = '#ffffff';
		} else {
			color = color;
		}


		if (description === undefined) {
			description = null;
		} else {
			description = description;
		}

		if (type === undefined || type === null) {
			type = null;
		} else {
			type = type;
		}

		let json_catalogue = {
			'name': name,
			'parent_code': parent_code,
			'icon': icon,
			'color': color,
			'description': description,
			'type': type
		}

        this.showModal();
        
		this._ignugService.post('catalogues', json_catalogue).subscribe(
			response => {
				this._messageService.add({
					key: 'tst',
					severity: 'success',
					summary: 'Se ha creado el catálogo item correctamente',
					detail: name,
					life: 5000
				});
				this.hideModal();
				this.getCatalogues();
				this.formCatalogue.reset();
				this.allCatalogues = [];
				document.getElementById("modal-register").classList.toggle("move");
				
			}, error => {
				this.hideModal();
				this._messageService.add({
					key: 'tst',
					severity: 'error',
					summary: 'Oops! Ha ocurrido un error en el servidor',
					detail: 'Vuelve a intentar más tarde',
					life: 5000
				});
			});

    }
    //Función para marcar nuevo catálogo creado como visto
	viewCatalogue(id: any, state: any) {

		if (state !== 'VISTO') {
			let ids = document.querySelectorAll('.nuevo');
			ids.forEach(element => {
				var idHiden = parseInt(element.id);
				if (id === idHiden) {
					document.getElementById(id).style.display = 'none';
				}
			});

			var json_catalogue = {
				'id': id
			}

			this._ignugService.post('catalogue/updateCreatedCatalogue', json_catalogue).subscribe(
				response => {
					this.getCatalogues();
					this.allCatalogues = [];
				}, error => {
					this.hideModal();
					this._messageService.add({
						key: 'tst',
						severity: 'error',
						summary: 'Oops! Problemas con el servidor',
						detail: 'Vuelve a intentar más tarde',
						life: 5000
					});
				});
		}
	}


	viewAllCatalogue(){

		this.showModal();
		var json_catalogue = {

			'id': 'VISTO'
		}

		this._ignugService.post('catalogue/updateAllCreatedCatalogue', json_catalogue).subscribe(
			response => {

				this.hideModal();
				this._messageService.add({
					key: 'tst',
					severity: 'success',
					summary: 'Se ha marcado todos los catálogos como vistos',
					detail: 'Procesado exitosamente',
					life: 5000
				});

				this.getCatalogues();
				this.allCatalogues = [];
			}, error => {
				this.hideModal();
				this._messageService.add({
					key: 'tst',
					severity: 'error',
					summary: 'Oops! Problemas con el servidor',
					detail: 'Vuelve a intentar más tarde',
					life: 5000
				});
			});

	}

    

    // Modal ocultar para procesar petición
	hideModal() {
		document.getElementById('process-modal-fade').style.display = 'none';
	}

	// Modal hacer visible para procesar petición
	showModal() {
		document.getElementById('process-modal-fade').style.display = 'block';
	}


    //modal crear item catálogos
	openModalItem() {

		document.getElementById("modal-register").classList.toggle("move");
	}

	closeModalItem() {
		document.getElementById("modal-register").classList.toggle("move");
	}

}
