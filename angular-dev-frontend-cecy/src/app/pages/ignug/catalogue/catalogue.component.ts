import {Component, OnInit } from '@angular/core';
import {ConfirmationService, MessageService} from 'primeng/api';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Catalogue} from '../../../models/ignug/catalogue';
import {IgnugService} from '../../../services/ignug/ignug.service';
import {SelectItem} from "primeng/api";
import icons from '../../../../catalogues/icons.json';
import color from '../../../../catalogues/color.json';


@Component({
	selector: 'app-catalogue',
	templateUrl: './catalogue.component.html',
	styleUrls: ['./catalogue.component.scss'],
})

export class CatalogueComponent implements OnInit {

	catalogues: Catalogue[];
	iconName: any[];
	colorName: any[];
	cols: any[];
	formCatalogue: FormGroup;
	formCatalogueParent: FormGroup;
	updateFormCatalogue: FormGroup;
	updateForm: Catalogue[];
	catalogueRestore: Catalogue[];
	itemRestore: Catalogue[];
	totalRecords: Number;
	allCatalogues: SelectItem[];
	listIcons: SelectItem[];
	listColor: SelectItem[];
	selectedItem: SelectItem;
    checkbox_restore: any[];
    checkbox_update: any[];
    
	constructor(
		private _fb: FormBuilder,
		private _fbs: FormBuilder,
		private _confirmationService: ConfirmationService,
		private _ignugService: IgnugService,
		private _messageService: MessageService
	) {

		this.catalogues = [];
		this.updateForm = [];
		this.catalogueRestore = [];
		this.itemRestore = [];
		this.buildFormCatalogueItem();
		this.buildFormCatalogueParent();
		this.buildFormUpdateCatalogue();
		this.iconName = icons;
		this.colorName = color;
		this.listIcons = [];
		this.listColor = [];
		this.allCatalogues = [];
	}

	ngOnInit(): void {
		this.getJsonElement();
		this.validateInputModalForm();
		this.getCatalogues();
		this.catalogues = [];
        this.checkbox_restore = [];
        this.checkbox_update = [];
		this.cols = [{
				field: 'parent_id',
				header: 'Referencia padre'
			},
			{
				field: 'code',
				header: 'Código'
			},
			{
				field: 'name',
				header: 'Nombre'
			},
			{
				field: 'description',
				header: 'Descripción'
			},
			{
				field: 'type',
				header: 'Tipo'
			},
			{
				field: 'icon',
				header: 'Icono'
			},
			{
				field: 'state_id',
				header: 'Estado'
			},
			{
				field: 'Acción',
				header: 'Acción'
			}
		];
		this.totalRecords = this.cols.length;
		this.hideModal();

	}

	//Función para leer archivo JSON para el combox de colores e iconos
	getJsonElement(): void {
		this.iconName.forEach(element => {
			this.listIcons.push({
				label: element.name,
				value: element.name
			});
		});

		this.colorName.forEach(element => {
			this.listColor.push({
				label: element.name,
				value: element.name
			});
		});
	}

	//Método get
	getCatalogues() {
		//this.showModal();
		this._ignugService.get('catalogues').subscribe(
			response => {
				this.hideModal();
				this.catalogues = response['data']['catalogues'];
				this.catalogues.forEach(catalogue => {
					this.allCatalogues.push({
						label: catalogue.name,
						value: catalogue.name
					});
				});
			}, error => {
				this.hideModal();
				this._messageService.add({
					key: 'tst',
					severity: 'error',
					summary: 'Oops! Problemas con el servidor',
					detail: 'Vuelve a intentar más tarde',
					life: 5000
				});
			});

	}


	  //validacion de seleccion de clase padre en modal catálogo ítem
	  validateInputModalForm(){

		document.getElementById('remove').classList.add('visible-restore');
		document.getElementById('reference').addEventListener('click', function(){

			document.getElementById('remove').classList.remove('visible-restore');
		
		});

		document.getElementById('nan_reference').addEventListener('click', function(){

			document.getElementById('remove').classList.add('visible-restore');
			
		});

    }


	//construimos el formulario post item catalogo
	buildFormCatalogueItem() {
		this.formCatalogue = this._fbs.group({
			name: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50), Validators.pattern(/^[A-Za-z ]*$/)]],
			parent_id: ['', [Validators.required]],
			icon: [''],
			color: [''],
			description: [''],
			type: ['', Validators.required]
		});
	}


	//construimos el formulario post catalogo padre
	buildFormCatalogueParent(): void {
		this.formCatalogueParent = this._fb.group({
			name_parent: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50), Validators.pattern(/^[A-Za-z ]*$/)]],
			parent_main_id: [''],
			icon_parent: [''],
			color_parent: [''],
			description_parent: [''],
			type_parent: ['', Validators.required]
		});
	}

//método post ítem catálogo 
createCatalogueItem(): void {

	var name = this.formCatalogue.value['name'];
	var parent_code = this.formCatalogue.value['parent_id'];
	var icon = this.formCatalogue.value['icon'];
	var color = this.formCatalogue.value['color'];
	var description = this.formCatalogue.value['description'];
	var type = this.formCatalogue.value['type'];


	if (parent_code === undefined) {
		parent_code = 'parent_null';
	}

	

	if (icon === undefined || icon === 'Ninguno') {
		icon = null;
	} else {
		icon = icon;
	}

	if (color === undefined || color === null) {
		color = '#ffffff';
	} else {
		color = color;
	}


	if (description === undefined) {
		description = null;
	} else {
		description = description;
	}

	if (type === undefined || type === null) {
		type = null;
	} else {
		type = type;
	}

	let json_catalogue = {
		'name': name,
		'parent_code': parent_code,
		'icon': icon,
		'color': color,
		'description': description,
		'type': type
	}

	this.showModal();
	this._ignugService.post('catalogues', json_catalogue).subscribe(
		response => {
			this._messageService.add({
				key: 'tst',
				severity: 'success',
				summary: 'Se ha creado el catálogo item correctamente',
				detail: name,
				life: 5000
			});
			this.hideModal();
			this.getCatalogues();
			this.formCatalogue.reset();
			this.allCatalogues = [];
			document.getElementById("modal-register").classList.toggle("move");
		}, error => {
			this.hideModal();
			this._messageService.add({
				key: 'tst',
				severity: 'error',
				summary: 'Oops! Ha ocurrido un error en el servidor',
				detail: 'Vuelve a intentar más tarde',
				life: 5000
			});
		});

}

//Método post catálogo padre
createCatalogueParent(): void {

	var name = this.formCatalogueParent.value['name_parent'];
	var parent_code = this.formCatalogueParent.value['parent_main_id'];
	var icon = this.formCatalogueParent.value['icon_parent'];
	var color = this.formCatalogueParent.value['color_parent'];
	var description = this.formCatalogueParent.value['description_parent'];
	var type = this.formCatalogueParent.value['type_parent'];

	if (parent_code === undefined) {
		parent_code = 'null';
	} else {
		parent_code = parent_code;
	}

	if (icon === undefined || icon === 'Ninguno') {
		icon = null;
	} else {
		icon = icon;
	}


	if (color === undefined || color === null) {
		color = '#ffffff';
	} else {
		color = color;
	}


	if (description === undefined) {
		description = null;
	} else {
		description = description;
	}

	if (type === undefined || type === null) {
		type = false;
	} else {
		type = type;
	}

	this.showModal();
	let json_catalogue = {
		'name': name,
		'parent_code': parent_code,
		'icon': icon,
		'color': color,
		'description': description,
		'type': type
	}
	this._ignugService.post('catalogues', json_catalogue).subscribe(
		response => {
			this._messageService.add({
				key: 'tst',
				severity: 'success',
				summary: 'Se ha creado el catálogo padre correctamente',
				detail: name,
				life: 5000
			});
			this.hideModal();
			this.getCatalogues();
			this.formCatalogueParent.reset();
			this.allCatalogues = [];
			document.getElementById("modal-register-main").classList.toggle("move");
		}, error => {
			this.hideModal();
			this._messageService.add({
				key: 'tst',
				severity: 'error',
				summary: 'Oops! Ha ocurrido un error en el servidor',
				detail: 'Vuelve a intentar más tarde',
				life: 5000
			});
		});
}
	//obtenemos registro método update en la ventana modal
	showCatalogue(id: any) {
		document.getElementById('modal-update').classList.toggle('move-update');
		this.showModal();
		this._ignugService.get('catalogues/' + id).subscribe(
			response => {
				this.hideModal();
				this.updateForm = response['data']['catalogue'];
			}, error => {
				this.hideModal();
				this._messageService.add({
					key: 'tst',
					severity: 'error',
					summary: 'Oops! Ha ocurrido un error en el servidor',
					detail: 'Vuelve a intentar más tarde',
					life: 5000
				});
			});
	}

	//creamos el formulario update
	buildFormUpdateCatalogue() {
		this.updateFormCatalogue = this._fb.group({
			name_catalogue: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50), Validators.pattern(/^[A-Za-z ]*$/)]],
			icon_catalogue: [''],
			icon_current: [''],
			color_catalogue: [''],
			color_current: [''],
			description_catalogue: [''],
			id_catalogue: ['']
		});
	}

	//funciones para combo box select icono y color update
	hideIconDefault() {

		document.getElementById('remove_default').classList.add('visible-restore');
		document.getElementById('select_icon').classList.remove('visible-restore');
		
	}

	showIconDefault() {

		document.getElementById('remove_default').classList.remove('visible-restore');
		document.getElementById('select_icon').classList.add('visible-restore');

	}

	hideColorDefault() {

		document.getElementById('color_default').classList.add('visible-restore');
		document.getElementById('select_color').classList.remove('visible-restore');

	}

	showColorDefault() {

		document.getElementById('color_default').classList.remove('visible-restore');
		document.getElementById('select_color').classList.add('visible-restore');

	}

	//método update para catálogos
	updateCatalogue() {
		var name_catalogue = this.updateFormCatalogue.value['name_catalogue'];
		var icon_catalogue = this.updateFormCatalogue.value['icon_catalogue'];
		var icon_current = this.updateFormCatalogue.value['icon_current'];
		var id_catalogue = this.updateFormCatalogue.value['id_catalogue'];
		var color_catalogue = this.updateFormCatalogue.value['color_catalogue'];
		var color_current = this.updateFormCatalogue.value['color_current'];
		var description_catalogue = this.updateFormCatalogue.value['description_catalogue'];

		
		if (icon_current === undefined || icon_current === "Ninguno") {

			icon_current = icon_catalogue;

		}  else {

			icon_current = icon_current;
		}


		if (color_current === undefined) {

			color_current = color_catalogue;

		} else {

			color_current = color_current;
		}


		if (description_catalogue === undefined) {

			description_catalogue = null;

		} else {

			description_catalogue = description_catalogue;
		}

		this.showModal();

		let json_catalogue = {
			'id': id_catalogue,
			'name': name_catalogue,
			'icon': icon_current,
			'color': color_current,
			'description': description_catalogue
		}

		this._ignugService.update('catalogue/update', json_catalogue ).subscribe(
			response => {
				this._messageService.add({
					key: 'tst',
					severity: 'success',
					summary: 'Se ha actualizado la información correctamente',
					detail: name_catalogue,
					life: 5000
				});

				this.hideModal();
				this.getCatalogues();
				document.getElementById("modal-update").classList.remove("move-update");
			}, error => {
				this.hideModal();
				this._messageService.add({
					key: 'tst',
					severity: 'error',
					summary: 'Oops! Ha ocurrido un error en el servidor',
					detail: 'Vuelve a intentar más tarde',
					life: 5000
				});

			});

	}

	//Capturamos todos los catálogos para deshabilitarlos en la vista principal
	updateStateGroupSelect(id: any):void{

		var selected = document.getElementById(id) as HTMLInputElement;

		if (!selected.checked) {

			this.checkbox_update.push(id);
		
		} else {
			
			var index = this.checkbox_update.indexOf(id);

			if (index > -1) {
			
				this.checkbox_update.splice(index, 1);
				
			}

		}

		if(this.checkbox_update.length > 0){

			document.getElementById("modal-checkbox").classList.add("move-checkbox");		
			
		}
		else{

			document.getElementById("modal-checkbox").classList.remove("move-checkbox");

		}

	}

	//Recolectamos los catálogs obtenidos en la función updateStateGroupSelect para deshabilitarlos
	updateStateModal():void {

		var string_convert:any;
		

		if(this.checkbox_update.length > 0){

			string_convert = this.checkbox_update.toString();

		}else{

			string_convert = 'VACIO';

		}
		
		this._confirmationService.confirm({
			header: 'Desabilitar los catálogos seleccionados ',
			message: 'Seguro que deseas desabilitar este catálogo?',
			acceptButtonStyleClass: 'ui-button-danger',
			rejectButtonStyleClass: 'ui-button-primary',
			acceptLabel: 'Si',
			rejectLabel: 'No',
			icon: 'pi pi-trash',
			accept: () => {
				
				this.showModal();
				var json_catalogue = {
					'id': string_convert,
				}
				this._ignugService.post('catalogue/updateIdCatalogue', json_catalogue).subscribe(
					response => {
						this.getCatalogues();
						this.hideModal();
						this.allCatalogues = [];
						this.checkbox_update = [];
						document.getElementById("modal-checkbox").classList.remove("move-checkbox");
						this._messageService.add({
							key: 'tst',
							severity: 'success',
							summary: 'Se ha desabilitado correctamente los catálogos seleccionados',
							detail: 'Proceso satisfactorio',
							life: 5000
						});

					}, error => {
						this.hideModal();
						this._messageService.add({
							key: 'tst',
							severity: 'error',
							summary: 'Oops! Ha ocurrido un error en el servidor',
							detail: 'Vuelve a intentar más tarde',
							life: 5000
						});
					});
			},

			reject: () => {
				//Si cancela la operación se vuelve a marcar los checkbox seleccionados
				this.checkbox_update.forEach(element => {
					var att = document.getElementById(element) as HTMLInputElement;
					att.checked = true;
					this.checkbox_update = [];
					string_convert = '';
				});

			}

		});

	}

	//Eiminado lógico de catálogos
	logicDeleteState(id: any, rowName: any) {

		this._confirmationService.confirm({
			header: 'Eliminar ' + rowName,
			message: 'Seguro que deseas eliminar este catálogo?',
			acceptButtonStyleClass: 'ui-button-danger',
			rejectButtonStyleClass: 'ui-button-primary',
			acceptLabel: 'Si',
			rejectLabel: 'No',
			icon: 'pi pi-trash',
			accept: () => {

				this.showModal();
				var json_catalogue = {
					'id': id,
				}
				this._ignugService.post('catalogue/destroy', json_catalogue).subscribe(
					response => {
						this.hideModal();
						this._messageService.add({
							key: 'tst',
							severity: 'success',
							summary: 'Se ha eliminado correctamente',
							detail: rowName,
							life: 5000
						});
						this.getCatalogues();
						this.allCatalogues = [];
						
					}, error => {
						this.hideModal();
						this._messageService.add({
							key: 'tst',
							severity: 'error',
							summary: 'Oops! Ha ocurrido un error en el servidor',
							detail: 'Vuelve a intentar más tarde',
							life: 5000
						});

					});
			}

		});

	}

	//método get para mostrar en la ventana modal los catálogos desabilitados
	getIndexCatalogue() {
		this.showModal();
		this._ignugService.get('catalogue/catalogue').subscribe(
			response => {
				this.hideModal();
				var arrayItem = [];
				var arrayParentPush = [];
				var arrayItemPush = [];
				arrayItem = response['data']['catalogues'];
				arrayItem.forEach(element => {

					if (element.parent_id === null) {

						arrayParentPush.push(element);
						this.catalogueRestore = arrayParentPush;

					} else {

						arrayItemPush.push(element);
						this.itemRestore = arrayItemPush;

					}

				});

			}, error => {
				this.hideModal();
				this._messageService.add({
					key: 'tst',
					severity: 'error',
					summary: 'Oops! Problemas con el servidor',
					detail: 'Vuelve a intentar más tarde',
					life: 5000
				});
			});
	}

	//Recolectamos todos los catálogos a habilitar  y mostrarlos en la vista pricipal
	updateStateCatalogue(id: any, parent_id: any) {

		var id_selected = document.getElementById(id) as HTMLInputElement;

		if (id_selected.checked) {
			this.checkbox_restore.push(id);
		} else {

			var index = this.checkbox_restore.indexOf(id);

			if (index > -1) {

				this.checkbox_restore.splice(index, 1);
			}

		}

		if (this.checkbox_restore.length > 0) {

			document.querySelector('.menu-catalogue-restore').classList.remove('visible-restore');
			document.querySelector('.menu-catalogue-restore-parent').classList.remove('visible-restore');


		} else {

			document.querySelector('.menu-catalogue-restore').classList.add('visible-restore');
			document.querySelector('.menu-catalogue-restore-parent').classList.add('visible-restore');
		}

	}

	// Enviamos los elementos seleccionados para restaurar el estado del catálogo en la base de datos
	restoreCatalogues(): void {

		var string_convert = this.checkbox_restore.toString();
		this.showModal();
		var json_catalogue = {

			'id': string_convert,
		}

		this._ignugService.post('catalogue/updateCatalogue', json_catalogue).subscribe(
			response => {
				this.hideModal();
				this._messageService.add({
					key: 'tst',
					severity: 'success',
					summary: 'Se ha habilitado los catálogos seleccionados correctamente',
					detail: 'Procesado exitosamente',
					life: 5000
				});
				this.getCatalogues();
				this.getIndexCatalogue();
				this.catalogueRestore = [];
				this.itemRestore = [];
				document.getElementById("modal-restore").classList.toggle("move-restore");
				document.querySelector('.menu-catalogue-restore').classList.add('visible-restore');
				document.querySelector('.menu-catalogue-restore-parent').classList.add('visible-restore');
				this.checkbox_restore = [];

			}, error => {
				this.hideModal();
				this._messageService.add({
					key: 'tst',
					severity: 'error',
					summary: 'Oops! Problemas con el servidor',
					detail: 'Vuelve a intentar más tarde',
					life: 5000
				});
			});


	}


	//Función para marcar nuevo catálogo creado como visto
	viewCatalogue(id: any, state: any) {

		if (state !== 'VISTO') {
			let ids = document.querySelectorAll('.nuevo');
			ids.forEach(element => {
				var idHiden = parseInt(element.id);
				if (id === idHiden) {
					document.getElementById(id).style.display = 'none';
				}
			});

			var json_catalogue = {
				'id': id
			}

			this._ignugService.post('catalogue/updateCreatedCatalogue', json_catalogue).subscribe(
				response => {
					this.getCatalogues();
					this.allCatalogues = [];
				}, error => {
					this.hideModal();
					this._messageService.add({
						key: 'tst',
						severity: 'error',
						summary: 'Oops! Problemas con el servidor',
						detail: 'Vuelve a intentar más tarde',
						life: 5000
					});
				});
		}
	}


	viewAllCatalogue(){

		this.showModal();
		var json_catalogue = {

			'id': 'VISTO'
		}

		this._ignugService.post('catalogue/updateAllCreatedCatalogue', json_catalogue).subscribe(
			response => {

				this.hideModal();
				this._messageService.add({
					key: 'tst',
					severity: 'success',
					summary: 'Se ha marcado todos los catálogos como vistos',
					detail: 'Procesado exitosamente',
					life: 5000
				});

				this.getCatalogues();
				this.allCatalogues = [];
			}, error => {
				this.hideModal();
				this._messageService.add({
					key: 'tst',
					severity: 'error',
					summary: 'Oops! Problemas con el servidor',
					detail: 'Vuelve a intentar más tarde',
					life: 5000
				});
			});

	}

	// Modal ocultar para procesar petición
	hideModal() {
		document.getElementById('process-modal-fade').style.display = 'none';
	}

	// Modal hacer visible para procesar petición
	showModal() {
		document.getElementById('process-modal-fade').style.display = 'block';
	}


	//modal crear item catálogos
	openModalItem() {

		document.getElementById("modal-register").classList.toggle("move");
	}

	closeModalItem() {
		document.getElementById("modal-register").classList.toggle("move");
	}


	// Modal catálogos padre
	openModalMain() {

		document.getElementById("modal-register-main").classList.toggle("move");
	}

	closeModalMain() {
		document.getElementById("modal-register-main").classList.toggle("move");
	}


	//modal actualizar catálogos
	closeModalUpdateCatalogue() {

		document.getElementById("modal-update").classList.toggle("move-update");
	}

	//modal restaurar catálogos
	openRestore() {
		this.getIndexCatalogue();
		document.getElementById("modal-restore").classList.toggle("move-restore");
	}

	closeModalRestore() {
		document.getElementById("modal-restore").classList.toggle("move-restore");
	}


	//Modal para desabilitar catálogos
	closeModalCheckbox() {
		document.getElementById("modal-checkbox").classList.toggle("move-checkbox");
	}

	

	//clasificación de catálogos desabilitados por padre e item
	seeCatalogue() {

		document.getElementById("item-container").classList.add("visible-restore");
		document.getElementById("parent-container").classList.add("visible-block");

	}

	seeItem() {

		var parent_container = document.getElementById("parent-container");
		document.getElementById("item-container").classList.remove("visible-restore");
		parent_container.classList.remove("visible-block");
		parent_container.classList.add("visible-restore");

	}

}
