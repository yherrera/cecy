import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReportsRoutes } from "./reports.routing";
import { FormsModule } from "@angular/forms";
import { AutoCompleteModule } from "primeng/autocomplete";
import { MultiSelectModule } from "primeng/multiselect";
import { CalendarModule } from "primeng/calendar";
import { ChipsModule } from "primeng/chips";
import { CardModule  } from 'primeng/card';
import { CheckboxModule } from "primeng/checkbox";
import { RadioButtonModule } from "primeng/radiobutton";
import { InputMaskModule } from "primeng/inputmask";
import { InputSwitchModule } from "primeng/inputswitch";
import { InputTextModule } from "primeng/inputtext";
import { InputTextareaModule } from "primeng/inputtextarea";
import { DropdownModule } from "primeng/dropdown";
import { DialogModule } from 'primeng/dialog';
import { DataViewModule } from 'primeng/dataview';
import { SpinnerModule } from "primeng/spinner";
import { SliderModule } from "primeng/slider";
import { LightboxModule } from "primeng/lightbox";
import { ListboxModule } from "primeng/listbox";
import { RatingModule } from "primeng/rating";
import { ColorPickerModule } from "primeng/colorpicker";
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { EditorModule } from "primeng/editor";
import { ToggleButtonModule } from "primeng/togglebutton";
import { SelectButtonModule } from "primeng/selectbutton";
import { SplitButtonModule } from "primeng/splitbutton";
import { PasswordModule } from "primeng/password";
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ReportsListComponent } from './reports-list/reports-list.component';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ConfirmationService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';

//component
import { FormD1Component } from './form-d1/form-d1.component';
import { FormF2Component } from './form-f2/form-f2.component';
import { FormA2Component } from './form-a2/form-a2.component';
import { FormB1Component } from './form-b1/form-b1.component';
import { FormE1Component } from './form-e1/form-e1.component';
import { PhotographicRegisterComponent } from './photographic-register/photographic-register.component';
import { DetailFormA2Component } from './detail-form-a2/detail-form-a2.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ReportsRoutes),
    FormsModule,
    AutoCompleteModule,
    MultiSelectModule,
    CalendarModule,
    ChipsModule,
    CardModule,
    CheckboxModule,
    RadioButtonModule,
    InputMaskModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    DropdownModule,
    DialogModule,
    DataViewModule,
    SpinnerModule,
    SliderModule,
    LightboxModule,
    ListboxModule,
    RatingModule,
    ColorPickerModule,
    ConfirmDialogModule,
    EditorModule,
    ToggleButtonModule,
    SelectButtonModule,
    SplitButtonModule,
    PasswordModule,
    TableModule,
    OverlayPanelModule,
    ToastModule,
    MessagesModule,
    MessageModule,
    TabViewModule,
  ],
  providers: [ConfirmationService],
  declarations: [ReportsListComponent, FormD1Component, FormF2Component, FormA2Component, FormB1Component, FormE1Component, PhotographicRegisterComponent, DetailFormA2Component],
})
export class ReportsModule { }
