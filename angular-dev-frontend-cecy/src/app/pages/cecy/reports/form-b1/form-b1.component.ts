import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from "../../../../shared/breadcrumb.service";
import { Router } from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { CecyServiceService } from "../../../../services/cecy/cecy-service.service";
import { Course, Schedule } from 'src/app/models/cecy/models.index';


@Component({
  selector: 'app-form-b1',
  templateUrl: './form-b1.component.html',
})
//FormB1Component equivaqlente -->INFORME DE NECESIDAD DEL CURSO
export class FormB1Component implements OnInit {
  courses : Course[];
  coursePdf: Course[];
  selectedListRow: Course[];
  selectCourseId: Course;
  exportColumns: any[];
  schedule:Schedule;
  loading: boolean = true;


  //popup
  displayMaximizable: boolean;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private cecyService: CecyServiceService,
    ) {
    this.breadcrumbService.setItems([
      { label: "CEC-Y" },
      { label: "Reportes", routerLink: ["/cecy/report/form_b1"] },
    ]);
  }

  ngOnInit(): void {
    this.obtenerCourse();
  }

  obtenerCourse() {
    this.cecyService
      .get("courses")
      //.get("courses/filter?for_free=true")
      .subscribe((response: any) => {
        this.courses = response.data;
        this.loading = false;
      });
  }

  onSelect(courseId: Course): void {
    this.selectCourseId = courseId;
    this.schedule = courseId.schedules;
  }

  pdf(courseId: Course) {
    let id = courseId
    console.log('id', id);
    this.cecyService
      .getPdf("courses/curricular_design_export.pdf/", id)
      .subscribe((response: any) => {
        this.coursePdf = response.data;
      });

      window.open("http://127.0.0.1:8000/api/cecy/courses/curricular_design_export.pdf/" +id);
  }

  //pop up
  showMaximizableDialog() {
    this.displayMaximizable = true;
  }

}
