import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from "../../../../shared/breadcrumb.service";
import { Router } from "@angular/router";
import { CecyServiceService } from "../../../../services/cecy/cecy-service.service";
import { DetailPlanification, Schedule} from 'src/app/models/cecy/models.index';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-form-a2',
  templateUrl: './form-a2.component.html',
  providers: [ConfirmationService, MessageService]
})
// FormA2Component equivalente a --> INSCRIPCIÓN PARA CURSOS DE CAPACITACIÓN
export class FormA2Component implements OnInit {
  selectedValues: string[] = [];
  courses: DetailPlanification[];
  cols: any[];
  selectCourseId: DetailPlanification;
  exportColumns: any[];
  schedule: Schedule;

  loading: boolean = true;

  //popup
  displayMaximizable: boolean;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private cecyService: CecyServiceService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) {
    this.breadcrumbService.setItems([
      { label: "CEC-Y" },
      { label: "Reportes", routerLink: ["/cecy/report/form_a2"] },
    ]);
  }

  ngOnInit(): void {
    this.obtenerCourse();
  }

  obtenerCourse() {
    this.cecyService
      .get("detail_planifications")
      .subscribe((response: any) => {
        this.courses = response.data;
        this.loading = false;
      });
      
  }

  detailInscription(detailRegistrationId: DetailPlanification){
    let id = detailRegistrationId.id;
    this.router.navigate(['cecy/reports/form_a2/detail/' + id]);
  }

  /* showMaximizableDialog() {
    this.displayMaximizable = true;
  } */
}
