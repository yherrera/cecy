import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from "../../../../shared/breadcrumb.service";
import { Router } from "@angular/router";


@Component({
  selector: 'app-reports-list',
  templateUrl: './reports-list.component.html',
})
export class ReportsListComponent implements OnInit {


  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
  ) { 
    this.breadcrumbService.setItems([
      { label: "CEC-Y" },
      { label: "Reportes", routerLink: ["/cecy/report/list"]  },
    ]);
  }

  ngOnInit(){
  }
}
