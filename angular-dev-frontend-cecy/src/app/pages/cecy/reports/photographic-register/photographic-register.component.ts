import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from "../../../../shared/breadcrumb.service";
import { Router } from "@angular/router";
import { CecyServiceService } from "../../../../services/cecy/cecy-service.service";
import { Course } from 'src/app/models/cecy/models.index';
@Component({
  selector: 'app-photographic-register',
  templateUrl: './photographic-register.component.html',
})
export class PhotographicRegisterComponent implements OnInit {

  selectedValues: string[] = [];
  coursesPhoto : Course[];
  photoPdf: Course[];
  cols: any[];
  selectCourseId: Course;
  exportColumns: any[];
  
  //popup
  displayMaximizable: boolean;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private cecyService: CecyServiceService
  ) { 
    this.breadcrumbService.setItems([
      { label: "CEC-Y" },
      { label: "Reportes", routerLink: ["/cecy/report/photographic_register"]  },
    ]);
  }

  ngOnInit(): void {
    this.obtenerCourseNeeds();
    this.cols = [
      { field: 'Sector', header: 'Sector' },
      { field: 'Área', header: 'Área' },
      { field: 'Nombre del curso', header: 'Nombre del curso' },
      { field: '¿Curso OCC? SI/NO', header: '¿Curso OCC? SI/NO' },
      { field: 'Duración', header: 'Duración' },
      { field: 'Fecha inicio', header: 'Fecha Inicio' },
      { field: 'Fecha Fin', header: 'Fecha Fin' },
      { field: 'Desde', header: 'Desde' },
      { field: 'Hasta', header: 'Hasta' },
      { field: 'Lugar del curso dictado', header: 'Lugar Del Curso Dictado' },
      { field: 'N° de paticipantes', header: 'N° de paticipantes' },
      { field: 'Docente', header: 'Doceente' },
      { field: 'Responsable', header: 'Responsable' },
    ];

    this.exportColumns = this.cols.map(col => ({ title: col.header, dataKey: col.field }));

  }

  obtenerCourseNeeds() {
    this.cecyService
      .get("courses")
      .subscribe((response: any) => {
        this.coursesPhoto = response.data;
        console.log('photo', this.coursesPhoto);
      });
  }

  onSelect(courseId: Course) {
    this.selectCourseId = courseId;
  }

  //strem pdf
  pdf(CourseId: Course) {
    let id = CourseId;
    this.cecyService
      .getPdf("courses/photographic_register_export.pdf/", id)
      .subscribe((response: any) => {
        this.photoPdf = response.data;
      });

      window.open("http://127.0.0.1:8000/api/cecy/courses/photographic_register_export.pdf/" +id);
  }

  showMaximizableDialog() {
    this.displayMaximizable = true;
  }

}
