import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from "../../../../shared/breadcrumb.service";
import { Router } from "@angular/router";
import { CecyServiceService } from "../../../../services/cecy/cecy-service.service";
import { DetailPlanification, Schedule } from 'src/app/models/cecy/models.index';

@Component({
  selector: 'app-form-e1',
  templateUrl: './form-e1.component.html',
  })
  // FormE1Component equivalente a --> REGISTRO DE PARTICIPANTES INSCRITOS Y MATRICULADOS
export class FormE1Component implements OnInit {

  detailPlanifications: DetailPlanification[];
  detailRegistration: DetailPlanification[];
  selectDetailPlanificationId: DetailPlanification;
  enrolledPdf:DetailPlanification[];
  schedule: Schedule;

  loading: boolean = true;

  //popup
  displayMaximizable: boolean;

  constructor(private breadcrumbService: BreadcrumbService,
    private router: Router,
    private cecyService: CecyServiceService
  ) { 
    this.breadcrumbService.setItems([
      { label: "CEC-Y" },
      { label: "Reportes", routerLink: ["/cecy/report/form_e1"]  },
    ]);
  }

  ngOnInit(): void {
    this.obtenerDetailPlanification();
  }

  obtenerDetailPlanification() {
    this.cecyService
      .get("detail_planifications")
      .subscribe((response: any) => {
        this.detailPlanifications = response.data;
        this.loading = false;
      });
  }

  onSelect(DetailPlanificationId: DetailPlanification): void {
    this.selectDetailPlanificationId = DetailPlanificationId;
    this.schedule = DetailPlanificationId.course.schedules;
    console.log('onSelect', this.selectDetailPlanificationId);
  }

  obtenerDetailRegistration(detailRegistrationId: any) {
    this.detailRegistration = [];
    let id = detailRegistrationId;
    this.cecyService
      .getDetail("detail_planifications/inscription/", id)
      .subscribe((response: any) => {
        this.detailRegistration = response.data;
        this.loading = false; 
        console.log('array filtardo', this.detailRegistration);
      });
  }

  pdf(planificationId: DetailPlanification) {
    let id = planificationId
    console.log('id', id);
    this.cecyService
      .getPdf("detail_planifications/enrolled_export.pdf/", id)
      .subscribe((response: any) => {
        this.enrolledPdf = response.data;
      });

      window.open("http://127.0.0.1:8000/api/cecy/detail_planifications/enrolled_export.pdf/" +id);
  }

  showMaximizableDialog() {
    this.displayMaximizable = true;
  }

}
