import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from "../../../../shared/breadcrumb.service";
import { Router } from "@angular/router";
import { CecyServiceService } from "../../../../services/cecy/cecy-service.service";
import { DetailPlanification, Schedule } from 'src/app/models/cecy/models.index';


@Component({
  selector: 'app-form-f2',
  templateUrl: './form-f2.component.html',
})

// FormF2Component equivalente a --> INFORME DE NECESIDAD DEL CURSO
export class FormF2Component implements OnInit {

  coursesNeeds : DetailPlanification[];
  courseNeedPdf : DetailPlanification[];
  cols: any[];
  selectCourseId: DetailPlanification;
  exportColumns: any[];
  schedule:Schedule;
  loading : boolean = true;

  //popup
  displayMaximizable: boolean;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private cecyService: CecyServiceService,
  ) { 
    this.breadcrumbService.setItems([
      { label: "CEC-Y" },
      { label: "Reportes", routerLink: ["/cecy/report/form_f2"]  },
    ]);
  }

  

  ngOnInit(): void {
    this.obtenerCourseNeeds();

    this.cols = [
      { field: 'Sector', header: 'Sector' },
      { field: 'Área', header: 'Área' },
      { field: 'Nombre del curso', header: 'Nombre del curso' },
      { field: '¿Curso OCC? SI/NO', header: '¿Curso OCC? SI/NO' },
      { field: 'Duración', header: 'Duración' },
      { field: 'Fecha inicio', header: 'Fecha Inicio' },
      { field: 'Fecha Fin', header: 'Fecha Fin' },
      { field: 'Desde', header: 'Desde' },
      { field: 'Hasta', header: 'Hasta' },
      { field: 'Lugar del curso dictado', header: 'Lugar Del Curso Dictado' },
      { field: 'N° de paticipantes', header: 'N° de paticipantes' },
      { field: 'Docente', header: 'Doceente' },
      { field: 'Responsable', header: 'Responsable' },
    ];

    this.exportColumns = this.cols.map(col => ({ title: col.header, dataKey: col.field }));
  }

  obtenerCourseNeeds() {
    this.cecyService
      .get("detail_planifications")
      //.get("courses/filter?for_free=true")
      .subscribe((response: any) => {
        this.coursesNeeds = response.data;
        this.loading = false;
      });
  }

  onSelect(courseId: DetailPlanification) {

    this.selectCourseId = courseId;
    this.schedule = courseId.course.schedules;
    console.log('detailId', this.selectCourseId);
    console.log('detailSchedule', this.schedule);
  }

  pdf(detailPlanificationId: DetailPlanification) {

    let id = detailPlanificationId;
    console.log('id', id);
    this.cecyService
      .getPdf("courses/photographic_register_export.pdf/", id)
      .subscribe((response: any) => {
        this.courseNeedPdf = response.data;
      });

      window.open("http://127.0.0.1:8000/api/cecy/detail_planifications/needs_export.pdf/" +id);
  }

  showMaximizableDialog() {
    this.displayMaximizable = true;
  }
}
