import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from "../../../../shared/breadcrumb.service";
import { Router } from "@angular/router";
import { MessageService } from 'primeng/api';
import { CecyServiceService } from "../../../../services/cecy/cecy-service.service";
import { DetailPlanification, Schedule } from 'src/app/models/cecy/models.index';


@Component({
  selector: 'app-form-d1',
  templateUrl: './form-d1.component.html',
  providers: [MessageService]
})

//FormD1Component equivalente a --> PROGRAMACIÓN DE CURSOS DE CAPACITACIÓN MENSUAL
export class FormD1Component implements OnInit {

  detailPlanification: DetailPlanification[];
  cols: any[];
  selectDetailId: DetailPlanification;
  detailPlanificationPdf: DetailPlanification[];
  exportColumns: any[];
  schedule: Schedule;
  loading: boolean = true;
  years = [];

  title = 'PROGRAMACIÓN DE CURSOS DE CAPACITACIÓN MENSUAL';
  
  //popup
  displayMaximizable: boolean;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private cecyService: CecyServiceService,
    private messageService: MessageService
  ) {
    this.breadcrumbService.setItems([
      { label: "CEC-Y" },
      { label: "Reportes", routerLink: ["/cecy/report/form_d1"] },
    ]);
  }

  ngOnInit() {

    this.obtenerDetailPlanification();
    this.getYear();

    this.cols = [
      { field: 'Sector', header: 'Sector' },
      { field: 'Área', header: 'Área' },
      { field: 'Nombre del curso', header: 'Nombre del curso' },
      { field: '¿Curso OCC? SI/NO', header: '¿Curso OCC? SI/NO' },
      { field: 'Duración', header: 'Duración' },
      { field: 'Fecha inicio', header: 'Fecha Inicio' },
      { field: 'Fecha Fin', header: 'Fecha Fin' },
      { field: 'Desde', header: 'Desde' },
      { field: 'Hasta', header: 'Hasta' },
      { field: 'Lugar del curso dictado', header: 'Lugar Del Curso Dictado' },
      { field: 'N° de paticipantes', header: 'N° de paticipantes' },
      { field: 'Docente', header: 'Docente' },
      { field: 'Responsable', header: 'Responsable' },
    ];
  }

  obtenerDetailPlanification() {
    this.cecyService
      .get("detail_planifications")
      .subscribe((response: any) => {
        this.detailPlanification = response.data;
        this.loading = false;
      });
  }

  //select one planification
  onSelect(detailId: DetailPlanification): void {
    this.selectDetailId = detailId;
    this.schedule = detailId.course.schedules;
  }
  //year
  getYear(){
    let year = new Date().getFullYear();
    this.years.push(year);
  }

  //strem pdf
  pdf(detailPlanificationId: DetailPlanification) {
    let id = detailPlanificationId;
    console.log('id', id);
    this.cecyService
      .getPdf("detail_planification/monthly_planning_export.pdf/", id)
      .subscribe((response: any) => {
        this.detailPlanificationPdf = response.data;
      });

      window.open("http://127.0.0.1:8000/api/cecy/detail_planifications/monthly_planning_export.pdf/" +id);
  } 

  //pop-up
  showMaximizableDialog() {
    this.displayMaximizable = true;
  }

  clear() {
    this.messageService.clear();
  }

}
