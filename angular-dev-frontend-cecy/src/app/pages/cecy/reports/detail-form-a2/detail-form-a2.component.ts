import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from "../../../../shared/breadcrumb.service";
import { Router, ActivatedRoute } from "@angular/router";
import { CecyServiceService } from "../../../../services/cecy/cecy-service.service";
import { DetailPlanification } from 'src/app/models/cecy/DetailPlanification';
import { Schedule } from 'src/app/models/cecy/schedule';
import { ConfirmationService, MessageService } from 'primeng/api';
import { from } from 'rxjs';

@Component({
  selector: 'app-detail-form-a2',
  templateUrl: './detail-form-a2.component.html',
  providers: [ConfirmationService, MessageService]
  
})
// DetailFormA2Component equivalente a --> INSCRIPCIÓN PARA CURSOS DE CAPACITACIÓN
export class DetailFormA2Component implements OnInit {
  detailRegistration: DetailPlanification[];
  cols: any[];
  selectDetailRegistrationId: DetailPlanification;
  loading: boolean = true;
  enrrolledPdf: DetailPlanification[];
  //popup
  displayMaximizable: boolean;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private activerouter: ActivatedRoute,
    private cecyService: CecyServiceService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ){
    this.breadcrumbService.setItems([
      { label: "CEC-Y" },
      { label: "Reportes", routerLink: ["/cecy/report/form_a2/detail/:id"] },
    ]);
  }

  ngOnInit(): void {
    this.obtenerDetailRegistration();
  }

  obtenerDetailRegistration() {
    let detailRegistrationId = this.activerouter.snapshot.paramMap.get('id');
    //let registrationId = this.activerouter.snapshot.paramMap.get('registration:id');
    this.cecyService
      .getDetail("detail_planifications/inscription/", detailRegistrationId)
      .subscribe((response: any) => {
        this.detailRegistration = response.data;
        this.loading = false; 
        console.log('array', this.detailRegistration);
      });
  }

  onSelect(detailId: DetailPlanification): void {
    this.selectDetailRegistrationId = detailId;
    //this.schedule = detailId.schedules;
    console.log('detailId', this.selectDetailRegistrationId);
    //console.log('detailSchedule', this.schedule);
  }

  pdf(inscriptionId: DetailPlanification) {
    let id = inscriptionId
    console.log('id', id);
    this.cecyService
      .getPdf("detail_planifications/inscription_export.pdf/", id)
      .subscribe((response: any) => {
        this.enrrolledPdf = response.data;
      });

      window.open("http://127.0.0.1:8000/api/cecy/detail_planifications/inscription_export.pdf/" +id);
  }


  showMaximizableDialog() {
    this.displayMaximizable = true;
  }

}
