import { Routes } from "@angular/router";
import { ReportsListComponent } from "./reports-list/reports-list.component";
import { FormD1Component } from './form-d1/form-d1.component';
import { FormF2Component } from './form-f2/form-f2.component';
import { FormA2Component } from './form-a2/form-a2.component';
import { FormB1Component } from './form-b1/form-b1.component';
import { FormE1Component } from './form-e1/form-e1.component';
import { PhotographicRegisterComponent } from './photographic-register/photographic-register.component';
import { DetailFormA2Component } from './detail-form-a2/detail-form-a2.component';
import { from } from 'rxjs';
export const ReportsRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "list",
        component: ReportsListComponent,
      },
      {
        path: "form_d1",
        component: FormD1Component,
        //PROGRAMACIÓN DE CURSOS DE CAPACITACIÓN MENSUAL
      },
      {
        path: "form_f2",
        component: FormF2Component,
        //INFORME DE NECESIDAD DEL CURSO
      },
      {
        path: "form_b1",
        component: FormB1Component,
        //INFORME DE NECESIDAD DEL CURSO
      },
      {
        path: "form_a2",
        component: FormA2Component,
        // INSCRIPCIÓN PARA CURSOS DE CAPACITACIÓN
      },
      {
        path: "form_a2/detail/:id",
        component: DetailFormA2Component,
        // INSCRIPCIÓN PARA CURSOS DE CAPACITACIÓN LIST
      },
      {
        path: "form_e1",
        component: FormE1Component,
        //REGISTRO DE PARTICIPANTES INSCRITOS Y MATRICULADOS
      }, 
      {
        path: "photographic_register",
        component: PhotographicRegisterComponent,
        //REGISTRO DE PARTICIPANTES INSCRITOS Y MATRICULADOS
      },
    ],
  },
];
