import { Authorities } from './../../../../models/cecy/authorities';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { CecyServiceService } from 'src/app/services/cecy/cecy-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Condition, Col, Paginator} from '../../../../models/setting/models.index' ;
import { Catalogue } from 'src/app/models/ignug/catalogue';
import {ConfirmationService, MessageService, SelectItem} from 'primeng/api';

@Component({
  selector: 'app-authorities',
  templateUrl: './authorities.component.html',
  styleUrls: ['./authorities.component.css'],
  providers: [MessageService, ConfirmationService]

})
export class AuthoritiesComponent implements OnInit {
  selectedCol: Col;
  cols: Col[];
  conditions: Condition[];
  rowsPerPageOptions: number[];
  paginator: Paginator;
  selectedAuthority: Authorities;
  dialog: boolean;
  form: FormGroup;
  positions :Catalogue[];
  authorities: any = [] ;
  user1:any=[];
  displayFormAuthorities: boolean;
  formAuthorities: FormGroup;
  position: any = [];
  estatus: any = [];

  constructor(private _spinnerService: NgxSpinnerService,
    private _cecyService: CecyServiceService,
    private _messageService: MessageService,
    private _fb: FormBuilder,
    //private _confirmationService: ConfirmationService,
) {
  this.paginator = {current_page: 1, per_page: 5};
    this.rowsPerPageOptions = [5, 10, 20, 30, 50];
 }

  ngOnInit(): void {
   
  this.cols = [
    { field: 'identification', header: 'Identificación' },
    { field: 'first_name', header: 'Nombres' },
    { field: 'first_lastname', header: 'Apellidos' },
    { field: 'position', header: 'Cargo' },
    { field: 'start_position', header: 'Apellidos' },
    { field: 'end_position', header: 'Cargo' },
    { field: 'state', header: 'Estado' }
  ];
  
  this.buildForm();

    this.getAuthorities();
    this.getPosition();
    this.getStatus();
    this.getUser();
 }


  openModal(authoritie: Authorities) {
    if (authoritie) {
      this.selectedAuthority = authoritie;
      console.log(authoritie);
      this.form.controls['id'].setValue(authoritie.id);
        this.form.controls['start_position'].setValue(authoritie.start_position);
        this.form.controls['end_position'].setValue(authoritie.end_position);
        this.form.controls['position'].setValue(authoritie.position.id);
        this.form.controls['status'].setValue(authoritie.status.id);
        this.form.controls['user'].setValue(authoritie.user.id);
    } else {
        this.form.reset();
    }
    this.dialog = true;
}


 
  

  buildForm() {
    this.form = this._fb.group({
      id: [],
      start_position: [''],
      end_position: [''],
      position: [''],
      status:[''], 
      user:[''], 
      });
  }


  getAuthorities(){
    this._spinnerService.show();
    this._cecyService.get('authorities').subscribe(response => {
      this._spinnerService.hide();
      this.authorities = response['data'];
    }, error => {
      this._spinnerService.hide();
      this._messageService.add({
        severity: 'error',
        summary: 'Oops! Problemas con el servidor',
        detail: 'Vuelve a intentar más tarde',
        life: 5000
    });
  });  
  }


  getPosition() {
    this._cecyService.get('catalogue').subscribe((r: any) => {
        for (let i = 0; i < r.length; i++) {
          if(r[i].code === 'authoritiesPosition'  ){
            let objeto: any = {};
            objeto.label = r[i].name;
            objeto.value = r[i].id;
            this.position.push(objeto);
    
          }
      }
    });
  }


  getStatus() {
    this._cecyService.get('catalogue').subscribe((r: any) => {
        for (let i = 0; i < r.length; i++) {
          if(r[i].code === 'authoritiesState'  ){
            let objeto: any = {};
            objeto.label = r[i].name;
            objeto.value = r[i].id;
            this.estatus.push(objeto);
          }
      }
    });
  }

  getUser() {
    this._cecyService.get('AuthoritiesUser').subscribe((r: any) => {
      console.log(r.data);
         for (let i = 0; i < r.data.length; i++) {
             let objeto: any = {};
             objeto.label = r.data[i].first_name + " " + r.data[i].first_lastname;
             objeto.value = r.data[i].id;
             this.user1.push(objeto);       
       }
     });
 
       
   }

 


  castAuthorities(): Authorities {
    return {
        id: this.form.controls['id'].value,
        start_position: this.form.controls['start_position'].value,
        end_position: this.form.controls['end_position'].value,
        user:
        {id:this.form.controls['user'].value},
        status:
        {id:this.form.controls['status'].value},
        position:
        {id:this.form.controls['position'].value},
    } as Authorities;
}



 

  onSubmit(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      if (this.form.controls['id'].value === null) {
        this.create();
      } else {
        this.update();
      }
      this.dialog = false;
    } else {
      this.form.markAllAsTouched();
    }
  }


  update() {
    this.selectedAuthority = this.castAuthorities();
    this._spinnerService.show();
    this._cecyService.update('authorities/'+ this.selectedAuthority.id , {
      'authorities': this.selectedAuthority,
      'position': this.selectedAuthority.position,
      'status': this.selectedAuthority.status,
      'user':this.selectedAuthority.user
    }).subscribe(
     ( response:any) => {
        this._spinnerService.hide();
         this._messageService.add({   
             severity: 'success',
             summary: 'Se creó correctamente',
             detail: 'Actualizada  Autoridades',
             life: 5000
         });
         this.getAuthorities();
      },error => {
        this._spinnerService.hide();
         this._messageService.add({
            severity: 'error',
            summary: 'Oops! Problemas con el servidor',
            detail: 'Vuelve a intentar más tarde',
            life: 5000
          });
        }
    );

  }

  create() {
    this.selectedAuthority = this.castAuthorities();
    console.log(this.selectedAuthority);
    this._spinnerService.show();
    this._cecyService.post('authorities/', {
      'authorities': this.selectedAuthority,
      'position': this.selectedAuthority.position,
      'status': this.selectedAuthority.status,
      'user':this.selectedAuthority.user
    }).subscribe(
      ( response:any) => {
        this._spinnerService.hide();
        this.getAuthorities();
        console.log(this.selectedAuthority);

        this._messageService.add({
          severity: 'success',
          summary: 'Se creó correctamente',
          detail: 'Autoridad Creada'
        });
      }, error => {
        this._spinnerService.hide();
        console.log(this.selectedAuthority);

        this._messageService.add({
          severity: 'error',
          summary: 'Oops! Problemas con el servidor',
          detail: 'Vuelve a intentar más tarde',
        });
      }
    );

  }

   delete(authoritie:Authorities){
    this._spinnerService.show();
    this._cecyService.delete('authorities/' + authoritie.id).subscribe(
      response => {
        this.authorities = this.authorities.filter(word => word.id != authoritie.id);
        this._spinnerService.hide();
        this._messageService.add({
            severity: 'success',
            summary: 'Se eliminó correctamente',
            detail: 'Eliminado  Autoridades',
            life: 5000
        });
      }, error =>{
        this._spinnerService.hide();
        this._messageService.add({
            severity: 'error',
            summary: 'Oops! Problemas con el servidor',
            detail: 'Vuelve a intentar más tarde',
            life: 5000
        });
      });

  }

  search(event, inputSearch) {
    if (inputSearch.length > 0 && event.key !== 'Backspace') {
      this.conditions = [{ field: this.selectedCol.field, logic_operator: 'ilike', match_mode: 'contains', value: inputSearch }];
      this.getAuthorities();
    } else if (inputSearch.length === 0) {
      this.conditions = null;
      this.getAuthorities();
    }
  }


  
}
