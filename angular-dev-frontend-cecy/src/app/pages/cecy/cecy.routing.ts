import { Routes } from "@angular/router";
import { ClassroomComponent } from './maestros/classroom/classroom.component';
import { SchoolPeriodComponent } from './maestros/school-period/school-period.component';
import { ScheduleComponent } from './maestros/schedule/schedule.component';
import { InstitucionComponent } from './maestros/institucion/institucion.component';
import { ParticipantscrudComponent } from './maestros/participantscrud/participantscrud.component';
import { AuthoritiesComponent } from './maestros/authorities/authorities.component';

/* import { PaidCoursesComponent } from "./matriculacion/paid-courses/paid-courses.component";
import { MisCursosComponent } from "./matriculacion/mis-cursos/mis-cursos.component";
 */
export const CecyRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "reports",
        loadChildren: () =>
          import(
            "./reports/reports.module"
          ).then((m) => m.ReportsModule),
      },
    ],
  },
  {
    path: "classroom",
    component: ClassroomComponent,
  },
  
  {
    path: "authorities",
    component: AuthoritiesComponent,
  },

  
  {
    path: "institucion",
    component: InstitucionComponent,
  },
  {
    path: "part",
    component: ParticipantscrudComponent,
  }, 
  {
    path: "schedule",
    component: ScheduleComponent,
  },
  
  {
    path: "schoolperiod",
    component: SchoolPeriodComponent,
  },
  /* {
    path: "cursos-pago",
    component: PaidCoursesComponent,
  },
  {
    path: "mis-cursos",
    component: MisCursosComponent,
  }, */
];
